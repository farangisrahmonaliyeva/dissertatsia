<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Battalions extends Model
{
    
    public function harbiyqism()
    {
        return $this->belongsTo(HarbiyQism::class);
    }
    public function jangchilars()
    {
        return $this->hasMany(jangchilar::class);
    }
    public function vzvods()
    {
        return $this->hasMany(Vzvod::class, 'battalion_id', 'id');
    }
    protected $fillable = ['nomi','harbiy_qism_id'];
}

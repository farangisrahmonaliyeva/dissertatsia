<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fan extends Model
{
    public function mavzus()
    {
        return $this->hasMany(Mavzu::class);
    }

    public function mashgulots()
    {
        return $this->hasMany(Mashgulot::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guruh extends Model
{
    public function vzvod()
    {
        return $this->belongsTo('App\Vzvod');
    }

    public function seksiyas()
    {
        return $this->hasMany('App\Seksiya');
    }
    public function jangchilars()
    {
        return $this->hasMany('App\jangchilar');
    }
    protected $fillable = ['nomi','vzvod_id'];
}

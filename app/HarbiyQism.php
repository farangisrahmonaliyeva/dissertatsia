<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HarbiyQism extends Model
{
    public function battalions()
    {
        return $this->hasMany(Battalions::class);
    }
    public function jangchilars()
    {
        return $this->hasMany(jangchilar::class,'harbiy_qism_id', 'id');
    }
    protected $fillable = ['nomi'];
}

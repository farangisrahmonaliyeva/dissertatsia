<?php

namespace App\Http\Controllers;

use App\Battalions;
use App\HarbiyQism;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BattalionsController extends Controller
{

    public function index()
    {
        $battalions = Battalions::all();
        return view('battalions',compact('battalions'));
    }
    public function battalionlar(Request $request)
    {
        $id = $request->route('id');
        $battalions = DB::table('battalions')->where('harbiy_qism_id',$id)->get();
        $hq = HarbiyQism::find($id);
        return view('batalionlar',['battalions'=>$battalions,'id'=>$id,'hq'=>$hq]);
    }
    public function create(Request $request)
    {
        $harbiy_qisms = HarbiyQism::all();
        $id = $request->get('id');
        return view('add_battalion',['harbiy_qisms'=>$harbiy_qisms,'id'=>$id]);
    }
    public function store(Request $request)
    {
        $request->validate([
            'nomi'=>'required',
        ]);
        $battalion = new Battalions([
            'nomi' => $request->get('nomi'),
            'harbiy_qism_id' => $request->get('id'),
        ]);
        $id = $request->get('id');
        $battalion->save();
        return redirect()->route('battalionlar',['id'=>$id])->with('success', 'Battalion qoshildi!');
    }

    public function show($id)
    {

    }
    public function edit($id)
    {
        $battalion = Battalions::find($id);
        $harbiy_qisms = HarbiyQism::all();
        return view('edit_battalion', ['battalion' => $battalion,'harbiy_qisms'=>$harbiy_qisms]);
    }
    public function update(Request $request, $id)
    {
        $battalion  = Battalions::find($id);
        $battalion->nomi = $request->get('nomi');
        $battalion->harbiy_qism_id = $request->get('harbiy_qism_id');
        $id = $request->get('harbiy_qism_id');
        $battalion->save();
        return redirect()->route('battalionlar',['id'=>$id])->with('success', 'Batalyon malumoti yangilandi!');
    }

    public function destroy($id)
    {
        $battalion = Battalions::find($id);
        $battalion->delete();
        return redirect()->back()->with('success', "Batalyon o'chirildi!");
    }
}

<?php

namespace App\Http\Controllers;

use App\arh;
use App\arh_end;
use App\Battalions;
use App\charchaganlik;
use App\charchaganlik_darajasi;
use App\Guruh;
use App\HarbiyQism;
use App\jangchilar;
use App\jangovor_tayyorgarlik;
use App\jis_hol_baho;
use App\jismoniy_holat;
use App\JismoniyHolati;
use App\jt__baholash;
use App\jt_fan;
use App\jt_fan_bahosi;
use App\jt_fan_ozlashtirish;
use App\jt_mash_ozlashtirish;
use App\jt_mashgulot;
use App\jt_mavzu;
use App\jt_mavzu_bahosi;
use App\jt_mavzu_ozlashtirish;
use App\MMT;
use App\mmt_bahosi;
use App\mmt_mash_baholash;
use App\mmt_mashgulot;
use App\mmt_mavzu;
use App\mmt_mavzu_bahosi;
use App\mmt_mavzu_ozlashtirish;
use App\mmt_ozlashtirish;
use App\mutaxassislik;
use App\Seksiya;
use App\soglomligi;
use App\Vzvod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BaxolashController extends Controller
{
    public function index(Request $request)
    {
        $seksiyas = [];//Seksiya::all();
        $guruhs = [];//Guruh::all();
        $vzvods = [];//Vzvod::all();
        $battalions = [];//Battalions::all();
        $harbiy_qisms = HarbiyQism::all();
        $jangchilars = [];//jangchilar::all();
        $id = 0;
        $baho  = [];
        $mutaxassisliks = mutaxassislik::all();
        if ($request->get('ismi')) {
            $id=$request->get('ismi');
            $baho = DB::table('jt_fans')->select('jt_fans.id', 'jt_fans.nomi', DB::Raw('sum(jt__baholashes.bahosi) as baho'))->
            leftJoin('jt_mavzus', 'jt_fans.id', 'jt_mavzus.fan_id')->
            leftJoin('jt_mashgulots', 'jt_mavzus.id', 'jt_mashgulots.jt_mavzu_id')->
            leftJoin('jt__baholashes', 'jt__baholashes.id', 'jt_mashgulots.jt_mavzu_id')->
            where('jt__baholashes.jangchi_id', $id)->groupBy('jt_fans.id', 'jt_fans.nomi')->get();
            $arr = [];
            foreach ($baho as $b)
                $arr[$b->id] = $b;
            $baho = $arr;
        }
        $mmt_mavzus = mmt_mavzu::all();
        $mmt_mashgulots = mmt_mashgulot::all();
        $jt_fans = jt_fan::all();
        $jt_mashgulots = jt_mashgulot::all();//jt_mashgulot::all();
        $jt_mavzus = jt_mavzu::all();//jt_mavzu::all();
        $jismoniy_holatis = [];
        $charchaganlik_darajasi = [];
        $arh_ends = [];
        $arh = [];
        $soglomligi = [];
        $mmt_ozlashtirish =[];
//        return $charchaganlik_darajasi;
        $jangchi = DB::table('jangchilars')->find($id);
//return $jismoniy_holatis;
        if (!empty($jangchi)) {
            $seksiyas = Seksiya::where('guruh_id', $jangchi->guruh_id)->get();//Seksiya::all();
            $guruhs = Guruh::where('vzvod_id', $jangchi->vzvod_id)->get();//Guruh::all();
            $vzvods = Vzvod::where('battalion_id', $jangchi->battalion_id)->get();//Vzvod::all();
            $battalions = Battalions::where('harbiy_qism_id', $jangchi->harbiy_qism_id)->get();//Battalions::all();
            if (!empty($jangchi->seksiya_id))
                $jangchilars = jangchilar::where('seksiya_id', $jangchi->seksiya_id)->get();
            elseif (!empty($jangchi->guruh_id))
                $jangchilars = jangchilar::where('guruh_id', $jangchi->guruh_id)->get();
            elseif (!empty($jangchi->vzvod_id))
                $jangchilars = jangchilar::where('vzvod_id', $jangchi->vzvod_id)->get();
            elseif (!empty($jangchi->battalion_id))
                $jangchilars = jangchilar::where('battalion_id', $jangchi->battalion_id)->get();
            elseif (!empty($jangchi->harbiy_qism_id))
                $jangchilars = jangchilar::where('harbiy_qism_id', $jangchi->harbiy_qism_id)->get();

                $jismoniy_holatis = jismoniy_holat::where('jangchi_id',$jangchi->id)->get();
                if (!count($jismoniy_holatis)) $jismoniy_holatis = [] ;

                $charchaganlik_darajasi = charchaganlik::where('jangchi_id',$jangchi->id)->get();
                if (!count($charchaganlik_darajasi)) $charchaganlik_darajasi=[];

                $arh_ends = arh_end::where('jangchi_id',$jangchi->id)->get();

                if (!count($arh_ends)) $arh_ends=[];
                $arh = arh::where('jangchi_id',$jangchi->id)->get();
                if (!count($arh)) $arh=[];

                $mmt_ozlashtirish = mmt_ozlashtirish::all();
                if (!count($mmt_ozlashtirish)) $mmt_ozlashtirish =[];

                $soglomligi = DB::table('soglomligis')->where('jangchi_id',$jangchi->id)->orderBy('id', 'desc')->first();
//                return var_dump($soglomligi);
//                if ($soglomligi==NULL) $soglomligi = [];
        }
//return $arh;
        return view('baholash',[
            'mutaxassisliks'=>$mutaxassisliks,
            'id'=>$id,'jangchi'=>$jangchi,
            'mmt_mavzus'=>$mmt_mavzus,
            'mmt_mashgulots'=>$mmt_mashgulots,
            'jt_fans'=>$jt_fans,
            'jt_mashgulots'=>$jt_mashgulots,
            'jt_mavzus'=>$jt_mavzus,
            'seksiyas'=>$seksiyas,
            'guruhs'=>$guruhs,
            'vzvods'=>$vzvods,
            'battalions'=>$battalions,
            'harbiy_qisms'=>$harbiy_qisms,
            'jangchilars'=>$jangchilars,
            'baho'=>$baho,
            'charchaganlik_darajasi'=>$charchaganlik_darajasi,
            'jismoniy_holatis'=>$jismoniy_holatis,
            'arh_ends'=>$arh_ends,
            'mmt_ozlashtirish'=>$mmt_ozlashtirish,
            'soglomligi'=>$soglomligi,
            'arh'=>$arh,
        ]);
    }
    public function search(Request $request)
    {
        $harbiy_qism = $request->get('harbiy_qism');
        $battalion = $request->get('battalion');
        $vzvod = $request->get('vzvod');
        $guruh = $request->get('guruh');
        $seksiya = $request->get('seksiya');
        $familiya = $request->get('familiya');
        $mavzu = $request->get('mavzu');
        $guruh2 = $request->get('guruh2');
        $battalion2 = $request->get('battalion2');
        $vzvod2 = $request->get('vzvod2');
        $harbiy_qism2 = $request->get('harbiy_qism2');
        $data = [];
        $data2 = [];
        if (!empty($harbiy_qism))
        {
            $data = Battalions::select('id  as num', 'nomi')->where('harbiy_qism_id', $harbiy_qism)->get();
            $txt = 'Batalyonni';
            $data2 = jangchilar::select('familyasi as num', 'familyasi as nomi')->where('harbiy_qism_id', $harbiy_qism)->groupBy('familyasi')->get();
        }
        if (!empty($battalion))
        {
            $data = Vzvod::select('id  as num', 'nomi')->where('battalion_id', $battalion)->get();
            $txt = 'Vzvodni';
            $data2 = jangchilar::select('familyasi as num', 'familyasi as nomi')->where('battalion_id', $battalion)->groupBy('familyasi')->get();
        }
        if (!empty($vzvod))
        {
            $data = Guruh::select('id  as num', 'nomi')->where('vzvod_id', $vzvod)->get();
            $txt = 'Guruhni';
            $data2 = jangchilar::select('familyasi as num', 'familyasi as nomi')->where('vzvod_id', $vzvod)->groupBy('familyasi')->get();
        }
        if (!empty($guruh))
        {
            $data = Seksiya::select('id  as num', 'nomi')->where('guruh_id', $guruh)->get();
            $txt = 'Seksiyani';
            $data2 = jangchilar::select('familyasi as num', 'familyasi as nomi')->where('guruh_id', $guruh)->groupBy('familyasi')->get();
        }
        if (!empty($familiya) && (!empty($seksiya) || !empty($guruh2) || !empty($battalion2) || !empty($vzvod2) || !empty($harbiy_qism2)))
        {
            if (!empty($seksiya))
                $data = jangchilar::select('id as num', 'ismi as nomi')->where('familyasi', $familiya)->where('seksiya_id', $seksiya)->get();
            elseif (!empty($guruh2))
                $data = jangchilar::select('id as num', 'ismi as nomi')->where('familyasi', $familiya)->where('guruh_id', $guruh2)->get();
            elseif (!empty($vzvod2))
                $data = jangchilar::select('id as num', 'ismi as nomi')->where('familyasi', $familiya)->where('vzvod_id', $vzvod2)->get();
            elseif (!empty($battalion2))
                $data = jangchilar::select('id as num', 'ismi as nomi')->where('familyasi', $familiya)->where('battalion_id', $battalion2)->get();
            elseif (!empty($harbiy_qism2))
                $data = jangchilar::select('id as num', 'ismi as nomi')->where('familyasi', $familiya)->where('harbiy_qism_id', $harbiy_qism2)->get();
//            $jangchi_id = DB::table('jangchilars')->where('familyasi', $familiya)->where('seksiya_id', $seksiya)->get();
            $txt = 'Ismini';
            $data2 = $data;
//            return "<pre>".print_r($data, true);
//            die;
        }
        elseif (!empty($seksiya))
        {
            $data = jangchilar::select('familyasi as num', 'familyasi as nomi')->
            where('seksiya_id', $seksiya)->groupBy('familyasi')->get();
            $txt = 'Familiyani';
            $data2 = $data;
        }
        if (!empty($mavzu))
        {
            $data = mmt_mashgulot::select('id  as num', 'nomi')->where('mmt_mavzu_id', $mavzu)->get();
            $txt = "Mashg'ulotni tanlang";
        }
        $text = '<option value="">'.$txt.' tanlang</option>';
        foreach ($data as $datum) {
            $text = $text.'<option value="'.$datum['num'].'">'.$datum['nomi'].'</option>';
        }
        $text2 = '<option value="">F.I.Sh tanlang</option>';
        foreach ($data2 as $datum) {
            $text2 = $text2.'<option value="'.$datum['num'].'">'.$datum['nomi'].'</option>';
        }
        return json_encode(['text' =>$text, 'text2' => $text2]);
//        return view('baholash',/$text;
    }
//    public function searchMRX(Request $request)
    public function getId(Request $request)
    {
        $jangchi_id = $request->get('ismi');
        return redirect()->route('baholashId',[$jangchi_id]);
    }
    public function soglomligi(Request $request)
    {
//        $a = [];
        $id = $request->get('ismi');
        if($id==0)return redirect()->back()->with('Jangchini tanlang');
            $soglomligi = new soglomligi([
               'jangchi_id' => $id,
                'soglomligi'=> $request->get('soglomligi'),
            ]);
            $soglomligi->save();
            $soglomligi = soglomligi::orderBy('created_at','desc')->where('jangchi_id',$id)->first();
            $charchaganlik_darajasi = charchaganlik_darajasi::orderBy('created_at','desc')->where('jangchi_id',$id)->first();
            $jismoniy_holati = JismoniyHolati::orderBy('created_at','desc')->where('jangchi_id',$id)->first();

            if($jismoniy_holati ==NULL)$jh =0; else $jh = $jismoniy_holati->umumiy_baho;
            if($charchaganlik_darajasi==NULL)$chd = 0; else $chd = $charchaganlik_darajasi->charchaganlik_darajasi;;

        $jis_hol_baho = new jis_hol_baho([
            'jangchi_id'=>$id,
            'baho' =>($soglomligi->soglomligi+$chd+$jh)*100/5,
        ]);
        $jis_hol_baho->save();

        return redirect()->route('baholashId',['ismi' => $id]);
    }
    public function jismoniy_tayyorgarlik(Request $request)
    {
        $id = $request->get('ismi');
        if($id==0)return redirect()->back()->with('success','Jangchini tanlang');

        $turnik = $request->get('turnik');
        $l100_metrga_yugurish = $request->get('100_metrga_yugurish');
        $l1_kmga_yugurish = $request->get('l1_kmga_yugurish');
        $l3_kmga_yugurish = $request->get('l3_kmga_yugurish');
        $lapkost = $request->get('lapkost');
        $l1erkin_mashqlar_toplami = $request->get('l1erkin_mashqlar_toplami');
        $l2erkin_mashqlar_toplami = $request->get('l2erkin_mashqlar_toplami');
        $umumiy_baho = $request->get('umumiy_baho');
        $press_qolni_bukish=$request->get('press_qolni_bukish');

        $jismoniy_holati = new JismoniyHolati([
                'jangchi_id'=>$id,
                'turnik'=>$turnik,
                '100_metrga_yugurish'=>$l100_metrga_yugurish,
                '1_kmga_yugurish'=>$l1_kmga_yugurish,
                '3_kmga_yugurish'=>$l3_kmga_yugurish,
                'lapkost'=>$lapkost,
                'press_qolni_bukish'=>$press_qolni_bukish,
                '1erkin_mashqlar_toplami'=>$l1erkin_mashqlar_toplami,
                '2erkin_mashqlar_toplami'=>$l2erkin_mashqlar_toplami,
                'umumiy_baho'=>$umumiy_baho,
            ]);
            $jismoniy_holati->save();

        $soglomligi = soglomligi::orderBy('created_at','desc')->where('jangchi_id',$id)->first();
        $charchaganlik_darajasi = charchaganlik_darajasi::orderBy('created_at','desc')->where('jangchi_id',$id)->first();
        $jismoniy_holati = JismoniyHolati::orderBy('created_at','desc')->where('jangchi_id',$id)->first();

        if($soglomligi ==NULL)$sg =0; else $sg = $soglomligi->soglomligi;
        if($jismoniy_holati ==NULL)$jh =0; else $jh = $jismoniy_holati->umumiy_baho;
        if($charchaganlik_darajasi==NULL)$chd = 0; else $chd = $charchaganlik_darajasi->charchaganlik_darajasi;;

        $jis_hol_baho = new jis_hol_baho([
            'jangchi_id'=>$id,
            'baho' =>($sg+$chd+$jh)*100.0/5,
        ]);
        $jis_hol_baho->save();


        return redirect()->route('baholashId',['ismi' => $id]);
    }
    public function charchaganlik_darajasi(Request $request)
    {
        $id = $request->get('ismi');
        if($id==0)return redirect()->back()->with('Jangchini tanlang');

            $jismoniy_holati = new charchaganlik_darajasi([
                'jangchi_id'=>$id,
                'charchaganlik_darajasi'=>$request->get('charchaganlik_darajasi'),
            ]);
            $jismoniy_holati->save();
        $soglomligi = soglomligi::orderBy('created_at','desc')->where('jangchi_id',$id)->first();
        $charchaganlik_darajasi = charchaganlik_darajasi::orderBy('created_at','desc')->where('jangchi_id',$id)->first();
        $jismoniy_holati = JismoniyHolati::orderBy('created_at','desc')->where('jangchi_id',$id)->first();

        if($soglomligi ==NULL)$sg =0; else $sg = $soglomligi->soglomligi;
        if($jismoniy_holati ==NULL)$jh =0; else $jh = $jismoniy_holati->umumiy_baho;
        if($charchaganlik_darajasi==NULL)$chd = 0; else $chd = $charchaganlik_darajasi->charchaganlik_darajasi;;

        $jis_hol_baho = new jis_hol_baho([
            'jangchi_id'=>$id,
            'baho' =>($sg+$chd+$jh)*100.0/5,
        ]);
        $jis_hol_baho->save();
        return redirect()->route('baholashId',['ismi' => $id]);
    }
    public function arh(Request $request)
    {
        $id = $request->get('ismi');
        if($id==0)return redirect()->back()->with('Jangchini tanlang');

        $jismoniy_holati = new arh([
            'jangchi_id'=>$request->get('ismi'),
            'arh'=>$request->get('arh'),
        ]);
        $jismoniy_holati->save();

        return redirect()->route('baholashId',['ismi' => $id]);
    }
    public function mmt(Request $request)
    {
        $id = $request->get('ismi');
        if($id==0)return redirect()->back()->with('Jangchini tanlang');

        $mmt_mash_baholash = new mmt_mash_baholash([
            'jangchi_id'=>$id,
            'oquv_yili'=>$request->get('oquv_yili'),
            'mmt_mavzu_id'=>$request->get('mavzu'),
            'mmt_mash_id'=>$request->get('mashgulot'),
            'bahosi'=>$request->get('bahosi'),
            'mmt_vaqti'=>$request->get('vaqti'),
        ]);
            $mmt_mash_baholash->save();

            $mmt_mavzu = mmt_mavzu::find($request->get('mavzu'));
            $mmt_mash = mmt_mashgulot::find($request->get('mashgulot'));
            $mmt_mash_baholash->ozlashtirish = $mmt_mash->soati * $mmt_mash_baholash->bahosi*1./$mmt_mavzu->soati;
            $mmt_mash_baholash->save();

            $mmt_ozlashtirish = mmt_ozlashtirish::where('jangchi_id',$id)->where('oquv_yili',$request->get('oquv_yili'))
                                                ->where('mmt_mavzu_id',$request->get('mavzu'))->where('mmt_mash_id',$request->get('mashgulot'))
                                                ->sum('ozlashtirish');
            $mmt_mavzu_bahosi = new mmt_mavzu_bahosi([
                'jangchi_id'=>$id,
                'oquv_yili'=>$request->get('oquv_yili'),
                'mavzu_id'=>$request->get('mavzu'),
                'mavzu_bahosi'=>$mmt_ozlashtirish,
            ]);
            $mmt_mavzu_bahosi->save();
            $ball = mmt_mavzu_ozlashtirish::where('jangchi_id',$id)
                ->where('oquv_yili',$request->get('oquv_yili'))->sum('mavzu_bahosi');
//            return $ball;
            $mmt = new mmt_bahosi([
                'jangchi_id'=>$id,
                'oquv_yili' =>$request->get('oquv_yili'),
                'bahosi' =>$ball,
                'vaqti'=>$request->get('vaqti'),
            ]);
            $mmt->save();
        return redirect()->route('baholashId',['ismi' => $id]);
    }
    public function jt(Request $request)
    {
        $id = $request->get('ismi');
        if($id==0)return redirect()->back()->with('success','Harbiy xizmatchini tanlang');

            $jt__baholash = new jt__baholash([
                'jangchi_id'=>$id,
                'oquv_yili'=>$request->get('oquv_yili'),
                'jt_fan_id'=>$request->get('jt_fan'),
                'mutaxassislik'=>$request->get('mutaxassislik'),
                'jt_mavzu_id'=>$request->get('jt_mavzu'),
                'jt_mash_id'=>$request->get('jt_mashgulot'),
                'bahosi'=>$request->get('bahosi'),
                'jt_vaqti'=>$request->get('vaqti'),
                'mutaxassislik'=>$request->get('mutaxassislik'),
            ]);
            $jt__baholash->save();
//           $jt_fan = jt_fan::find($request->get('jt_fan'));
            $jt_mavzu = jt_mavzu::find($request->get('jt_mavzu'));
            $jt_mash = jt_mashgulot::find($request->get('jt_mashgulot'));

            $jt__baholash->ozlashtirish = $jt_mash->soati * $jt__baholash->bahosi*1./$jt_mavzu->soati;
            $jt__baholash->save();
//        return $jt__baholash;

            $jt_mavzu_ozlashtirish = jt_mash_ozlashtirish::where('jangchi_id',$id)->where('oquv_yili',$request->get('oquv_yili'))
                ->where('jt_fan_id',$request->get('jt_fan'))->where('jt_mavzu_id',$request->get('jt_mavzu'))
                ->where('mutaxassislik',$request->get('mutaxassislik'))->sum('ozlashtirish');

            $jt_mavzu_bahosi = new jt_mavzu_bahosi([
                'jangchi_id'=>$id,
                'mutaxassislik'=>$request->get('mutaxassislik'),
                'oquv_yili'=>$request->get('oquv_yili'),
                'fan_id'=>$request->get('jt_fan'),
                'mavzu_id'=>$request->get('jt_mavzu'),
                'bahosi'=>$jt_mavzu_ozlashtirish,
            ]);
            $jt_mavzu_bahosi->save();
            $ball = jt_mavzu_ozlashtirish::where('jangchi_id',$id)->where('oquv_yili',$request->get('oquv_yili'))
            ->where('fan_id',$request->get('jt_fan'))->where('mutaxassislik',$request->get('mutaxassislik'))->sum('bahosi');

//            return $ball;
        $jt_fan_bahosi = new jt_fan_bahosi([
            'jangchi_id'=>$id,
            'oquv_yili' =>$request->get('oquv_yili'),
            'mutaxassislik' =>$request->get('mutaxassislik'),
            'fan_id'=>$request->get('jt_fan'),
            'bahosi'=>$ball,
            'vaqti'=>$request->get('vaqti'),
        ]);
        $jt_fan_bahosi->save();
        $ball = jt_fan_ozlashtirish::where('jangchi_id',$id)->where('oquv_yili',$request->get('oquv_yili'))
            ->where('mutaxassislik',$request->get('mutaxassislik'))->avg('bahosi');
        $jangovor_tayyorgarlik = new jangovor_tayyorgarlik([
            'jangchi_id'=>$id,
            'oquv_yili' =>$request->get('oquv_yili'),
            'mutaxassislik' =>$request->get('mutaxassislik'),
            'ball' =>$ball
        ]);
        $jangovor_tayyorgarlik->save();
        return redirect()->route('baholashId',['ismi' => $id])->with('success',"Harbiy xizmatchi jangovor tayyorgarlik bo'yicha baxolandi");
    }

}

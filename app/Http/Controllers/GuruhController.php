<?php

namespace App\Http\Controllers;

use App\Guruh;
use App\Vzvod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GuruhController extends Controller
{
    public function index()
    {
        $guruhs = Guruh::all();
        return view('guruhs',compact('guruhs'));
    }
    public function guruhlar(Request $request)
    {
        $id = $request->route('id');
        $vz = Vzvod::find($id);
        $guruhs = DB::table('guruhs')->where('vzvod_id',$id)->get();
        return view('guruhlar',['guruhs'=>$guruhs,'id'=>$id,'hq'=>$vz]);
    }

    public function create(Request $request)
    {
        $vzvods = Vzvod::all();
        $id = $request->get('id');
//        return $id;
        return view('add_guruh',['vzvods'=>$vzvods,'id'=>$id]);
    }

    public function store(Request $request)
    {
        $guruh = new Guruh([
            'nomi' => $request->get('nomi'),
            'vzvod_id' => $request->get('id'),
        ]);
        $id = $request->get('id');
        $guruh->save();
        return redirect()->route('guruhlar',['id'=>$id])->with('success', 'Guruh qoshildi!');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $guruh = Guruh::find($id);
        $vzvods = Vzvod::all();
        return view('edit_guruh', ['vzvods' => $vzvods,'guruh'=>$guruh]);
    }

    public function update(Request $request, $id)
    {
        $guruh  = Guruh::find($id);
        $guruh->nomi = $request->get('nomi');
        $guruh->vzvod_id = $request->get('vzvod_id');
        $guruh->save();
        $id = $request->get('vzvod_id');
        return redirect()->route('guruhlar',['id'=>$id])->with('success', "Guruh ma'lumoti yangilandi!");

    }

    public function destroy($id)
    {
        $guruh  = Guruh::find($id);
        $guruh->delete();
        return redirect()->back()->with('success', "Guruh o'chirildi!");
    }
}

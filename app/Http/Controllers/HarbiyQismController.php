<?php

namespace App\Http\Controllers;

use App\Battalions;
use App\HarbiyQism;
use App\jangchilar;
use Illuminate\Http\Request;

class HarbiyQismController extends Controller
{

    public function index()
    {
        $harbiy_qism = HarbiyQism::all();
        return view('harbiy_qism',compact('harbiy_qism'));
    }
    public function harbiy_qismlar()
    {
        $harbiy_qisms = HarbiyQism::all();
        $jangchilars = jangchilar::all();

        return view('bolinmalar',['harbiy_qisms'=>$harbiy_qisms,'jangchilars'=>$jangchilars]);
    }

    public function create()
    {
        return view('add_harbiy_qism');
    }

    public function store(Request $request)
    {

        $harbiy_qism = new HarbiyQism([
            'nomi' => $request->get('nomi'),
        ]);
        $harbiy_qism->save();
        return redirect()->route('bolinma')->with('success', 'harbiy qism qoshildi!');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $harbiy_qism = HarbiyQism::find($id);
//        $jangchi = jangchilar::find($id);
        return view('edit_harbiy_qism', ['harbiy_qism' => $harbiy_qism]);
    }

    public function update(Request $request, $id)
    {
        $harbiy_qism  = HarbiyQism::find($id);
        $harbiy_qism->nomi = $request->get('nomi');
        $harbiy_qism->save();
        return redirect('/bolinma')->with('success', "Harbiy qism malumoti yangilandi!");
    }

    public function destroy($id)
    {
        $harbiy_qism = HarbiyQism::find($id)->delete();
        return redirect('/bolinma')->with('success', 'Harbiy qism o\'chirildi! ');
    }
}

<?php

namespace App\Http\Controllers;

use App\Battalions;
use App\Guruh;
use App\HarbiyQism;
use App\jangchilar;
use App\Seksiya;
use App\Vzvod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JangchilarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jangchilars = jangchilar::all();
        $harbiy_qisms = HarbiyQism::all();
        $seksiyas = [];//Seksiya::all();
        $guruhs = [];//Guruh::all();
        $vzvods = [];//Vzvod::all();
        $battalions = [];//Battalions::all();

//        $jangchilars = [];//jangchilar::all();
//        $id = 0;
        $baho  = [];
        return view('jangchilar',['jangchilars'=>$jangchilars,'seksiyas'=>$seksiyas,'guruhs'=>$guruhs,'vzvods'=>$vzvods,'battalions'=>$battalions,'harbiy_qisms'=>$harbiy_qisms]);

    }
    public function seksiya(Request $request)
    {
        $id = $request->route('seksiya_id');
        $jangchilars = DB::table('jangchilars')->where('seksiya_id',$id)->get();
        $harbiy_qisms = HarbiyQism::all();
        return view('s_jangchilar',['jangchilars'=>$jangchilars,'harbiy_qisms'=>$harbiy_qisms]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $harbiy_qisms = HarbiyQism::all();
        return view('add_jangchi', ['harbiy_qism' => $harbiy_qisms]);
    }

    /**
     * Store a jnewly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
             $jangchi = new jangchilar([
            'unvoni' => $request->get('unvoni'),
            'familyasi' => $request->get('familiya'),
            'ismi' => $request->get('ismi'),
            'sharifi' => $request->get('sharifi'),
            'jinsi'=>$request->get('jinsi'),
            'tugilgan_vaqti'=>$request->get('date'),
            'ish_joyi'=>$request->get('ish_joyi'),
            'boyi'=>$request->get('boy'),
            'vazni'=>$request->get('vazn'),
            'harbiy_qism_id'=>$request->get('harbiy_qism'),
            'battalion_id'=>$request->get('battalion'),
            'vzvod_id'=>$request->get('vzvod'),
            'guruh_id'=>$request->get('guruh'),
            'seksiya_id'=>$request->get('seksiya'),
        ]);
//             var_dump($jangchi);
        $jangchi->save();
        $harbiy_qism = HarbiyQism::find($request->get('harbiy_qism'));
        $harbiy_qism->shaxsiy_tarkib_soni ++;
        $harbiy_qism->save();
if($request->get('battalion'))
{
    $battalion = Battalions::find($request->get('battalion'));
    $battalion->shaxsiy_tarkib_soni ++;
    $battalion->save();
}

if($request->get('vzvod'))
{
    $vzvod = Vzvod::find($request->get('vzvod'));
    $vzvod->shaxsiy_tarkib_soni ++;
    $vzvod->save();
}

if($request->get('guruh'))
{
    $guruh = Guruh::find($request->get('guruh'));
    $guruh->shaxsiy_tarkib_soni ++;
    $guruh->save();
}

if($request->get('seksiya'))
{
    $seksiya = Seksiya::find($request->get('seksiya'));
    $seksiya->shaxsiy_tarkib_soni ++;
    $seksiya->save();
}

        return redirect()->back()->with('success', "Harbiy xizmatchi qo'shildi!");
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $harbiy_qism = HarbiyQism::all();
        $jangchi = jangchilar::find($id);
        return view('edit_jangchi', ['harbiy_qism' => $harbiy_qism,'jangchi'=>$jangchi]);
    }

    public function update(Request $request, $id)
    {
        $jangchi = jangchilar::find($id);

        $jangchi->unvoni =  $request->get('unvoni');
        $jangchi->familyasi = $request->get('familiya');
        $jangchi->ismi = $request->get('ismi');
        $jangchi->sharifi = $request->get('sharifi');
        $jangchi->jinsi = $request->get('jinsi');
        $jangchi->tugilgan_vaqti = $request->get('date');
        $jangchi->ish_joyi = $request->get('ish_joyi');
        $jangchi->boyi = $request->get('boy');
        $jangchi->vazni = $request->get('vazn');
        $jangchi->harbiy_qism_id = $request->get('harbiy_qism');
        $jangchi->battalion_id = $request->get('battalion');
        $jangchi->vzvod_id = $request->get('vzvod');
        $jangchi->guruh_id = $request->get('guruh');
        $jangchi->seksiya_id = $request->get('seksiya');

        $jangchi->save();

        return redirect('/jangchilar')->with('success', "Harbiy xizmatchi ma'lumoti yangilandi!");
    }


    public function destroy(Request $request,$id)
    {
        $jangchi = jangchilar::find($id);
        $jangchi->delete();
        $harbiy_qism = HarbiyQism::find($request->get('harbiy_qism'));
        $harbiy_qism->shaxsiy_tarkib_soni --;
        $harbiy_qism->save();

        if($request->get('battalion'))
        {
            $battalion = Battalions::find($request->get('battalion'));
            $battalion->shaxsiy_tarkib_soni --;
            $battalion->save();
        }

        if($request->get('vzvod'))
        {
            $vzvod = Vzvod::find($request->get('vzvod'));
            $vzvod->shaxsiy_tarkib_soni --;
            $vzvod->save();
        }

        if($request->get('guruh'))
        {
            $guruh = Guruh::find($request->get('guruh'));
            $guruh->shaxsiy_tarkib_soni --;
            $guruh->save();
        }

        if($request->get('seksiya'))
        {
            $seksiya = Seksiya::find($request->get('seksiya'));
            $seksiya->shaxsiy_tarkib_soni --;
            $seksiya->save();
        }
        return redirect()->back()->with("success','Harbiy xizmatchi o'chirildi");
    }
}

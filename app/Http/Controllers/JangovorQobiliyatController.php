<?php

namespace App\Http\Controllers;

use App\Battalions;
use App\Guruh;
use App\HarbiyQism;
use App\jangchilar;
use App\Vzvod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JangovorQobiliyatController extends Controller
{

    public function index()
    {
        $harbiy_qisms = HarbiyQism::all();
        $jangchilars = jangchilar::all();
        $i=1;
        return view('jangovor_qobilyat',['harbiy_qisms'=>$harbiy_qisms,'jangchilars'=>$jangchilars,'i'=>$i]);
//        return view('',compact('jangchilars'));
    }
    public function jismoniy_xolati()
    {
        $jangchilars = jangchilar::all();
        return view('jismoniy_xolati',compact('jangchilars'));
    }
    public function battalionlar(Request $request)
    {
        $id = $request->route('id');
        $battalions = DB::table('battalions')->where('harbiy_qism_id',$id)->get();
        $hq = HarbiyQism::find($id);
        $jangchilars  = jangchilar::where('harbiy_qism_id',$id)->get();
        return view('jq_bat',['battalions'=>$battalions,'jangchilars'=>$jangchilars,'id'=>$id,'hq'=>$hq]);
    }
    public function vzvodlar(Request $request)
    {
        $id = $request->route('id');
        $bt = Battalions::find($id);
        $vzvods = DB::table('vzvods')->where('battalion_id',$id)->get();
        $jangchilars  = jangchilar::where('battalion_id',$id)->get();
        return view('jq_vzvod',['vzvods'=>$vzvods,'jangchilars'=>$jangchilars,'id'=>$id,'hq'=>$bt]);
    }
    public function guruhlar(Request $request)
    {
        $id = $request->route('id');
        $vz = Vzvod::find($id);
        $guruhs = DB::table('guruhs')->where('vzvod_id',$id)->get();
        $jangchilars  = jangchilar::where('vzvod_id',$id)->get();
        return view('jq_guruh',['guruhs'=>$guruhs,'jangchilars'=>$jangchilars,'id'=>$id,'hq'=>$vz]);
    }
    public function seksiyalar(Request $request)
    {
        $id = $request->route('id');
        $gr = Guruh::find($id);
        $seksiyas = DB::table('seksiyas')->where('guruh_id',$id)->get();
        $jangchilars  = jangchilar::where('vzvod_id',$id)->get();
        return view('jq_seksiya',['seksiyas'=>$seksiyas,'jangchilars'=>$jangchilars,'id'=>$id,'hq'=>$gr]);
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}

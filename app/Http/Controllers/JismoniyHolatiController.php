<?php

namespace App\Http\Controllers;

use App\Battalions;
use App\Guruh;
use App\HarbiyQism;
use App\jangchilar;
use App\Seksiya;
use App\Vzvod;
use Illuminate\Http\Request;

class JismoniyHolatiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function soglomligi()
    {
        $seksiyas = Seksiya::all();
        $guruhs = Guruh::all();
        $vzvods = Vzvod::all();
        $battalions = Battalions::all();
        $harbiy_qisms = HarbiyQism::all();
        $jangchilars = jangchilar::all();
        return view('soglomligi',compact(['seksiyas','guruhs','vzvods','battalions','harbiy_qisms','jangchilars']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

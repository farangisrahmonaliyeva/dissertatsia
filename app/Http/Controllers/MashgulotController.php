<?php

namespace App\Http\Controllers;

use App\JismoniyHolati;
use App\jt_fan;
use App\jt_mashgulot;
use App\jt_mavzu;
use App\Mashgulot;
use App\mmt_mashgulot;
use App\mmt_mavzu;
use App\mutaxassislik;
use Illuminate\Http\Request;

class MashgulotController extends Controller
{
    public function index()
    {
        $mashgulotlars = Mashgulot::all();
        $mmt_mavzus = mmt_mavzu::all();
        $mmt_mashgulots = mmt_mashgulot::all();
        $fans = jt_fan::all();
        $jt_mavzus = jt_mavzu::all();
        $jt_mashgulots = jt_mashgulot::all();
        $mutaxassisliks = mutaxassislik::all();
        return view('mashgulotlar',compact(['mmt_mavzus','fans','jt_mavzus','jt_mashgulots','mmt_mashgulots','mutaxassisliks']));
    }
    public function add_mmt_mavzu(Request $request)
    {
        $nomi = $request->get('mavzu');
        $mmt_mavzu = new mmt_mavzu([
            'nomi'=> $request->get('mavzu'),
            'mavzu_raqami'=>$request->get('mavzu_raqami'),
//            'oquv_yili'=>$request->get('oquv_yili'),
        ]);
        $mmt_mavzu->save();
        return redirect()->route('mashgulotlar')->with('success', "Mavzu qo'shildi!");
    }
    public function edit_mmt_mavzu($id)
    {
        return view();
    }
    public function delete_mmt_mavzu(Request $request)
    {
        $id = $request->get('mavzu_id');
        $mmt_mavzu = mmt_mavzu::find($id);
        $mmt_mavzu->delete();
        return redirect()->back()->with('success',"MMT mavzu o'chirildi");
    }
    public function add_mmt_mashgulot(Request $request)
    {
        $mmt_mashgulot = new mmt_mashgulot([
            'nomi' => $request->get('nomi'),
            'mmt_mavzu_id' => $request->get('mavzu_id'),
            'soati'=> $request->get('soati'),
        ]);
        $mmt_mashgulot->save();
        $mmt_mavzu =  mmt_mavzu::find($request->get('mavzu_id'));
        $mmt_mavzu->soati  +=$mmt_mashgulot->soati;
        $mmt_mavzu->save();
        return redirect()->route('mashgulotlar')->with('success', 'Mashgulot qoshildi!');
    }
    public function delete_mmt_mashgulot(Request $request)
    {
        $mavzu_id = $request->get('mavzu_id');
//        $mash_id =
//        $mmt_mavzu = mmt_mavzu::find($id);
//        $mmt_mavzu->delete();
        return redirect()->back()->with('success',"MMT mavzu o'chirildi");
    }
    public function mutax(Request $request)
    {
        $jt_fan = new mutaxassislik([
            'nomi'=> $request->get('nomi'),
            'oquv_yili'=>$request->get('oquv_yili'),
        ]);
        $jt_fan->save();
        return redirect()->route('mashgulotlar')->with('success', 'Mutaxassislik qoshildi!');
    }

    public function add_jt_fan(Request $request)
    {
        $jt_fan = new jt_fan([
            'nomi'=> $request->get('nomi'),
            'oquv_yili'=> $request->get('oquv_yili'),
            'mutaxassislik_id'=> $request->get('mutaxassislik'),
        ]);
        $jt_fan->save();
        return redirect()->route('mashgulotlar')->with('success', 'Mashgulot qoshildi!');
    }
    public function add_jt_mavzu(Request $request)
    {
        $jt_mavzu = new jt_mavzu([
            'nomi' => $request->get('jt_mavzu'),
            'oquv_yili'=>$request->get('oquv_yili'),
            'raqami'=>$request->get('jt_mavzu_raqami'),
            'mutaxassislik_id'=>$request->get('mutaxassislik'),
            'fan_id' => $request->get('fan_id'),
        ]);
        $jt_mavzu->save();
        return redirect()->route('mashgulotlar')->with('success', 'JT Mavzusi qoshildi!');
    }
    public function edit_jt_mavzu(Request $request)
    {
        $jt_mavzu = jt_mavzu::find($request->get('mavzu_nomi'));
        $jt_mavzu->nomi = $request->get('yangi_nomi');
        $jt_mavzu->fan_id = $request->get('fan_id');
        $jt_mavzu->save();
        return redirect()->route('mashgulotlar')->with('success', 'JT Mavzusi qoshildi!');
    }
    public function add_jt_mashgulot(Request $request)
    {
        $jt_mashgulot = new jt_mashgulot([
            'nomi' => $request->get('nomi'),
            'oquv_yili'=>$request->get('oquv_yili'),
            'mutaxassislik_id'=>$request->get('mutaxassislik'),
            'fan_id' => $request->get('jt_fan'),
            'jt_mavzu_id' => $request->get('jt_mavzu_id'),
            'soati'=> $request->get('soati'),
        ]);
        $jt_mashgulot->save();

        $jt_mavzu = jt_mavzu::find($request->get('jt_mavzu_id'));

        $jt_mavzu->soati+=$jt_mashgulot->soati;
        $jt_mavzu->save();
        $jt_fan = jt_fan::find($request->get('jt_fan'));
        $jt_fan->soati+=$jt_mashgulot->soati;
        $jt_fan->save();

        return redirect()->route('mashgulotlar')->with('success', 'Mashgulot qoshildi!');
    }
    public function search(Request $request)
    {
        $jt_fan = $request->get('jt_fan');
        $jt_mavzu = $request->get('jt_mavzu');
        $js_year = $request->get('js_year');
        $jt_mash = $request->get('jt_mash');
        $data = [];
        if (!empty($jt_mash))
            $data = jt_fan::select('id as num', 'nomi')->where('mutaxassislik_id', $jt_mash)->get();
        if (!empty($js_year))
            $data = mutaxassislik::select('id as num', 'nomi')->where('oquv_yili', $js_year)->get();
        if (!empty($jt_fan))
            $data = jt_mavzu::select('id  as num', 'nomi')->where('fan_id', $jt_fan)->get();
        if (!empty($jt_mavzu))
            $data = jt_mashgulot::select('id  as num', 'nomi')->where('jt_mavzu_id', $jt_mavzu)->get();
        $text = '<option value="">tanlang</option>';
        foreach ($data as $datum) {
            $text = $text.'<option value="'.$datum['num'].'">'.$datum['nomi'].'</option>';
        }
        return $text;
    }
}

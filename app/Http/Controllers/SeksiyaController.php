<?php

namespace App\Http\Controllers;

use App\Guruh;
use App\Seksiya;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SeksiyaController extends Controller
{
    public function index()
    {
        $seksiyas = Seksiya::all();
        return view('seksiyas',compact('seksiyas'));
    }
    public function seksiyalar(Request $request)
    {
        $id = $request->route('id');
        $gr = Guruh::find($id);
        $seksiyas = DB::table('seksiyas')->where('guruh_id',$id)->get();
        return view('seksiyalar',['seksiyas'=>$seksiyas,'id'=>$id,'hq'=>$gr]);
    }

    public function create(Request $request)
    {
        $guruhs = Guruh::all();
        $id = $request->get('id');
        return view('add_seksiya',['guruhs'=>$request,'id'=>$id]);
    }
    public function store(Request $request)
    {
        $request->validate([
            'nomi'=>'required',
        ]);
        $seksiya = new Seksiya([
            'nomi' => $request->get('nomi'),
            'guruh_id' => $request->get('id'),
        ]);
        $id = $request->get('id');
        $seksiya->save();
        return redirect()->route('seksiyalar',['id'=>$id])->with('success', "Seksiya qo'shildi!");
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $seksiya = Seksiya::find($id);
        $guruhs = Guruh::all();
        return view('edit_seksiya', ['seksiya' => $seksiya,'guruhs'=>$guruhs]);
    }
    public function update(Request $request, $id)
    {
        $seksiya = Seksiya::find($id);
        $seksiya->nomi = $request->get('nomi');
        $seksiya->guruh_id = $request->get('guruh_id');
        $seksiya->save();
        $id = $request->get('guruh_id');
        return redirect()->route('seksiyalar',['id'=>$id])->with('success', "Seksiya ma'lumoti yangilandi!");
    }

    public function destroy($id)
    {
        $seksiya  = Seksiya::find($id);
        $seksiya->delete();
        return redirect()->back()->with('success', "Seksiya o'chirildi!");
    }
}

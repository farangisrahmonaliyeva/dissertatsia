<?php

namespace App\Http\Controllers;

use App\artileriya_oziyurar;
use App\Btr;
use App\minamyot_va_artileriya;
use App\oqotar_qurol;
use App\Pjm;
use App\Tank;
use Illuminate\Http\Request;

class TankController extends Controller
{
    public function index()
    {
        $tanks = Tank::all();
        return view('tank',compact('tanks'));
    }
    public function btr()
    {
        $btr = Btr::all();
        return view('btr',compact('btr'));
    }
    public function pjm()
    {
        $pjm = Pjm::all();
        return view('pjm',compact('pjm'));
    }
    public function oqotar_qurol()
    {
        $oqotar_qurol = Btr::all();
        return view('oqotar_qurol',compact('oqotar_qurol'));
    }
    public function minamyot()
    {
        $minamyot = Btr::all();
        return view('minamyot',compact('minamyot'));
    }
    public function artileriya_oziyurar()
    {
        $minamyot = Btr::all();
        return view('artileriya_oziyurar',compact('minamyot'));
    }
    public function tankka_qarshi_qurol()
    {
        $minamyot = Btr::all();
        return view('tankka_qarshi_qurol',compact('minamyot'));
    }
    public function store(Request $request)
    {
        $tank = new Tank([
            'marka_nomi'=>$request->get('marka_nomi'),
            'dvegatel_quvvati'=>$request->get('dvegatel_quvvati'),
            'tankning_ogirligi'=>$request->get('tankning_ogirligi'),
            'max_harakat_tezligi'=>$request->get('max_harakat_tezligi'),
            'orqaga_harakatlanish_max_tezligi'=>$request->get('orqaga_harakatlanish_max_tezligi'),
            'umumiy_yurish_zaxirasi'=>$request->get('umumiy_yurish_zaxirasi'),
            'kotarilish_burchagi'=>$request->get('kotarilish_burchagi'),
            'yon_tomonlarga_burilish_burchagi'=>$request->get('yon_tomonlarga_burilish_burchagi'),
            'devorlardan_otish_balandligi'=>$request->get('devorlardan_otish_balandligi'),
            'chuqurlikdan_otish_kengligi'=>$request->get('chuqurlikdan_otish_kengligi'),
            'dvigatelning_kmga_benzin_istemoli'=>$request->get('dvigatelning_kmga_benzin_istemoli'),
            'dvigatelning_soatiga_benzin_istemoli'=>$request->get('dvigatelning_soatiga_benzin_istemoli'),
            'korpus_uzunligi'=>$request->get('korpus_uzunligi'),
            'korpus_kengligi'=>$request->get('korpus_kengligi'),
            'bashnya_qopqogi_boyicha_balandligi'=>$request->get('bashnya_qopqogi_boyicha_balandligi'),
            'ekvivalent_zirx_qalinligi'=>$request->get('ekvivalent_zirx_qalinligi'),
            'niqoblash_granatalarining_soni'=>$request->get('niqoblash_granatalarining_soni'),
            'pushka_kalibri'=>$request->get('pushka_kalibri'),
            'jangovor_toplam'=>$request->get('jangovor_toplam'),
            'otish_surati'=>$request->get('otish_surati'),
            'nishonlarni_yoq_qilish_uzoqligi'=>$request->get('nishonlarni_yoq_qilish_uzoqligi'),
            'gorizont_boyicha_pushkani_nishonga_olish_tezligi'=>$request->get('gorizont_boyicha_pushkani_nishonga_olish_tezligi'),
            'jangovor_toplam_nomenklaturasi'=>$request->get('jangovor_toplam_nomenklaturasi'),
        ])    ;
        $tank->save();
        return back()->with('success',"Tank qo'shildi!");
    }
    public function storeBTR(Request $request)
    {
        $btr = new Btr([
            'marka_nomi'=>$request->get('marka_nomi'),
            'dvegatel_quvvati'=>$request->get('dvegatel_quvvati'),
            'tankning_ogirligi'=>$request->get('tankning_ogirligi'),
            'max_harakat_tezligi'=>$request->get('max_harakat_tezligi'),
            'orqaga_harakatlanish_max_tezligi'=>$request->get('orqaga_harakatlanish_max_tezligi'),
            'umumiy_yurish_zaxirasi'=>$request->get('umumiy_yurish_zaxirasi'),
            'kotarilish_burchagi'=>$request->get('kotarilish_burchagi'),
            'yon_tomonlarga_burilish_burchagi'=>$request->get('yon_tomonlarga_burilish_burchagi'),
            'devorlardan_otish_balandligi'=>$request->get('devorlardan_otish_balandligi'),
            'chuqurlikdan_otish_kengligi'=>$request->get('chuqurlikdan_otish_kengligi'),
            'dvigatelning_kmga_benzin_istemoli'=>$request->get('dvigatelning_kmga_benzin_istemoli'),
            'dvigatelning_soatiga_benzin_istemoli'=>$request->get('dvigatelning_soatiga_benzin_istemoli'),
            'korpus_uzunligi'=>$request->get('korpus_uzunligi'),
            'korpus_kengligi'=>$request->get('korpus_kengligi'),
            'bashnya_qopqogi_boyicha_balandligi'=>$request->get('bashnya_qopqogi_boyicha_balandligi'),
            'ekvivalent_zirx_qalinligi'=>$request->get('ekvivalent_zirx_qalinligi'),
            'niqoblash_granatalarining_soni'=>$request->get('niqoblash_granatalarining_soni'),
            'pushka_kalibri'=>$request->get('pushka_kalibri'),
            'jangovor_toplam'=>$request->get('jangovor_toplam'),
            'otish_surati'=>$request->get('otish_surati'),
            'nishonlarni_yoq_qilish_uzoqligi'=>$request->get('nishonlarni_yoq_qilish_uzoqligi'),
            'gorizont_boyicha_pushkani_nishonga_olish_tezligi'=>$request->get('gorizont_boyicha_pushkani_nishonga_olish_tezligi'),
            'jangovor_toplam_nomenklaturasi'=>$request->get('jangovor_toplam_nomenklaturasi'),
        ])    ;
        $btr->save();
        return back()->with('success',"BTR qo'shildi!");
    }
    public function storePJM(Request $request)
    {
        $btr = new Pjm([
            'marka_nomi'=>$request->get('marka_nomi'),
            'dvegatel_quvvati'=>$request->get('dvegatel_quvvati'),
            'tankning_ogirligi'=>$request->get('tankning_ogirligi'),
            'max_harakat_tezligi'=>$request->get('max_harakat_tezligi'),
            'orqaga_harakatlanish_max_tezligi'=>$request->get('orqaga_harakatlanish_max_tezligi'),
            'umumiy_yurish_zaxirasi'=>$request->get('umumiy_yurish_zaxirasi'),
            'kotarilish_burchagi'=>$request->get('kotarilish_burchagi'),
            'yon_tomonlarga_burilish_burchagi'=>$request->get('yon_tomonlarga_burilish_burchagi'),
            'devorlardan_otish_balandligi'=>$request->get('devorlardan_otish_balandligi'),
            'chuqurlikdan_otish_kengligi'=>$request->get('chuqurlikdan_otish_kengligi'),
            'dvigatelning_kmga_benzin_istemoli'=>$request->get('dvigatelning_kmga_benzin_istemoli'),
            'dvigatelning_soatiga_benzin_istemoli'=>$request->get('dvigatelning_soatiga_benzin_istemoli'),
            'korpus_uzunligi'=>$request->get('korpus_uzunligi'),
            'korpus_kengligi'=>$request->get('korpus_kengligi'),
            'bashnya_qopqogi_boyicha_balandligi'=>$request->get('bashnya_qopqogi_boyicha_balandligi'),
            'ekvivalent_zirx_qalinligi'=>$request->get('ekvivalent_zirx_qalinligi'),
            'niqoblash_granatalarining_soni'=>$request->get('niqoblash_granatalarining_soni'),
            'pushka_kalibri'=>$request->get('pushka_kalibri'),
            'jangovor_toplam'=>$request->get('jangovor_toplam'),
            'otish_surati'=>$request->get('otish_surati'),
            'nishonlarni_yoq_qilish_uzoqligi'=>$request->get('nishonlarni_yoq_qilish_uzoqligi'),
            'gorizont_boyicha_pushkani_nishonga_olish_tezligi'=>$request->get('gorizont_boyicha_pushkani_nishonga_olish_tezligi'),
            'jangovor_toplam_nomenklaturasi'=>$request->get('jangovor_toplam_nomenklaturasi'),
        ]);
        $btr->save();
        return back()->with('success',"BMP qo'shildi!");
    }
    public function storeoqotar_qurol(Request $request)
    {
        $btr = new oqotar_qurol([
            'qurol_ogirligi'=>$request->get('qurol_ogirligi'),
            'inson_max_harakat_tezligi'=>$request->get('inson_max_harakat_tezligi'),
            'inson_orqaga_harakatlanish_max_tezligi'=>$request->get('inson_orqaga_harakatlanish_max_tezligi'),
            'umumiy_yurish_zaxirasi'=>$request->get('umumiy_yurish_zaxirasi'),
            'kotarilish_burchagi'=>$request->get('kotarilish_burchagi'),
            'yon_tomonlarga_burilish_burchagi'=>$request->get('yon_tomonlarga_burilish_burchagi'),
            'devorlardan_otish_balandligi'=>$request->get('devorlardan_otish_balandligi'),
            'chuqurlikdan_otish_kengligi'=>$request->get('chuqurlikdan_otish_kengligi'),
            'qurol_uzunligi'=>$request->get('qurol_uzunligi'),
            'qurol_kalibri'=>$request->get('qurol_kalibri'),
            'magazindagi_oqlar_soni'=>$request->get('magazindagi_oqlar_soni'),
            'otish_surati'=>$request->get('otish_surati'),
            'nishonlarni_yoq_qilish_uzoqligi'=>$request->get('nishonlarni_yoq_qilish_uzoqligi'),
            'jangovor_toplam_nomenklaturasi'=>$request->get('jangovor_toplam_nomenklaturasi'),
        ]);
        $btr->save();
        return back()->with('success',"O'qotar qurol qo'shildi!");
    }
    public function storeminamyot_va_artileriya(Request $request)
    {
        $btr = new minamyot_va_artileriya([
            'shatakka_oluvchi_dvegateli_quvvati'=>$request->get('shatakka_oluvchi_dvegateli_quvvati'),
            'ogirligi'=>$request->get('ogirligi'),
            'shatakka_oluvchining_ogirligi'=>$request->get('shatakka_oluvchining_ogirligi'),
            'max_harakat_tezligi'=>$request->get('max_harakat_tezligi'),
            'orqaga_harakatlanish_max_tezligi'=>$request->get('orqaga_harakatlanish_max_tezligi'),
            'umumiy_yurish_zaxirasi'=>$request->get('umumiy_yurish_zaxirasi'),
            'kotarilish_burchagi'=>$request->get('kotarilish_burchagi'),
            'yon_tomonlarga_burilish_burchagi'=>$request->get('yon_tomonlarga_burilish_burchagi'),
            'devorlardan_otish_balandligi'=>$request->get('devorlardan_otish_balandligi'),
            'chuqurlikdan_otish_kengligi'=>$request->get('chuqurlikdan_otish_kengligi'),
            'shatakka_oluvchi_dvegatelning_km_benzin_istemoli'=>$request->get('shatakka_oluvchi_dvegatelning_km_benzin_istemoli'),
            'shatakka_oluvchi_dvegatelning_soatiga_benzin_istemoli'=>$request->get('shatakka_oluvchi_dvegatelning_soatiga_benzin_istemoli'),
            'korpus_uzunligi'=>$request->get('korpus_uzunligi'),
            'korpus_kengligi'=>$request->get('korpus_kengligi'),
            'bor_boyicha_balandligi'=>$request->get('bor_boyicha_balandligi'),
            'ekvivalent_zirx_qalinligi'=>$request->get('ekvivalent_zirx_qalinligi'),
            'pushka_kalibri'=>$request->get('pushka_kalibri'),
            'jangovor_toplam'=>$request->get('jangovor_toplam'),
            'otish_surati'=>$request->get('otish_surati'),
            'nishonlarni_yoq_qilish_uzoqligi'=>$request->get('nishonlarni_yoq_qilish_uzoqligi'),
            'gorizontal_boyicha_pushkaning_nishonga_olish_tezligi'=>$request->get('gorizontal_boyicha_pushkaning_nishonga_olish_tezligi'),
            'snaryadlarning_talofat_yetkazish_boyicha_kengligi'=>$request->get('snaryadlarning_talofat_yetkazish_boyicha_kengligi'),
        ]);
        $btr->save();
        return back()->with('success',"Minamyot va artileriya qo'shildi!");
    }
    public function storeartileriya_oziyurar(Request $request)
    {
        $arti = new artileriya_oziyurar([
            'dvegatel_quvvati'=>$request->get('dvegatel_quvvati'),
            'ogirligi'=>$request->get('ogirligi'),
            'max_harakat_tezligi'=>$request->get('max_harakat_tezligi'),
            'orqaga_harakatlanish_max_tezligi'=>$request->get('orqaga_harakatlanish_max_tezligi'),
            'umumiy_yurish_zaxirasi'=>$request->get('umumiy_yurish_zaxirasi'),
            'kotarilish_burchagi'=>$request->get('kotarilish_burchagi'),
            'yon_tomonlarga_burilish_burchagi'=>$request->get('yon_tomonlarga_burilish_burchagi'),
            'devorlardan_otish_balandligi'=>$request->get('devorlardan_otish_balandligi'),
            'chuqurlikdan_otish_kengligi'=>$request->get('chuqurlikdan_otish_kengligi'),
            'dvigatelning_kmga_benzin_istemoli'=>$request->get('dvigatelning_kmga_benzin_istemoli'),
            'dvigatelning_soatiga_benzin_istemoli'=>$request->get('dvigatelning_soatiga_benzin_istemoli'),
            'korpus_uzunligi'=>$request->get('korpus_uzunligi'),
            'korpus_kengligi'=>$request->get('korpus_kengligi'),
            'bor_boyicha_balandligi'=>$request->get('bor_boyicha_balandligi'),
            'ekvivalent_zirx_qalinligi'=>$request->get('ekvivalent_zirx_qalinligi'),
            'niqoblash_granatalarining_soni'=>$request->get('niqoblash_granatalarining_soni'),
            'pushka_kalibri'=>$request->get('pushka_kalibri'),
            'jangovor_toplam'=>$request->get('jangovor_toplam'),
            'otish_surati'=>$request->get('otish_surati'),
            'nishonlarni_yoq_qilish_uzoqligi'=>$request->get('nishonlarni_yoq_qilish_uzoqligi'),
            'gorizont_boyicha_pushkani_nishonga_olish_tezligi'=>$request->get('gorizont_boyicha_pushkani_nishonga_olish_tezligi'),
            'jangovor_toplam_nomenklaturasi'=>$request->get('jangovor_toplam_nomenklaturasi'),

        ]);
        $arti->save();
        return back()->with('success',"O'ziyurar artileriya qo'shildi!");
    }
}

<?php

namespace App\Http\Controllers;

use App\Battalions;
use App\Vzvod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VzvodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vzvods = Vzvod::all();
        return view('vzvods',compact('vzvods'));
    }
    public function vzvodlar(Request $request)
    {
        $id = $request->route('id');
        $bt = Battalions::find($id);
        $vzvods = DB::table('vzvods')->where('battalion_id',$id)->get();
        return view('vzvodlar',['vzvods'=>$vzvods,'id'=>$id,'hq'=>$bt]);
    }
    public function create(Request $request)
    {
        $id = $request->get('id');
        $battalions = DB::table('battalions')->where('id',$id)->get();
        return view('add_vzvod',['battalions'=>$battalions,'id'=>$id]);
    }

    public function store(Request $request)
    {

        $vzvod = new Vzvod([
            'nomi' => $request->get('nomi'),
            'battalion_id' => $request->get('id'),
        ]);
        $id = $request->get('id');
        $vzvod->save();
        return redirect()->route('vzvodlar',['id'=>$id])->with('success', 'Vzvod qoshildi!');
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $vzvod = Vzvod::find($id);
        $battalions = Battalions::all();
        return view('edit_vzvod', ['vzvod' => $vzvod,'battalions'=>$battalions]);
    }

    public function update(Request $request, $id)
    {
        $vzvod  = Vzvod::find($id);
        $vzvod->nomi = $request->get('nomi');
        $vzvod->battalion_id = $request->get('battalion_id');
        $vzvod->save();
        $id = $request->get('battalion_id');
        return redirect()->route('vzvodlar',['id'=>$id])->with('success', 'Vzvod malumoti yangilandi!');

    }

    public function destroy($id)
    {
        $vzvod = Vzvod::find($id);
        $vzvod->delete();
        return redirect()->back()->with('success', "Vzvod o'chirildi!");
    }
}

<?php

namespace App\Http\Controllers;

use App\harakatchanlik_o_k;
use App\himoyalanganlik_o_k;
use App\qurollanishi_o_k;
use Illuminate\Http\Request;

class o_kController extends Controller
{
    public function index()
    {
        return view('ogirlik_koeffitsenti');
    }
    public function store_harakatchanlik(Request $request)
    {
        $harakatchanlik = new harakatchanlik_o_k([
            'solishtirma_quvvati'=>$request->get('solishtirma_quvvati'),
            'yerga_berilgan_bosim'=>$request->get('yerga_berilgan_bosim'),
            'max_harakat_tezligi'=>$request->get('max_harakat_tezligi'),
            'orqaga_harakatlanish_max_tezligi'=>$request->get('orqaga_harakatlanish_max_tezligi'),
            'umumiy_yurish_zaxirasi'=>$request->get('umumiy_yurish_zaxirasi'),
            'kotarilish_burchagi'=>$request->get('kotarilish_burchagi'),
            'yon_tomonlarga_burilish_burchagi'=>$request->get('yon_tomonlarga_burilish_burchagi'),
            'devorlardan_otish_burchagi'=>$request->get('devorlardan_otish_burchagi'),
            'chuqurlikdan_otish_kengligi'=>$request->get('chuqurlikdan_otish_kengligi')
        ]);
        $harakatchanlik->save();
        return back()->with('success', "Harakatchanlik o.k qo'shildi");
    }
    public function store_himoyalanganlik(Request $request)
    {
        $himoyalanganlik = new himoyalanganlik_o_k([
            'korpus_uzunligi'=>$request->get('korpus_uzunligi'),
            'korpus_kengligi'=>$request->get('korpus_kengligi'),
            'bashnya_qopqogi_boyicha_balandligi'=>$request->get('bashnya_qopqogi_boyicha_balandligi'),
            'ekvivalent_zirx_qalinligi'=>$request->get('ekvivalent_zirx_qalinligi'),
            'niqoblash_granatalarining_soni'=>$request->get('niqoblash_granatalarining_soni'),
           ]);
        $himoyalanganlik->save();
        return back()->with('success', "Ximoyalanganlik o.k qo'shildi");
    }
    public function store_qurollanish(Request $request)
    {
        $qurollanishi = new qurollanishi_o_k([
            'pushka_kalibri'=>$request->get('pushka_kalibri'),
            'jangovor_toplam'=>$request->get('jangovor_toplam'),
            'otish_surati'=>$request->get('otish_surati'),
            'nishonlarni_yoq_qilish_uzoqligi'=>$request->get('nishonlarni_yoq_qilish_uzoqligi'),
            'gorizont_boyicha_pushkani_nishonga_olish_tezligi'=>$request->get('gorizont_boyicha_pushkani_nishonga_olish_tezligi'),
            'jangovor_toplam_nomenklaturasi'=>$request->get('jangovor_toplam_nomenklaturasi'),
        ]);
        $qurollanishi->save();
        return back()->with('success', "Qurollanishi o.k qo'shildi");
    }
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

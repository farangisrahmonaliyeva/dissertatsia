<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JismoniyHolati extends Model
{
    protected $fillable=['arh','jangchi_id','charchaganlik_darajasi','soglomligi','umumiy_baho','vaqti','end','2erkin_mashqlar_toplami','1erkin_mashqlar_toplami','press_qolni_bukish','lapkost','3_kmga_yugurish','1_kmga_yugurish','100_metrga_yugurish','turnik'];
}

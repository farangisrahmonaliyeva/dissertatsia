<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mashgulot extends Model
{
    public function mavzu()
    {
        return $this->belongsTo(Mavzu::class);
    }
    public function fan()
    {
        return $this->belongsTo(Fan::class);
    }


}

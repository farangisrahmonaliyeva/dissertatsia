<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mavzu extends Model
{
    public function fan()
    {
        return $this->belongsTo(Fan::class);
    }

    public function mashgulots()
    {
        return $this->hasMany(Mashgulot::class);
    }
}

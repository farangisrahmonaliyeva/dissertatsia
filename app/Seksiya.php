<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seksiya extends Model
{
    public function guruh()
    {
        return $this->belongsTo('App\Guruh');
    }

    public function jangchilar()
    {
        return $this->hasMany('App\Vzvod');
    }
    protected $fillable = ['nomi','guruh_id'];
}

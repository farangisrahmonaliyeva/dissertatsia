<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tank extends Model
{
    protected $fillable =
        [
            'marka_nomi',
            'dvegatel_quvvati',
            'tankning_ogirligi',
            'max_harakat_tezligi',
            'orqaga_harakatlanish_max_tezligi',
            'umumiy_yurish_zaxirasi',
            'kotarilish_burchagi',
            'yon_tomonlarga_burilish_burchagi',
            'devorlardan_otish_balandligi',
            'chuqurlikdan_otish_kengligi',
            'dvigatelning_kmga_benzin_istemoli',
            'dvigatelning_soatiga_benzin_istemoli',
            'korpus_uzunligi',
            'korpus_kengligi',
            'bashnya_qopqogi_boyicha_balandligi',
            'ekvivalent_zirx_qalinligi',
            'niqoblash_granatalarining_soni',
            'pushka_kalibri',
            'jangovor_toplam',
            'otish_surati',
            'nishonlarni_yoq_qilish_uzoqligi',
            'gorizont_boyicha_pushkani_nishonga_olish_tezligi',
            'jangovor_toplam_nomenklaturasi',
            'solishtirma_quvvati',
            'harakatchanlik',
            'himoyalanganlik',
            'qurollanganlik',
            'yerga_berilgan_bosim'
        ];
}

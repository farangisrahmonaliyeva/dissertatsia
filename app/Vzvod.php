<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vzvod extends Model
{
    public function battalion()
    {
        return $this->belongsTo(Battalions::class);
    }

    public function guruhs()
    {
        return $this->hasMany(Guruh::class);
    }
    public function jangchilars()
{
    return $this->hasMany(jangchilar::class);
}
    protected $fillable = ['nomi','battalion_id'];
}

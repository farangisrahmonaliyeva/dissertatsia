<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class charchaganlik_darajasi extends Model
{
    protected $fillable = ['jangchi_id', 'charchaganlik_darajasi'];
}

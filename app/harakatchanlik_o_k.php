<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class harakatchanlik_o_k extends Model
{
    protected $fillable =
        [
            'solishtirma_quvvati',
            'yerga_berilgan_bosim',
            'max_harakat_tezligi',
            'orqaga_harakatlanish_max_tezligi',
            'umumiy_yurish_zaxirasi',
            'kotarilish_burchagi',
            'yon_tomonlarga_burilish_burchagi',
            'devorlardan_otish_burchagi',
            'chuqurlikdan_otish_kengligi'
        ];
}

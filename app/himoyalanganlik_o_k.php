<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class himoyalanganlik_o_k extends Model
{
    protected $fillable =
        [
            'korpus_uzunligi',
            'korpus_kengligi',
            'bashnya_qopqogi_boyicha_balandligi',
            'ekvivalent_zirx_qalinligi',
            'niqoblash_granatalarining_soni',
        ];
}

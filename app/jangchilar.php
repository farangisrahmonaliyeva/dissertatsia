<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jangchilar extends Model
{
    public function seksiya()
    {
        return $this->belongsTo('App\Seksiya');
    }
    public function guruh()
    {
        return $this->belongsTo('App\Guruh');
    }
    public function vzvod()
    {
        return $this->belongsTo('App\Vzvod');
    }
    public function battalion()
    {
        return $this->belongsTo('App\Battalions');
    }
    public function harbiyqism()
    {
        $harbiy_qism = HarbiyQism::find($this->harbiy_qism_id);
        if (empty($harbiy_qism))
            return '';
        return $harbiy_qism['nomi'];
//        return '';
    }


    protected $fillable = [
                                'unvoni',
                                'familyasi',
                                'ismi',
                                'sharifi',
                                'jinsi',
                                'tugilgan_vaqti',
                                'ish_joyi',
                                'boyi',
                                'vazni',
                                'harbiy_qism_id',
                                'battalion_id',
                                'vzvod_id',
                                'guruh_id',
                                'seksiya_id'
                            ];
}

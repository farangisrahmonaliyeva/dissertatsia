<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jt__baholash extends Model
{
    protected $fillable = ['jt_mash_id','jangchi_id','bahosi','jt_vaqti','end','oquv_yili','jt_fan_id','jt_mavzu_id','ozlashtirish','mutaxassislik'];
}

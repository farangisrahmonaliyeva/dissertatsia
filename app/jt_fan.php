<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jt_fan extends Model
{
    public function mutaxassislik()
    {
        return $this->belongsTo(mutaxassislik::class);
    }
    public function jt_mavzus()
    {
        return $this->hasMany(jt_mavzu::class, 'fan_id', 'id');
    }
    protected $fillable = ['nomi','oquv_yili','mutaxassislik_id','soati'];
}

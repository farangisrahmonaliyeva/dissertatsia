<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jt_mashgulot extends Model
{
    public function mutaxassislik()
    {
        return $this->belongsTo(mutaxassislik::class);
    }

    public function jt_mavzu()
    {
        return $this->belongsTo(jt_mavzu::class);
    }

    public function jt__baholashes()
    {
        return $this->hasMany(jt__baholash::class);
    }
    protected $fillable = ['nomi','jt_mavzu_id','soati','mutaxassislik_id','oquv_yili','fan_id'];
}

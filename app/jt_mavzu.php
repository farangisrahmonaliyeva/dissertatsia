<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jt_mavzu extends Model
{
    public function mutaxassislik()
    {
        return $this->belongsTo(mutaxassislik::class);
    }
    public function jt_fan()
    {
        return $this->belongsTo(jt_fan::class);
    }
    public function jt_mashgulots()
    {
        return $this->hasMany(jt_mashgulot::class);
    }
    protected $fillable = ['nomi','fan_id','soati','raqami','oquv_yili','mutaxassislik_id'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jt_mavzu_bahosi extends Model
{
    protected $fillable = ['jangchi_id','fan_id','mavzu_id','oquv_yili','mutaxassislik','bahosi','vaqti'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class minamyot_va_artileriya extends Model
{
    protected $fillable = [
        'shatakka_oluvchi_dvegateli_quvvati',
        'ogirligi',
        'shatakka_oluvchining_ogirligi',
        'max_harakat_tezligi',
        'orqaga_harakatlanish_max_tezligi',
        'umumiy_yurish_zaxirasi',
        'kotarilish_burchagi',
        'yon_tomonlarga_burilish_burchagi',
        'devorlardan_otish_balandligi',
        'chuqurlikdan_otish_kengligi',
        'shatakka_oluvchi_dvegatelning_km_benzin_istemoli',
        'shatakka_oluvchi_dvegatelning_soatiga_benzin_istemoli',
        'korpus_uzunligi',
        'korpus_kengligi',
        'bor_boyicha_balandligi',
        'ekvivalent_zirx_qalinligi',
        'pushka_kalibri',
        'jangovor_toplam',
        'otish_surati',
        'nishonlarni_yoq_qilish_uzoqligi',
        'gorizontal_boyicha_pushkaning_nishonga_olish_tezligi',
        'solishtirma_quvvati',
        'yerga_berilgan_bosim',
        'bashnya_qopqogi_boyicha_balandligi',
        'snaryadlarning_talofat_yetkazish_boyicha_kengligi',
    ];
}

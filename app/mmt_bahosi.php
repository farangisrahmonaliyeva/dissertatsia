<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mmt_bahosi extends Model
{
    protected $fillable = ['jangchi_id','oquv_yili','bahosi','vaqti'];
}

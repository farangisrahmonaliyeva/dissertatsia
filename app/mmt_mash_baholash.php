<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mmt_mash_baholash extends Model
{
    public function mmt_mashgulot()
    {
        return $this->belongsTo(mmt_mashgulot::class);
    }
    public function jangchi()
    {
        return $this->belongsTo(jangchilar::class);
    }
    protected $fillable = ['mmt_mash_id','jangchi_id','bahosi','mmt_vaqti','oquv_yili','mmt_mavzu_id'];
}

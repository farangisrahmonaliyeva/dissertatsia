<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mmt_mashgulot extends Model
{
    public function mmt_mavzu()
    {
        return $this->belongsTo(mmt_mavzu::class);
    }

    public function mmt_mash_baholashes()
    {
        return $this->hasMany(mmt_mash_baholash::class);
    }
    protected $fillable = ['nomi','mmt_mavzu_id','soati'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mmt_mavzu extends Model
{
    public function mmt_mashgulots()
    {
        return $this->hasMany(mmt_mashgulot::class);
    }
    protected $fillable = ['nomi','soati','mavzu_raqami','oquv_yili'];
}

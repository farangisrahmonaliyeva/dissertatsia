<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mmt_mavzu_bahosi extends Model
{
    protected $fillable = ['jangchi_id','mavzu_id','mavzu_bahosi','oquv_yili'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mutaxassislik extends Model
{
 protected $fillable  = ['nomi','oquv_yili'];
    public function jt_fans()
    {
        return $this->hasMany(jt_fan::class, 'mutaxassislik_id', 'id');
    }
    public function jt_mavzus()
    {
        return $this->hasMany(jt_mavzu::class, 'mutaxassislik_id', 'id');
    }
    public function jt_mashgulots()
    {
        return $this->hasMany(jt_mashgulot::class, 'mutaxassislik_id', 'id');
    }
}

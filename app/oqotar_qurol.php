<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class oqotar_qurol extends Model
{
   protected $fillable = [
        'qurol_ogirligi',
        'inson_max_harakat_tezligi',
        'inson_orqaga_harakatlanish_max_tezligi',
        'umumiy_yurish_zaxirasi',
        'kotarilish_burchagi',
        'yon_tomonlarga_burilish_burchagi',
        'devorlardan_otish_balandligi',
        'chuqurlikdan_otish_kengligi',
        'qurol_uzunligi',
        'qurol_kalibri',
        'magazindagi_oqlar_soni',
        'otish_surati',
        'nishonlarni_yoq_qilish_uzoqligi',
        'jangovor_toplam_nomenklaturasi',
        'solishtirma_quvvati',
        'yerga_berilgan_bosim',
        'max_harakat_tezligi',
        'orqaga_harakatlanish_max_tezligi',
        'qurol_kengligi',
        'inson_balandligi',
        'bronjilet_qalinligi',
        'pushka_kalibri',
        'gorizontal_boyicha_pushkani_nishonga_olish_tezligi'
   ];
}

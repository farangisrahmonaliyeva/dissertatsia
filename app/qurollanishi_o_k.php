<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class qurollanishi_o_k extends Model
{
    protected $fillable =
        [
            'pushka_kalibri',
            'jangovor_toplam',
            'otish_surati',
            'nishonlarni_yoq_qilish_uzoqligi',
            'gorizont_boyicha_pushkani_nishonga_olish_tezligi',
            'jangovor_toplam_nomenklaturasi'
        ];
}

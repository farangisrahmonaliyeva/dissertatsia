<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tanks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('marka_nomi');
            $table->double('dvegatel_quvvati', 8, 2);
            $table->double('texnikaning_ogirligi', 8, 2);
            $table->double('max_harakat_tezligi', 8, 2);
            $table->double('orqaga_harakatlanish_max_tezligi', 8, 2);
            $table->double('umumiy_yurish_zaxirasi', 8, 2);
            $table->double('kotarilish_burchagi', 8, 2);
            $table->double('yon_tomonlarga_burilish_burchagi', 8, 2);
            $table->double('devorlardan_otish_balandligi', 8, 2);
            $table->double('chuqurlikdan_otish_kengligi', 8, 2);
            $table->double('dvigatelning_soatiga_benzin_istemoli', 8, 2);
            $table->integer('ekipaj_soni');
            $table->double('solishtirma_quvvati', 8, 2);
            $table->integer('jangovor_toplam_nomenklaturasi');
            $table->double('pushka_kalibri', 8, 2);
            $table->integer('jangovor_toplam');
            $table->double('otish_surati', 8, 2);
            $table->double('nishonlarni_yoq_qilish_uzoqligi', 8, 2);
            $table->double('gorizont_boyicha_pushkani_nishonga_olish_tezligi', 8, 2);
            $table->double('korpus_uzunligi', 8, 2);
            $table->double('korpus_kengligi', 8, 2);
            $table->double('bashnya_qopqogi_boyicha_balandligi', 8, 2);
            $table->double('ekvivalent_zirx_qalinligi', 8, 2);
            $table->double('niqoblanish_darajasi', 8, 2);
            $table->integer('niqoblash_granatalarining_soni');
            $table->double('harakatchanlik', 8, 2);
            $table->double('himoyalanganlik', 8, 2);
            $table->double('qurollanganlik', 8, 2);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tanks');
    }
}

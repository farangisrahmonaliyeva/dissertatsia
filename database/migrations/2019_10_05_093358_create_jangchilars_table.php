<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJangchilarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jangchilars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unvoni');
            $table->string('familyasi');
            $table->string('ismi');
            $table->string('sharifi');
            $table->integer('jinsi');
            $table->date('tugilgan_vaqti');
            $table->string('ish_joyi');
            $table->integer('seksiya_id')->unsigned();
            $table->foreign('seksiya_id')->references('id')->on('seksiyas')
                ->onDelete('cascade');
            $table->integer('guruh_id')->unsigned();
            $table->foreign('guruh_id')->references('id')->on('guruhs')
                ->onDelete('cascade');
            $table->integer('vzvod_id')->unsigned();
            $table->foreign('vzvod_id')->references('id')->on('vzvods')
                ->onDelete('cascade');
            $table->integer('battalion_id')->unsigned();
            $table->foreign('battalion_id')->references('id')->on('battalions')
                ->onDelete('cascade');
            $table->integer('harbiy_qism_id')->unsigned();
            $table->foreign('harbiy_qism_id')->references('id')->on('harbiy_qisms')
                ->onDelete('cascade');
            $table->integer('boyi');
            $table->double('vazni','8','2');
            $table->double('jangovor_qobilyati','8','2');
            $table->double('soglomligi','8','2');
            $table->double('jismoniy_tayyorgarligi','8','2');
            $table->double('jismoniy_xolati','8','2');
            $table->double('charchaganlik_darajasi','8','2');
            $table->double('manaviy_ruhiy_holati','8','2');
            $table->double('ahloqiy_ruhiy_xolati','8','2');
            $table->double('mmt','8','2');
            $table->double('jangovor_tayyorgarligi','8','2');
            $table->double('mmt_vaqti','8','2');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jangchilars');
    }
}

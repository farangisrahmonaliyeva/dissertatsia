<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVzvodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vzvods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomi');
            $table->integer('battalion_id')->unsigned();
            $table->foreign('battalion_id')->references('id')->on('battalions')
                ->onDelete('cascade');
            $table->string('jangovor_qobilyati');
            $table->double('jismoniy_holati','8','2');
            $table->double('manaviy_ruxiy_holati','8','2');
            $table->double('jangovor_tayyorgarligi');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vzvods');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuruhsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guruhs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomi');
            $table->integer('vzvod_id')->unsigned();
            $table->foreign('vzvod_id')->references('id')->on('vzvods')
                ->onDelete('cascade');
            $table->string('jangovor_qobilyati');
            $table->double('jismoniy_holati','8','2');
            $table->double('manaviy_ruxiy_holati','8','2');
            $table->double('jangovor_tayyorgarligi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guruhs');
    }
}

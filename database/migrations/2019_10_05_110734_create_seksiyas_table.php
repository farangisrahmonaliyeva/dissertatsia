<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeksiyasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seksiyas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('guruh_id')->unsigned();
            $table->foreign('guruh_id')->references('id')->on('guruhs')
                ->onDelete('cascade');
            $table->string('nomi');
            $table->string('jangovor_qobilyati');
            $table->double('jismoniy_holati','8','2');
            $table->double('manaviy_ruxiy_holati','8','2');
            $table->double('jangovor_tayyorgarligi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seksiyas');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHarbiyQismsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harbiy_qisms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomi');
            $table->integer('battalionlar_soni');
            $table->string('jangovor_qobilyati');
            $table->double('jismoniy_holati','8','2');
            $table->double('manaviy_ruxiy_holati','8','2');
            $table->double('jangovor_tayyorgarligi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harbiy_qisms');
    }
}

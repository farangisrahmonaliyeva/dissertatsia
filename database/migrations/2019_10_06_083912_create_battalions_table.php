<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBattalionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(/**
         * @param Blueprint $table
         */
            'battalions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomi');
            $table->integer('harbiy_qism_id')->unsigned();
            $table->string('jangovor_qobilyati');
            $table->double('jismoniy_holati','8','2');
            $table->double('manaviy_ruxiy_holati','8','2');
            $table->double('jangovor_tayyorgarligi');
            $table->timestamps();
            $table->foreign('harbiy_qism_id')->references('id')->on('harbiy_qisms')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battalions');
    }
}

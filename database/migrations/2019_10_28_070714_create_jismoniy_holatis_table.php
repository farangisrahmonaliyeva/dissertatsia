<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJismoniyHolatisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jismoniy_holatis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('jangchi_id');
            $table->integer('turnik');
            $table->integer('100_metrga_yugurish');
            $table->integer('1_kmga_yugurish');
            $table->integer('3_kmga_yugurish');
            $table->integer('lapkost');
            $table->integer('press_qolni_bukish');
            $table->integer('1erkin_mashqlar_toplami');
            $table->integer('2erkin_mashqlar_toplami');
            $table->integer('umumiy_baho');
            $table->integer('charchaganlik_darajasi');
            $table->dateTime('vaqti');
            $table->integer('end');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jismoniy_holatis');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmtMashBaholashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmt_mash_baholashes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('mmt_mash_id');
            $table->integer('jangchi_id');
            $table->integer('bahosi');
            $table->dateTime('mmt_vaqti');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmt_mash_baholashes');
    }
}

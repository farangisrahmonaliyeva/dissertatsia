<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmtMashgulotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmt_mashgulots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomi');
            $table->integer('mmt_mavzu_id');
            $table->integer('soati');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmt_mashgulots');
    }
}

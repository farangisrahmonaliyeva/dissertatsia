<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJtBaholashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jt__baholashes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('jt_mash_id');
            $table->integer('jangchi_id');
            $table->integer('bahosi');
            $table->dateTime('jt_vaqti');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jt__baholashes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharchaganlikDarajasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charchaganlik_darajasis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('jangchi_id');
            $table->integer('charchaganlik_darajasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charchaganlik_darajasis');
    }
}

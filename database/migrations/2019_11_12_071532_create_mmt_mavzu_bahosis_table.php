<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmtMavzuBahosisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmt_mavzu_bahosis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('jangchi_id');
            $table->integer('mavzu_id');
            $table->float('mavzu_bahosi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmt_mavzu_bahosis');
    }
}

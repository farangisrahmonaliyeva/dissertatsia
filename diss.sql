-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 22 2019 г., 07:34
-- Версия сервера: 5.7.19
-- Версия PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `diss`
--

-- --------------------------------------------------------

--
-- Структура таблицы `arhs`
--

DROP TABLE IF EXISTS `arhs`;
CREATE TABLE IF NOT EXISTS `arhs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jangchi_id` int(11) NOT NULL,
  `arh` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `arhs`
--

INSERT INTO `arhs` (`id`, `jangchi_id`, `arh`, `created_at`, `updated_at`) VALUES
(4, 24, 3, '2019-11-19 05:21:36', '2019-11-19 05:21:36'),
(3, 24, 4, '2019-11-19 05:07:17', '2019-11-19 05:07:17'),
(5, 25, 2, '2019-11-21 05:04:42', '2019-11-21 05:04:42'),
(6, 24, 4, '2019-11-22 00:10:57', '2019-11-22 00:10:57');

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `arh_ends`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `arh_ends`;
CREATE TABLE IF NOT EXISTS `arh_ends` (
`id` bigint(20) unsigned
,`jangchi_id` int(11)
,`arh` int(11)
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Структура таблицы `battalions`
--

DROP TABLE IF EXISTS `battalions`;
CREATE TABLE IF NOT EXISTS `battalions` (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harbiy_qism_id` int(10) UNSIGNED NOT NULL,
  `jangovor_qobilyati` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jismoniy_holati` double(8,2) DEFAULT NULL,
  `manaviy_ruxiy_holati` double(8,2) DEFAULT NULL,
  `jangovor_tayyorgarligi` double DEFAULT NULL,
  `shaxsiy_tarkib_soni` bigint(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `harbiy_qism_id` (`harbiy_qism_id`,`nomi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `battalions`
--

INSERT INTO `battalions` (`id`, `nomi`, `harbiy_qism_id`, `jangovor_qobilyati`, `jismoniy_holati`, `manaviy_ruxiy_holati`, `jangovor_tayyorgarligi`, `shaxsiy_tarkib_soni`, `created_at`, `updated_at`) VALUES
(20, '1.1 batalyon', 23, NULL, NULL, NULL, NULL, 1, '2019-11-18 07:17:54', '2019-11-21 23:54:30'),
(21, '1.2 batalyon', 23, NULL, NULL, NULL, NULL, NULL, '2019-11-18 07:18:05', '2019-11-18 07:18:05'),
(22, '2.1 batalyon', 24, NULL, NULL, NULL, NULL, 2, '2019-11-18 07:18:33', '2019-11-19 01:04:17'),
(23, '2.2 batalyon', 24, NULL, NULL, NULL, NULL, NULL, '2019-11-18 07:18:47', '2019-11-18 07:18:47'),
(24, 'aloqa vzvodi', 24, NULL, NULL, NULL, NULL, NULL, '2019-11-18 07:19:06', '2019-11-18 07:19:06'),
(25, 'ta\'mirlash guruhi', 24, NULL, NULL, NULL, NULL, 1, '2019-11-18 07:19:18', '2019-11-18 07:24:53');

-- --------------------------------------------------------

--
-- Структура таблицы `btrs`
--

DROP TABLE IF EXISTS `btrs`;
CREATE TABLE IF NOT EXISTS `btrs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `charchaganliks`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `charchaganliks`;
CREATE TABLE IF NOT EXISTS `charchaganliks` (
`id` bigint(20) unsigned
,`jangchi_id` int(11)
,`charchaganlik_darajasi` int(11)
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Структура таблицы `charchaganlik_darajasis`
--

DROP TABLE IF EXISTS `charchaganlik_darajasis`;
CREATE TABLE IF NOT EXISTS `charchaganlik_darajasis` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jangchi_id` int(11) NOT NULL,
  `charchaganlik_darajasi` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `charchaganlik_darajasis`
--

INSERT INTO `charchaganlik_darajasis` (`id`, `jangchi_id`, `charchaganlik_darajasi`, `created_at`, `updated_at`) VALUES
(10, 25, 3, '2019-11-21 05:05:23', '2019-11-21 05:05:23'),
(9, 24, 5, '2019-11-19 05:21:05', '2019-11-19 05:21:05'),
(8, 24, 1, '2019-11-19 05:20:54', '2019-11-19 05:20:54'),
(7, 24, 4, '2019-11-19 05:20:42', '2019-11-19 05:20:42'),
(6, 24, 4, '2019-11-19 04:44:25', '2019-11-19 04:44:25'),
(11, 27, 1, '2019-11-21 23:54:49', '2019-11-21 23:54:49');

-- --------------------------------------------------------

--
-- Структура таблицы `guruhs`
--

DROP TABLE IF EXISTS `guruhs`;
CREATE TABLE IF NOT EXISTS `guruhs` (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vzvod_id` int(10) UNSIGNED NOT NULL,
  `jangovor_qobilyati` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jismoniy_holati` double(8,2) DEFAULT NULL,
  `manaviy_ruxiy_holati` double(8,2) DEFAULT NULL,
  `jangovor_tayyorgarligi` double DEFAULT NULL,
  `shaxsiy_tarkib_soni` bigint(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `guruhs_ibfk_1` (`vzvod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `guruhs`
--

INSERT INTO `guruhs` (`id`, `nomi`, `vzvod_id`, `jangovor_qobilyati`, `jismoniy_holati`, `manaviy_ruxiy_holati`, `jangovor_tayyorgarligi`, `shaxsiy_tarkib_soni`, `created_at`, `updated_at`) VALUES
(18, '1331 guruh', 19, NULL, NULL, NULL, NULL, 2, '2019-11-18 07:20:33', '2019-11-19 01:04:18'),
(19, '0061 seksiyasi', 19, NULL, NULL, NULL, NULL, NULL, '2019-11-18 07:20:47', '2019-11-18 07:20:47');

-- --------------------------------------------------------

--
-- Структура таблицы `harbiy_qisms`
--

DROP TABLE IF EXISTS `harbiy_qisms`;
CREATE TABLE IF NOT EXISTS `harbiy_qisms` (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `battalionlar_soni` int(11) DEFAULT NULL,
  `jangovor_qobilyati` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jismoniy_holati` double(8,2) DEFAULT NULL,
  `manaviy_ruxiy_holati` double(8,2) DEFAULT NULL,
  `jangovor_tayyorgarligi` double DEFAULT NULL,
  `shaxsiy_tarkib_soni` bigint(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `harbiy_qisms`
--

INSERT INTO `harbiy_qisms` (`id`, `nomi`, `battalionlar_soni`, `jangovor_qobilyati`, `jismoniy_holati`, `manaviy_ruxiy_holati`, `jangovor_tayyorgarligi`, `shaxsiy_tarkib_soni`, `created_at`, `updated_at`) VALUES
(23, '11121 harbiy qism', NULL, NULL, NULL, NULL, NULL, 1, '2019-11-18 07:17:08', '2019-11-21 23:54:30'),
(24, '22211 harbiy qism', NULL, NULL, NULL, NULL, NULL, 3, '2019-11-18 07:17:23', '2019-11-19 01:04:17');

-- --------------------------------------------------------

--
-- Структура таблицы `history`
--

DROP TABLE IF EXISTS `history`;
CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL,
  `jangchi_id` int(10) DEFAULT NULL,
  `jismoniy_holati` int(10) DEFAULT NULL,
  `jismoniy_tayyorgarligi` int(10) DEFAULT NULL,
  `charchaganlik_darajasi` int(10) DEFAULT NULL,
  `jh_vaqti` date DEFAULT NULL,
  `mmt` int(10) DEFAULT NULL,
  `arh` int(10) DEFAULT NULL,
  `mrh` int(10) DEFAULT NULL,
  `mrh_vaqti` date DEFAULT NULL,
  `jt` int(10) DEFAULT NULL,
  `jt_vaqti` date DEFAULT NULL,
  `created_at` timestamp(6) NULL DEFAULT NULL,
  `updated_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `jangchilars`
--

DROP TABLE IF EXISTS `jangchilars`;
CREATE TABLE IF NOT EXISTS `jangchilars` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `unvoni` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `familyasi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ismi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sharifi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jinsi` int(11) NOT NULL,
  `tugilgan_vaqti` date DEFAULT NULL,
  `ish_joyi` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seksiya_id` int(10) UNSIGNED DEFAULT NULL,
  `guruh_id` int(10) UNSIGNED DEFAULT NULL,
  `vzvod_id` int(10) UNSIGNED DEFAULT NULL,
  `battalion_id` int(10) UNSIGNED DEFAULT NULL,
  `harbiy_qism_id` int(10) UNSIGNED NOT NULL,
  `boyi` int(11) DEFAULT NULL,
  `vazni` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jangchilars_ibfk_1` (`harbiy_qism_id`),
  KEY `jangchilars_ibfk_2` (`battalion_id`),
  KEY `jangchilars_ibfk_3` (`vzvod_id`),
  KEY `jangchilars_ibfk_4` (`guruh_id`),
  KEY `seksiya_id` (`seksiya_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jangchilars`
--

INSERT INTO `jangchilars` (`id`, `unvoni`, `familyasi`, `ismi`, `sharifi`, `jinsi`, `tugilgan_vaqti`, `ish_joyi`, `seksiya_id`, `guruh_id`, `vzvod_id`, `battalion_id`, `harbiy_qism_id`, `boyi`, `vazni`, `created_at`, `updated_at`) VALUES
(24, 'kapitan', 'Meliqo\'ziyev', 'Rustam', 'Shuxrat o\'gli', 1, '1990-11-04', 'guruh kamandiri', 25, 18, 19, 22, 24, 188, 90.00, '2019-11-18 07:23:07', '2019-11-19 00:20:35'),
(25, 'leytenant', 'Qosimov', 'Javohir', 'Alisherivich', 1, '1992-11-11', 'o\'qchi', NULL, NULL, NULL, 25, 24, 180, 83.00, '2019-11-18 07:24:53', '2019-11-18 07:24:53'),
(26, 'ShAHX oddiy askar', 'test', '1', 'test', 1, '2019-10-28', 'test', 25, 18, 19, 22, 24, 111, 90.00, '2019-11-19 01:04:17', '2019-11-19 01:04:17'),
(27, 'kichik serjant', 'test', '1', 'test', 1, '2019-10-28', 'test', NULL, NULL, NULL, 20, 23, 180, 90.00, '2019-11-21 23:54:30', '2019-11-21 23:54:30');

-- --------------------------------------------------------

--
-- Структура таблицы `jangovor_qobiliyats`
--

DROP TABLE IF EXISTS `jangovor_qobiliyats`;
CREATE TABLE IF NOT EXISTS `jangovor_qobiliyats` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jangchi_id` int(255) DEFAULT NULL,
  `jismoniy_holati` int(255) DEFAULT NULL,
  `soglomlik_darajasi` int(255) DEFAULT NULL,
  `charchaganlik_darajasi` int(255) DEFAULT NULL,
  `jismoniy_tayyorgarligi` int(255) DEFAULT NULL,
  `mrh` int(255) DEFAULT NULL,
  `arh` int(255) DEFAULT NULL,
  `mmt` int(255) DEFAULT NULL,
  `jangovor_tayyorgarligi` int(255) DEFAULT NULL,
  `created_at` timestamp(6) NOT NULL,
  `updated_at` timestamp(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `jangovor_tayyorgarliks`
--

DROP TABLE IF EXISTS `jangovor_tayyorgarliks`;
CREATE TABLE IF NOT EXISTS `jangovor_tayyorgarliks` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jangchi_id` int(12) DEFAULT NULL,
  `mutaxassislik` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oquv_yili` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ball` float(255,0) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jangovor_tayyorgarliks`
--

INSERT INTO `jangovor_tayyorgarliks` (`id`, `jangchi_id`, `mutaxassislik`, `oquv_yili`, `ball`, `created_at`, `updated_at`) VALUES
(1, 24, 'mergan', '2016-2017', 8, '2019-11-20 08:09:48', '2019-11-20 08:09:48'),
(2, 24, 'mergan', '2016-2017', 8, '2019-11-20 11:11:06', '2019-11-20 11:11:06'),
(3, 24, 'mergan', '2018-2019', 2, '2019-11-20 21:37:29', '2019-11-20 21:37:29'),
(4, 24, 'mergan', '2016-2017', 10, '2019-11-20 22:04:25', '2019-11-20 22:04:25'),
(5, 25, 'o\'qchi', '2018-2019', 4, '2019-11-21 05:05:10', '2019-11-21 05:05:10'),
(6, 27, 'mergan', '2016-2017', 3, '2019-11-21 23:55:55', '2019-11-21 23:55:55'),
(7, 27, 'mergan', '2017-2018', 3, '2019-11-21 23:56:12', '2019-11-21 23:56:12'),
(8, 27, 'mergan', '2018-2019', 2, '2019-11-21 23:56:24', '2019-11-21 23:56:24');

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `jangovor_tayyorgarlik_ozlashtirishes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `jangovor_tayyorgarlik_ozlashtirishes`;
CREATE TABLE IF NOT EXISTS `jangovor_tayyorgarlik_ozlashtirishes` (
`id` bigint(20) unsigned
,`jangchi_id` int(12)
,`mutaxassislik` varchar(255)
,`oquv_yili` varchar(255)
,`ball` float(255,0)
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Структура таблицы `jismoniy_holatis`
--

DROP TABLE IF EXISTS `jismoniy_holatis`;
CREATE TABLE IF NOT EXISTS `jismoniy_holatis` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jangchi_id` int(11) DEFAULT NULL,
  `turnik` int(11) DEFAULT NULL,
  `100_metrga_yugurish` int(11) DEFAULT NULL,
  `1_kmga_yugurish` int(11) DEFAULT NULL,
  `3_kmga_yugurish` int(11) DEFAULT NULL,
  `lapkost` int(11) DEFAULT NULL,
  `press_qolni_bukish` int(11) DEFAULT NULL,
  `1erkin_mashqlar_toplami` int(11) DEFAULT NULL,
  `2erkin_mashqlar_toplami` int(11) DEFAULT NULL,
  `umumiy_baho` int(11) DEFAULT NULL,
  `vaqti` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jismoniy_holatis`
--

INSERT INTO `jismoniy_holatis` (`id`, `jangchi_id`, `turnik`, `100_metrga_yugurish`, `1_kmga_yugurish`, `3_kmga_yugurish`, `lapkost`, `press_qolni_bukish`, `1erkin_mashqlar_toplami`, `2erkin_mashqlar_toplami`, `umumiy_baho`, `vaqti`, `created_at`, `updated_at`) VALUES
(11, 27, 3, 4, 3, 4, 2, 4, 5, 4, 3, NULL, '2019-11-21 23:55:05', '2019-11-21 23:55:05'),
(10, 25, 4, 4, 4, 4, 4, 4, 4, 4, 4, NULL, '2019-11-21 05:05:42', '2019-11-21 05:05:42'),
(9, 24, 5, 5, 5, 5, 5, 5, 5, 5, 5, NULL, '2019-11-19 04:46:45', '2019-11-19 04:46:45');

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `jismoniy_holats`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `jismoniy_holats`;
CREATE TABLE IF NOT EXISTS `jismoniy_holats` (
`id` bigint(20) unsigned
,`jangchi_id` int(11)
,`turnik` int(11)
,`100_metrga_yugurish` int(11)
,`1_kmga_yugurish` int(11)
,`3_kmga_yugurish` int(11)
,`lapkost` int(11)
,`press_qolni_bukish` int(11)
,`1erkin_mashqlar_toplami` int(11)
,`2erkin_mashqlar_toplami` int(11)
,`umumiy_baho` int(11)
,`vaqti` datetime
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Структура таблицы `jis_hol_bahos`
--

DROP TABLE IF EXISTS `jis_hol_bahos`;
CREATE TABLE IF NOT EXISTS `jis_hol_bahos` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jangchi_id` int(11) NOT NULL,
  `baho` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jis_hol_bahos`
--

INSERT INTO `jis_hol_bahos` (`id`, `jangchi_id`, `baho`, `created_at`, `updated_at`) VALUES
(27, 27, 60.00, '2019-11-21 23:54:45', '2019-11-21 23:54:45'),
(26, 25, 180.00, '2019-11-21 05:05:42', '2019-11-21 05:05:42'),
(25, 25, 100.00, '2019-11-21 05:05:24', '2019-11-21 05:05:24'),
(24, 25, 40.00, '2019-11-21 05:05:20', '2019-11-21 05:05:20'),
(23, 24, 300.00, '2019-11-19 05:21:05', '2019-11-19 05:21:05'),
(22, 24, 220.00, '2019-11-19 05:20:54', '2019-11-19 05:20:54'),
(21, 24, 280.00, '2019-11-19 05:20:42', '2019-11-19 05:20:42'),
(20, 24, 280.00, '2019-11-19 05:20:33', '2019-11-19 05:20:33'),
(19, 24, 280.00, '2019-11-19 04:54:03', '2019-11-19 04:54:03'),
(18, 24, 220.00, '2019-11-19 04:47:42', '2019-11-19 04:47:42'),
(17, 24, 180.00, '2019-11-19 04:46:45', '2019-11-19 04:46:45'),
(16, 24, 80.00, '2019-11-19 04:44:25', '2019-11-19 04:44:25'),
(28, 27, 80.00, '2019-11-21 23:54:49', '2019-11-21 23:54:49'),
(29, 27, 140.00, '2019-11-21 23:55:05', '2019-11-21 23:55:05');

-- --------------------------------------------------------

--
-- Структура таблицы `jt_fans`
--

DROP TABLE IF EXISTS `jt_fans`;
CREATE TABLE IF NOT EXISTS `jt_fans` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mutaxassislik` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oquv_yili` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jt_fans`
--

INSERT INTO `jt_fans` (`id`, `nomi`, `mutaxassislik`, `oquv_yili`, `created_at`, `updated_at`) VALUES
(9, 'otish tayyorgarligi', NULL, NULL, '2019-11-18 07:46:01', '2019-11-18 07:46:01'),
(10, 'taktika', NULL, NULL, '2019-11-18 07:46:26', '2019-11-18 07:46:26'),
(11, 'muxandislik tayyorgarligi', NULL, NULL, '2019-11-18 07:47:06', '2019-11-18 07:47:06'),
(12, 'razvedka', NULL, NULL, '2019-11-18 07:47:18', '2019-11-18 07:47:18');

-- --------------------------------------------------------

--
-- Структура таблицы `jt_fan_bahosis`
--

DROP TABLE IF EXISTS `jt_fan_bahosis`;
CREATE TABLE IF NOT EXISTS `jt_fan_bahosis` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jangchi_id` int(255) DEFAULT NULL,
  `mutaxassislik` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oquv_yili` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fan_id` int(11) DEFAULT NULL,
  `bahosi` float(255,0) DEFAULT NULL,
  `vaqti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jt_fan_bahosis`
--

INSERT INTO `jt_fan_bahosis` (`id`, `jangchi_id`, `mutaxassislik`, `oquv_yili`, `fan_id`, `bahosi`, `vaqti`, `created_at`, `updated_at`) VALUES
(5, 24, 'mergan', '2016-2017', 10, 3, '2019-11-20', '2019-11-20 03:34:03', '2019-11-20 03:34:03'),
(4, 24, 'mergan', '2016-2017', 9, 3, '2019-11-20', '2019-11-20 03:16:28', '2019-11-20 03:16:28'),
(6, 24, 'mergan', '2016-2017', 10, 2, '2019-11-20', '2019-11-20 08:02:36', '2019-11-20 08:02:36'),
(7, 24, 'mergan', '2016-2017', 10, 5, '2019-11-20', '2019-11-20 08:09:10', '2019-11-20 08:09:10'),
(8, 24, 'mergan', '2016-2017', 10, 5, '2019-11-20', '2019-11-20 08:09:48', '2019-11-20 08:09:48'),
(9, 24, 'mergan', '2016-2017', 10, 5, '2019-11-20', '2019-11-20 11:11:06', '2019-11-20 11:11:06'),
(10, 24, 'mergan', '2018-2019', 9, 2, '2019-11-21', '2019-11-20 21:37:29', '2019-11-20 21:37:29'),
(11, 24, 'mergan', '2016-2017', 9, 5, '2019-11-21', '2019-11-20 22:04:25', '2019-11-20 22:04:25'),
(12, 25, 'o\'qchi', '2018-2019', 10, 4, '2019-11-21', '2019-11-21 05:05:10', '2019-11-21 05:05:10'),
(13, 27, 'mergan', '2016-2017', 9, 3, '2019-11-22', '2019-11-21 23:55:54', '2019-11-21 23:55:54'),
(14, 27, 'mergan', '2017-2018', 9, 3, '2019-11-22', '2019-11-21 23:56:12', '2019-11-21 23:56:12'),
(15, 27, 'mergan', '2018-2019', 9, 2, '2019-11-22', '2019-11-21 23:56:24', '2019-11-21 23:56:24');

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `jt_fan_ozlashtirishes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `jt_fan_ozlashtirishes`;
CREATE TABLE IF NOT EXISTS `jt_fan_ozlashtirishes` (
`id` bigint(20) unsigned
,`jangchi_id` int(255)
,`mutaxassislik` varchar(255)
,`oquv_yili` varchar(255)
,`fan_id` int(11)
,`bahosi` float(255,0)
,`vaqti` varchar(255)
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Структура таблицы `jt_mashgulots`
--

DROP TABLE IF EXISTS `jt_mashgulots`;
CREATE TABLE IF NOT EXISTS `jt_mashgulots` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mutaxassislik_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jt_mavzu_id` int(11) NOT NULL,
  `soati` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jt_mashgulots`
--

INSERT INTO `jt_mashgulots` (`id`, `nomi`, `mutaxassislik_id`, `jt_mavzu_id`, `soati`, `created_at`, `updated_at`) VALUES
(13, 'Amaliy o\'q otish', 'o\'qchi', 8, 4, '2019-11-18 07:55:22', '2019-11-18 07:55:22'),
(14, 'Mudofaa', 'mergan', 9, 7, '2019-11-18 07:56:26', '2019-11-18 07:56:26'),
(15, 'Mina to\'siqlaridan o\'tish', 'o\'qchi', 10, 5, '2019-11-18 07:56:54', '2019-11-18 07:56:54'),
(16, '1-mashgulot', 'o\'qchi', 8, 2, '2019-11-19 15:21:06', '2019-11-19 15:21:06');

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `jt_mash_ozlashtirishes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `jt_mash_ozlashtirishes`;
CREATE TABLE IF NOT EXISTS `jt_mash_ozlashtirishes` (
`id` bigint(20) unsigned
,`oquv_yili` varchar(255)
,`jt_fan_id` int(6)
,`jt_mavzu_id` int(11)
,`jt_mash_id` int(11)
,`jangchi_id` int(11)
,`bahosi` int(11)
,`ozlashtirish` float(255,0)
,`mutaxassislik` varchar(255)
,`jt_vaqti` datetime
,`end` int(11)
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Структура таблицы `jt_mavzus`
--

DROP TABLE IF EXISTS `jt_mavzus`;
CREATE TABLE IF NOT EXISTS `jt_mavzus` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `raqami` int(255) DEFAULT NULL,
  `nomi` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fan_id` int(11) DEFAULT NULL,
  `soati` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asas` (`fan_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jt_mavzus`
--

INSERT INTO `jt_mavzus` (`id`, `raqami`, `nomi`, `fan_id`, `soati`, `created_at`, `updated_at`) VALUES
(10, 1, 'Minalardan o\'tish', 11, 5, '2019-11-18 07:52:17', '2019-11-18 07:56:54'),
(9, 1, 'Jangchining mudofadgi harakati', 10, 7, '2019-11-18 07:51:28', '2019-11-18 07:56:26'),
(8, 1, '1-o\'quv o\'q otish mashqi', 9, 6, '2019-11-18 07:50:48', '2019-11-19 15:21:06'),
(11, 1, 'Dushman haqida ma\'lumotlar yig\'ish', 12, NULL, '2019-11-18 07:52:47', '2019-11-18 07:52:47'),
(14, 1, 'razvedka', 12, NULL, '2019-11-19 15:20:45', '2019-11-19 15:20:45'),
(13, 2, '2-mavzu', 9, NULL, '2019-11-19 08:00:27', '2019-11-19 08:00:27');

-- --------------------------------------------------------

--
-- Структура таблицы `jt_mavzu_bahosis`
--

DROP TABLE IF EXISTS `jt_mavzu_bahosis`;
CREATE TABLE IF NOT EXISTS `jt_mavzu_bahosis` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jangchi_id` int(11) DEFAULT NULL,
  `fan_id` int(255) DEFAULT NULL,
  `oquv_yili` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mutaxassislik` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mavzu_id` int(255) DEFAULT NULL,
  `bahosi` float(255,0) DEFAULT NULL,
  `vaqti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jt_mavzu_bahosis`
--

INSERT INTO `jt_mavzu_bahosis` (`id`, `jangchi_id`, `fan_id`, `oquv_yili`, `mutaxassislik`, `mavzu_id`, `bahosi`, `vaqti`, `created_at`, `updated_at`) VALUES
(14, 24, 10, '2016-2017', 'mergan', 9, 5, NULL, '2019-11-20 08:09:48', '2019-11-20 08:09:48'),
(13, 24, 10, '2016-2017', 'mergan', 9, 5, NULL, '2019-11-20 08:09:10', '2019-11-20 08:09:10'),
(12, 24, 10, '2016-2017', 'mergan', 9, 2, NULL, '2019-11-20 08:02:36', '2019-11-20 08:02:36'),
(11, 24, 10, '2016-2017', 'mergan', 9, 3, NULL, '2019-11-20 03:34:03', '2019-11-20 03:34:03'),
(10, 24, 9, '2016-2017', 'mergan', 8, 3, NULL, '2019-11-20 03:16:28', '2019-11-20 03:16:28'),
(15, 24, 10, '2016-2017', 'mergan', 9, 5, NULL, '2019-11-20 11:11:06', '2019-11-20 11:11:06'),
(16, 24, 9, '2018-2019', 'mergan', 8, 2, NULL, '2019-11-20 21:37:29', '2019-11-20 21:37:29'),
(17, 24, 9, '2016-2017', 'mergan', 8, 5, NULL, '2019-11-20 22:04:25', '2019-11-20 22:04:25'),
(18, 25, 10, '2018-2019', 'o\'qchi', 9, 4, NULL, '2019-11-21 05:05:10', '2019-11-21 05:05:10'),
(19, 27, 9, '2016-2017', 'mergan', 8, 3, NULL, '2019-11-21 23:55:54', '2019-11-21 23:55:54'),
(20, 27, 9, '2017-2018', 'mergan', 8, 3, NULL, '2019-11-21 23:56:12', '2019-11-21 23:56:12'),
(21, 27, 9, '2018-2019', 'mergan', 8, 2, NULL, '2019-11-21 23:56:24', '2019-11-21 23:56:24');

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `jt_mavzu_ozlashtirishes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `jt_mavzu_ozlashtirishes`;
CREATE TABLE IF NOT EXISTS `jt_mavzu_ozlashtirishes` (
`id` bigint(20) unsigned
,`jangchi_id` int(11)
,`fan_id` int(255)
,`oquv_yili` varchar(255)
,`mutaxassislik` varchar(255)
,`mavzu_id` int(255)
,`bahosi` float(255,0)
,`vaqti` varchar(255)
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Структура таблицы `jt__baholashes`
--

DROP TABLE IF EXISTS `jt__baholashes`;
CREATE TABLE IF NOT EXISTS `jt__baholashes` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oquv_yili` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jt_fan_id` int(6) DEFAULT NULL,
  `jt_mavzu_id` int(11) DEFAULT NULL,
  `jt_mash_id` int(11) DEFAULT NULL,
  `jangchi_id` int(11) DEFAULT NULL,
  `bahosi` int(11) DEFAULT NULL,
  `ozlashtirish` float(255,0) DEFAULT NULL,
  `mutaxassislik` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jt_vaqti` datetime DEFAULT NULL,
  `end` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jt__baholashes`
--

INSERT INTO `jt__baholashes` (`id`, `oquv_yili`, `jt_fan_id`, `jt_mavzu_id`, `jt_mash_id`, `jangchi_id`, `bahosi`, `ozlashtirish`, `mutaxassislik`, `jt_vaqti`, `end`, `created_at`, `updated_at`) VALUES
(32, '2017-2018', 9, 8, 13, 27, 4, 3, 'mergan', '2019-11-22 00:00:00', NULL, '2019-11-21 23:56:12', '2019-11-21 23:56:12'),
(30, '2018-2019', 10, 9, 14, 25, 4, 4, 'o\'qchi', '2019-11-21 00:00:00', NULL, '2019-11-21 05:05:10', '2019-11-21 05:05:10'),
(31, '2016-2017', 9, 8, 13, 27, 5, 3, 'mergan', '2019-11-22 00:00:00', NULL, '2019-11-21 23:55:54', '2019-11-21 23:55:54'),
(28, '2018-2019', 9, 8, 13, 24, 3, 2, 'mergan', '2019-11-21 00:00:00', NULL, '2019-11-20 21:37:29', '2019-11-20 21:37:29'),
(29, '2016-2017', 9, 8, 16, 24, 5, 2, 'mergan', '2019-11-21 00:00:00', NULL, '2019-11-20 22:04:25', '2019-11-20 22:04:25'),
(27, '2016-2017', 10, 9, 14, 24, 5, 5, 'mergan', '2019-11-20 00:00:00', NULL, '2019-11-20 11:11:05', '2019-11-20 11:11:06'),
(26, '2016-2017', 10, 9, 14, 24, 5, 5, 'mergan', '2019-11-20 00:00:00', NULL, '2019-11-20 08:09:48', '2019-11-20 08:09:48'),
(25, '2016-2017', 10, 9, 14, 24, 5, 5, 'mergan', '2019-11-20 00:00:00', NULL, '2019-11-20 08:09:10', '2019-11-20 08:09:10'),
(24, '2016-2017', 10, 9, 14, 24, 2, 2, 'mergan', '2019-11-20 00:00:00', NULL, '2019-11-20 08:02:36', '2019-11-20 08:02:36'),
(23, '2016-2017', 10, 9, 14, 24, 3, 3, 'mergan', '2019-11-20 00:00:00', NULL, '2019-11-20 03:34:03', '2019-11-20 03:34:03'),
(22, '2016-2017', 9, 8, 13, 24, 5, 3, 'mergan', '2019-11-20 00:00:00', NULL, '2019-11-20 03:16:28', '2019-11-20 03:16:28'),
(33, '2018-2019', 9, 8, 13, 27, 3, 2, 'mergan', '2019-11-22 00:00:00', NULL, '2019-11-21 23:56:24', '2019-11-21 23:56:24');

-- --------------------------------------------------------

--
-- Структура таблицы `mashgulots`
--

DROP TABLE IF EXISTS `mashgulots`;
CREATE TABLE IF NOT EXISTS `mashgulots` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mavzu_id` int(11) NOT NULL,
  `fan_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `mavzus`
--

DROP TABLE IF EXISTS `mavzus`;
CREATE TABLE IF NOT EXISTS `mavzus` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fan_id` int(11) NOT NULL,
  `mashgulot_soati` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(61, '2014_10_12_000000_create_users_table', 1),
(62, '2014_10_12_100000_create_password_resets_table', 1),
(63, '2019_10_03_131207_create_tanks_table', 1),
(64, '2019_10_03_140849_create_btrs_table', 1),
(65, '2019_10_05_093331_create_pjms_table', 1),
(66, '2019_10_05_093358_create_jangchilars_table', 1),
(67, '2019_10_05_110535_create_vzvods_table', 1),
(68, '2019_10_05_110635_create_guruhs_table', 1),
(69, '2019_10_05_110734_create_seksiyas_table', 1),
(70, '2019_10_05_110943_create_mashgulots_table', 1),
(71, '2019_10_06_081954_create_harbiy_qisms_table', 1),
(72, '2019_10_06_083912_create_battalions_table', 1),
(73, '2019_10_10_074834_create_mavzus_table', 1),
(74, '2019_10_10_075841_create_fans_table', 1),
(75, '2019_10_10_082143_create_mmts_table', 1),
(76, '2019_10_17_070445_create_jangovor_qobiliyats_table', 2),
(77, '2019_10_28_055343_create_m_m_t_s_table', 2),
(78, '2019_10_28_070714_create_jismoniy_holatis_table', 2),
(79, '2019_10_28_185222_create_mmt_mash_baholashes_table', 2),
(80, '2019_10_28_193037_create_mmt_mavzus_table', 2),
(81, '2019_10_28_193702_create_mmt_mashgulots_table', 2),
(82, '2019_10_28_193852_create_jt__baholashes_table', 2),
(83, '2019_10_28_193954_create_jt_fans_table', 2),
(84, '2019_10_28_194040_create_jt_mavzus_table', 2),
(85, '2019_10_28_194149_create_jt_mashgulots_table', 2),
(86, '2019_11_12_031252_create_soglomligis_table', 3),
(87, '2019_11_12_042846_create_arhs_table', 4),
(88, '2019_11_12_044029_create_charchaganlik_darajasis_table', 5),
(89, '2019_11_12_050211_create_jis_hol_bahos_table', 6),
(90, '2019_11_12_071532_create_mmt_mavzu_bahosis_table', 7),
(91, '2019_11_13_083911_create_mmt_ozlashtirishes_table', 8),
(92, '2019_11_13_142525_create_mmt_mavzu_ozlashtirishes_table', 9),
(93, '2019_11_13_210849_create_mmt_bahosis_table', 10),
(94, '2019_11_14_122258_create_jt_mavzu_bahosis_table', 11),
(95, '2019_11_14_131528_create_mutaxassisliks_table', 12),
(96, '2019_11_14_133913_create_jt_mash_ozlashtirishes_table', 13),
(97, '2019_11_14_141453_create_jt_mavzu_ozlashtirishes_table', 14),
(98, '2019_11_15_130031_create_jt_fan_bahosis_table', 15),
(99, '2019_11_18_041929_create_jt_fan_ozlashtirishes_table', 16),
(100, '2019_11_18_054309_create_charchaganliks_table', 17),
(101, '2019_11_20_125327_create_jangovor_tayyorgarliks_table', 18);

-- --------------------------------------------------------

--
-- Структура таблицы `mmt_bahosis`
--

DROP TABLE IF EXISTS `mmt_bahosis`;
CREATE TABLE IF NOT EXISTS `mmt_bahosis` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jangchi_id` int(8) DEFAULT NULL,
  `oquv_yili` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahosi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vaqti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `mmt_bahosis`
--

INSERT INTO `mmt_bahosis` (`id`, `jangchi_id`, `oquv_yili`, `bahosi`, `vaqti`, `created_at`, `updated_at`) VALUES
(10, 25, '2018-2019', '3', '2019-11-21', '2019-11-21 05:04:54', '2019-11-21 05:04:54'),
(9, 24, '2017-2018', '5', '2019-11-21', '2019-11-20 22:28:51', '2019-11-20 22:28:51'),
(8, 24, '2018-2019', '5', '2019-11-21', '2019-11-20 22:27:48', '2019-11-20 22:27:48'),
(7, 24, '2018-2019', '5', '2019-11-21', '2019-11-20 21:43:08', '2019-11-20 21:43:08'),
(6, 24, '2016-2017', '2', '2019-11-19', '2019-11-19 07:00:05', '2019-11-19 07:00:05'),
(11, 27, '2016-2017', '4', '2019-11-22', '2019-11-21 23:55:23', '2019-11-21 23:55:23'),
(12, 27, '2017-2018', '4', '2019-11-22', '2019-11-21 23:55:31', '2019-11-21 23:55:31'),
(13, 27, '2018-2019', '2', '2019-11-22', '2019-11-21 23:55:40', '2019-11-21 23:55:40');

-- --------------------------------------------------------

--
-- Структура таблицы `mmt_mashgulots`
--

DROP TABLE IF EXISTS `mmt_mashgulots`;
CREATE TABLE IF NOT EXISTS `mmt_mashgulots` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mmt_mavzu_id` int(11) NOT NULL,
  `soati` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `mmt_mashgulots`
--

INSERT INTO `mmt_mashgulots` (`id`, `nomi`, `mmt_mavzu_id`, `soati`, `created_at`, `updated_at`) VALUES
(20, '1-mashgulot', 19, 4, '2019-11-21 23:57:20', '2019-11-21 23:57:20'),
(19, '1-mashgulot', 18, 2, '2019-11-20 05:39:50', '2019-11-20 05:39:50'),
(18, 'Andijon voqealari.', 17, 4, '2019-11-18 07:35:56', '2019-11-18 07:35:56'),
(17, 'Terroristik aktlar.', 17, 6, '2019-11-18 07:35:19', '2019-11-18 07:35:19'),
(16, 'Prezident asarlari.', 16, 4, '2019-11-18 07:34:27', '2019-11-18 07:34:27');

-- --------------------------------------------------------

--
-- Структура таблицы `mmt_mash_baholashes`
--

DROP TABLE IF EXISTS `mmt_mash_baholashes`;
CREATE TABLE IF NOT EXISTS `mmt_mash_baholashes` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jangchi_id` int(11) DEFAULT NULL,
  `oquv_yili` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mmt_mavzu_id` int(6) DEFAULT NULL,
  `mmt_mash_id` int(11) DEFAULT NULL,
  `bahosi` int(11) DEFAULT NULL,
  `ozlashtirish` float(10,0) DEFAULT NULL,
  `mmt_vaqti` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `mmt_mash_baholashes`
--

INSERT INTO `mmt_mash_baholashes` (`id`, `jangchi_id`, `oquv_yili`, `mmt_mavzu_id`, `mmt_mash_id`, `bahosi`, `ozlashtirish`, `mmt_vaqti`, `created_at`, `updated_at`) VALUES
(89, 27, '2018-2019', 17, 18, 4, 2, '2019-11-22 00:00:00', '2019-11-21 23:55:40', '2019-11-21 23:55:40'),
(88, 27, '2017-2018', 18, 19, 4, 4, '2019-11-22 00:00:00', '2019-11-21 23:55:31', '2019-11-21 23:55:31'),
(87, 27, '2016-2017', 18, 19, 4, 4, '2019-11-22 00:00:00', '2019-11-21 23:55:23', '2019-11-21 23:55:23'),
(86, 25, '2018-2019', 18, 19, 3, 3, '2019-11-21 00:00:00', '2019-11-21 05:04:54', '2019-11-21 05:04:54'),
(85, 24, '2017-2018', 18, 19, 5, 5, '2019-11-21 00:00:00', '2019-11-20 22:28:51', '2019-11-20 22:28:51'),
(83, 24, '2018-2019', 18, 19, 5, 5, '2019-11-21 00:00:00', '2019-11-20 21:43:08', '2019-11-20 21:43:08'),
(84, 24, '2018-2019', 18, 19, 5, 5, '2019-11-21 00:00:00', '2019-11-20 22:27:48', '2019-11-20 22:27:48'),
(82, 24, '2016-2017', 17, 18, 5, 2, '2019-11-19 00:00:00', '2019-11-19 07:00:05', '2019-11-19 07:00:05');

-- --------------------------------------------------------

--
-- Структура таблицы `mmt_mavzus`
--

DROP TABLE IF EXISTS `mmt_mavzus`;
CREATE TABLE IF NOT EXISTS `mmt_mavzus` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mavzu_raqami` int(255) NOT NULL,
  `nomi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `soati` int(6) DEFAULT NULL,
  `oquv_yili` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `mmt_mavzus`
--

INSERT INTO `mmt_mavzus` (`id`, `mavzu_raqami`, `nomi`, `soati`, `oquv_yili`, `created_at`, `updated_at`) VALUES
(18, 1, '1-mavzu', 2, NULL, '2019-11-20 04:58:59', '2019-11-20 05:39:50'),
(17, 2, 'Xavfsizlikka tahdid.', 10, '2016-2017', '2019-11-18 07:34:56', '2019-11-18 07:35:57'),
(19, 4, 'sasasasa', 4, NULL, '2019-11-21 23:57:11', '2019-11-21 23:57:20');

-- --------------------------------------------------------

--
-- Структура таблицы `mmt_mavzu_bahosis`
--

DROP TABLE IF EXISTS `mmt_mavzu_bahosis`;
CREATE TABLE IF NOT EXISTS `mmt_mavzu_bahosis` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jangchi_id` int(11) NOT NULL,
  `oquv_yili` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mavzu_id` int(11) NOT NULL,
  `mavzu_bahosi` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `mmt_mavzu_bahosis`
--

INSERT INTO `mmt_mavzu_bahosis` (`id`, `jangchi_id`, `oquv_yili`, `mavzu_id`, `mavzu_bahosi`, `created_at`, `updated_at`) VALUES
(30, 27, '2018-2019', 17, 2.00, '2019-11-21 23:55:40', '2019-11-21 23:55:40'),
(29, 27, '2017-2018', 18, 4.00, '2019-11-21 23:55:31', '2019-11-21 23:55:31'),
(28, 27, '2016-2017', 18, 4.00, '2019-11-21 23:55:23', '2019-11-21 23:55:23'),
(27, 25, '2018-2019', 18, 3.00, '2019-11-21 05:04:54', '2019-11-21 05:04:54'),
(26, 24, '2017-2018', 18, 5.00, '2019-11-20 22:28:51', '2019-11-20 22:28:51'),
(25, 24, '2018-2019', 18, 5.00, '2019-11-20 22:27:48', '2019-11-20 22:27:48'),
(24, 24, '2018-2019', 18, 5.00, '2019-11-20 21:43:08', '2019-11-20 21:43:08'),
(23, 24, '2016-2017', 17, 2.00, '2019-11-19 07:00:05', '2019-11-19 07:00:05');

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `mmt_mavzu_ozlashtirishes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `mmt_mavzu_ozlashtirishes`;
CREATE TABLE IF NOT EXISTS `mmt_mavzu_ozlashtirishes` (
`id` bigint(20) unsigned
,`jangchi_id` int(11)
,`oquv_yili` varchar(255)
,`mavzu_id` int(11)
,`mavzu_bahosi` double(8,2)
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `mmt_ozlashtirishes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `mmt_ozlashtirishes`;
CREATE TABLE IF NOT EXISTS `mmt_ozlashtirishes` (
`id` bigint(20) unsigned
,`jangchi_id` int(11)
,`oquv_yili` varchar(255)
,`mmt_mavzu_id` int(6)
,`mmt_mash_id` int(11)
,`bahosi` int(11)
,`ozlashtirish` float(10,0)
,`mmt_vaqti` datetime
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Структура таблицы `mutaxassisliks`
--

DROP TABLE IF EXISTS `mutaxassisliks`;
CREATE TABLE IF NOT EXISTS `mutaxassisliks` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `mutaxassisliks`
--

INSERT INTO `mutaxassisliks` (`id`, `nomi`, `created_at`, `updated_at`) VALUES
(4, 'mergan', '2019-11-18 07:43:45', '2019-11-18 07:43:45'),
(3, 'o\'qchi', '2019-11-18 07:43:35', '2019-11-18 07:43:35'),
(5, NULL, '2019-11-20 04:54:56', '2019-11-20 04:54:56'),
(6, 'asadad dsadasdsadsadasdsadsadasdsadasdasdasdadsad', '2019-11-22 00:20:11', '2019-11-22 00:20:11'),
(7, 'jnjnojnjn', '2019-11-22 00:20:35', '2019-11-22 00:20:35');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pjms`
--

DROP TABLE IF EXISTS `pjms`;
CREATE TABLE IF NOT EXISTS `pjms` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `seksiyas`
--

DROP TABLE IF EXISTS `seksiyas`;
CREATE TABLE IF NOT EXISTS `seksiyas` (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `guruh_id` int(10) UNSIGNED DEFAULT NULL,
  `nomi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jangovor_qobilyati` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jismoniy_holati` double(8,2) DEFAULT NULL,
  `manaviy_ruxiy_holati` double(8,2) DEFAULT NULL,
  `jangovor_tayyorgarligi` double DEFAULT NULL,
  `shaxsiy_tarkib_soni` bigint(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `guruh_id` (`guruh_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `seksiyas`
--

INSERT INTO `seksiyas` (`id`, `guruh_id`, `nomi`, `jangovor_qobilyati`, `jismoniy_holati`, `manaviy_ruxiy_holati`, `jangovor_tayyorgarligi`, `shaxsiy_tarkib_soni`, `created_at`, `updated_at`) VALUES
(25, 18, '00071 seksiya', NULL, NULL, NULL, NULL, 2, '2019-11-18 07:21:08', '2019-11-19 01:04:18'),
(26, 18, '0007711 seksiyasi', NULL, NULL, NULL, NULL, NULL, '2019-11-18 07:21:26', '2019-11-18 07:21:26');

-- --------------------------------------------------------

--
-- Структура таблицы `soglomligis`
--

DROP TABLE IF EXISTS `soglomligis`;
CREATE TABLE IF NOT EXISTS `soglomligis` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `jangchi_id` int(5) NOT NULL,
  `soglomligi` int(5) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `soglomligis`
--

INSERT INTO `soglomligis` (`id`, `jangchi_id`, `soglomligi`, `created_at`, `updated_at`) VALUES
(87, 27, 3, '2019-11-21 23:54:45', '2019-11-21 23:54:45'),
(86, 25, 2, '2019-11-21 05:05:20', '2019-11-21 05:05:20'),
(85, 24, 5, '2019-11-19 05:20:33', '2019-11-19 05:20:33'),
(84, 24, 5, '2019-11-19 04:54:03', '2019-11-19 04:54:03'),
(83, 24, 2, '2019-11-19 04:47:42', '2019-11-19 04:47:42');

-- --------------------------------------------------------

--
-- Структура таблицы `tanks`
--

DROP TABLE IF EXISTS `tanks`;
CREATE TABLE IF NOT EXISTS `tanks` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `marka_nomi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dvegatel_quvvati` double(8,2) NOT NULL,
  `texnikaning_ogirligi` double(8,2) NOT NULL,
  `max_harakat_tezligi` double(8,2) NOT NULL,
  `orqaga_harakatlanish_max_tezligi` double(8,2) NOT NULL,
  `umumiy_yurish_zaxirasi` double(8,2) NOT NULL,
  `kotarilish_burchagi` double(8,2) NOT NULL,
  `yon_tomonlarga_burilish_burchagi` double(8,2) NOT NULL,
  `devorlardan_otish_balandligi` double(8,2) NOT NULL,
  `chuqurlikdan_otish_kengligi` double(8,2) NOT NULL,
  `dvigatelning_soatiga_benzin_istemoli` double(8,2) NOT NULL,
  `ekipaj_soni` int(11) NOT NULL,
  `solishtirma_quvvati` double(8,2) NOT NULL,
  `jangovor_toplam_nomenklaturasi` int(11) NOT NULL,
  `pushka_kalibri` double(8,2) NOT NULL,
  `jangovor_toplam` int(11) NOT NULL,
  `otish_surati` double(8,2) NOT NULL,
  `nishonlarni_yoq_qilish_uzoqligi` double(8,2) NOT NULL,
  `gorizont_boyicha_pushkani_nishonga_olish_tezligi` double(8,2) NOT NULL,
  `korpus_uzunligi` double(8,2) NOT NULL,
  `korpus_kengligi` double(8,2) NOT NULL,
  `bashnya_qopqogi_boyicha_balandligi` double(8,2) NOT NULL,
  `ekvivalent_zirx_qalinligi` double(8,2) NOT NULL,
  `niqoblanish_darajasi` double(8,2) NOT NULL,
  `niqoblash_granatalarining_soni` int(11) NOT NULL,
  `harakatchanlik` double(8,2) NOT NULL,
  `himoyalanganlik` double(8,2) NOT NULL,
  `qurollanganlik` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `vzvods`
--

DROP TABLE IF EXISTS `vzvods`;
CREATE TABLE IF NOT EXISTS `vzvods` (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomi` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `battalion_id` int(10) UNSIGNED NOT NULL,
  `jangovor_qobilyati` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jismoniy_holati` double(8,2) DEFAULT NULL,
  `manaviy_ruxiy_holati` double(8,2) DEFAULT NULL,
  `jangovor_tayyorgarligi` double DEFAULT NULL,
  `shaxsiy_tarkib_soni` bigint(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vzvods_ibfk_1` (`battalion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `vzvods`
--

INSERT INTO `vzvods` (`id`, `nomi`, `battalion_id`, `jangovor_qobilyati`, `jismoniy_holati`, `manaviy_ruxiy_holati`, `jangovor_tayyorgarligi`, `shaxsiy_tarkib_soni`, `created_at`, `updated_at`) VALUES
(19, '551 vzvod', 22, NULL, NULL, NULL, NULL, 2, '2019-11-18 07:19:52', '2019-11-19 01:04:18'),
(20, '552 vzvod', 22, NULL, NULL, NULL, NULL, NULL, '2019-11-18 07:20:07', '2019-11-18 07:20:07');

-- --------------------------------------------------------

--
-- Структура для представления `arh_ends`
--
DROP TABLE IF EXISTS `arh_ends`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `arh_ends`  AS  select `arhs`.`id` AS `id`,`arhs`.`jangchi_id` AS `jangchi_id`,`arhs`.`arh` AS `arh`,`arhs`.`created_at` AS `created_at`,`arhs`.`updated_at` AS `updated_at` from `arhs` where `arhs`.`id` in (select max(`b`.`id`) from `arhs` `b` group by `b`.`jangchi_id`) ;

-- --------------------------------------------------------

--
-- Структура для представления `charchaganliks`
--
DROP TABLE IF EXISTS `charchaganliks`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `charchaganliks`  AS  select `charchaganlik_darajasis`.`id` AS `id`,`charchaganlik_darajasis`.`jangchi_id` AS `jangchi_id`,`charchaganlik_darajasis`.`charchaganlik_darajasi` AS `charchaganlik_darajasi`,`charchaganlik_darajasis`.`created_at` AS `created_at`,`charchaganlik_darajasis`.`updated_at` AS `updated_at` from `charchaganlik_darajasis` where `charchaganlik_darajasis`.`id` in (select max(`b`.`id`) from `charchaganlik_darajasis` `b` group by `b`.`jangchi_id`) ;

-- --------------------------------------------------------

--
-- Структура для представления `jangovor_tayyorgarlik_ozlashtirishes`
--
DROP TABLE IF EXISTS `jangovor_tayyorgarlik_ozlashtirishes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jangovor_tayyorgarlik_ozlashtirishes`  AS  select `jangovor_tayyorgarliks`.`id` AS `id`,`jangovor_tayyorgarliks`.`jangchi_id` AS `jangchi_id`,`jangovor_tayyorgarliks`.`mutaxassislik` AS `mutaxassislik`,`jangovor_tayyorgarliks`.`oquv_yili` AS `oquv_yili`,`jangovor_tayyorgarliks`.`ball` AS `ball`,`jangovor_tayyorgarliks`.`created_at` AS `created_at`,`jangovor_tayyorgarliks`.`updated_at` AS `updated_at` from `jangovor_tayyorgarliks` where `jangovor_tayyorgarliks`.`id` in (select max(`b`.`id`) from `jangovor_tayyorgarliks` `b` group by `b`.`oquv_yili`,`b`.`jangchi_id`,`b`.`mutaxassislik`) ;

-- --------------------------------------------------------

--
-- Структура для представления `jismoniy_holats`
--
DROP TABLE IF EXISTS `jismoniy_holats`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jismoniy_holats`  AS  select `jismoniy_holatis`.`id` AS `id`,`jismoniy_holatis`.`jangchi_id` AS `jangchi_id`,`jismoniy_holatis`.`turnik` AS `turnik`,`jismoniy_holatis`.`100_metrga_yugurish` AS `100_metrga_yugurish`,`jismoniy_holatis`.`1_kmga_yugurish` AS `1_kmga_yugurish`,`jismoniy_holatis`.`3_kmga_yugurish` AS `3_kmga_yugurish`,`jismoniy_holatis`.`lapkost` AS `lapkost`,`jismoniy_holatis`.`press_qolni_bukish` AS `press_qolni_bukish`,`jismoniy_holatis`.`1erkin_mashqlar_toplami` AS `1erkin_mashqlar_toplami`,`jismoniy_holatis`.`2erkin_mashqlar_toplami` AS `2erkin_mashqlar_toplami`,`jismoniy_holatis`.`umumiy_baho` AS `umumiy_baho`,`jismoniy_holatis`.`vaqti` AS `vaqti`,`jismoniy_holatis`.`created_at` AS `created_at`,`jismoniy_holatis`.`updated_at` AS `updated_at` from `jismoniy_holatis` where `jismoniy_holatis`.`id` in (select max(`b`.`id`) from `jismoniy_holatis` `b` group by `b`.`jangchi_id`) ;

-- --------------------------------------------------------

--
-- Структура для представления `jt_fan_ozlashtirishes`
--
DROP TABLE IF EXISTS `jt_fan_ozlashtirishes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jt_fan_ozlashtirishes`  AS  select `jt_fan_bahosis`.`id` AS `id`,`jt_fan_bahosis`.`jangchi_id` AS `jangchi_id`,`jt_fan_bahosis`.`mutaxassislik` AS `mutaxassislik`,`jt_fan_bahosis`.`oquv_yili` AS `oquv_yili`,`jt_fan_bahosis`.`fan_id` AS `fan_id`,`jt_fan_bahosis`.`bahosi` AS `bahosi`,`jt_fan_bahosis`.`vaqti` AS `vaqti`,`jt_fan_bahosis`.`created_at` AS `created_at`,`jt_fan_bahosis`.`updated_at` AS `updated_at` from `jt_fan_bahosis` where `jt_fan_bahosis`.`id` in (select max(`b`.`id`) from `jt_fan_bahosis` `b` group by `b`.`oquv_yili`,`b`.`fan_id`,`b`.`jangchi_id`,`b`.`mutaxassislik`) ;

-- --------------------------------------------------------

--
-- Структура для представления `jt_mash_ozlashtirishes`
--
DROP TABLE IF EXISTS `jt_mash_ozlashtirishes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jt_mash_ozlashtirishes`  AS  select `jt__baholashes`.`id` AS `id`,`jt__baholashes`.`oquv_yili` AS `oquv_yili`,`jt__baholashes`.`jt_fan_id` AS `jt_fan_id`,`jt__baholashes`.`jt_mavzu_id` AS `jt_mavzu_id`,`jt__baholashes`.`jt_mash_id` AS `jt_mash_id`,`jt__baholashes`.`jangchi_id` AS `jangchi_id`,`jt__baholashes`.`bahosi` AS `bahosi`,`jt__baholashes`.`ozlashtirish` AS `ozlashtirish`,`jt__baholashes`.`mutaxassislik` AS `mutaxassislik`,`jt__baholashes`.`jt_vaqti` AS `jt_vaqti`,`jt__baholashes`.`end` AS `end`,`jt__baholashes`.`created_at` AS `created_at`,`jt__baholashes`.`updated_at` AS `updated_at` from `jt__baholashes` where `jt__baholashes`.`id` in (select max(`b`.`id`) from `jt__baholashes` `b` group by `b`.`oquv_yili`,`b`.`jt_fan_id`,`b`.`jt_mavzu_id`,`b`.`jt_mash_id`,`b`.`jangchi_id`,`b`.`mutaxassislik`) ;

-- --------------------------------------------------------

--
-- Структура для представления `jt_mavzu_ozlashtirishes`
--
DROP TABLE IF EXISTS `jt_mavzu_ozlashtirishes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jt_mavzu_ozlashtirishes`  AS  select `jt_mavzu_bahosis`.`id` AS `id`,`jt_mavzu_bahosis`.`jangchi_id` AS `jangchi_id`,`jt_mavzu_bahosis`.`fan_id` AS `fan_id`,`jt_mavzu_bahosis`.`oquv_yili` AS `oquv_yili`,`jt_mavzu_bahosis`.`mutaxassislik` AS `mutaxassislik`,`jt_mavzu_bahosis`.`mavzu_id` AS `mavzu_id`,`jt_mavzu_bahosis`.`bahosi` AS `bahosi`,`jt_mavzu_bahosis`.`vaqti` AS `vaqti`,`jt_mavzu_bahosis`.`created_at` AS `created_at`,`jt_mavzu_bahosis`.`updated_at` AS `updated_at` from `jt_mavzu_bahosis` where `jt_mavzu_bahosis`.`id` in (select max(`b`.`id`) from `jt_mavzu_bahosis` `b` group by `b`.`oquv_yili`,`b`.`fan_id`,`b`.`mavzu_id`,`b`.`jangchi_id`,`b`.`mutaxassislik`) ;

-- --------------------------------------------------------

--
-- Структура для представления `mmt_mavzu_ozlashtirishes`
--
DROP TABLE IF EXISTS `mmt_mavzu_ozlashtirishes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mmt_mavzu_ozlashtirishes`  AS  select `a`.`id` AS `id`,`a`.`jangchi_id` AS `jangchi_id`,`a`.`oquv_yili` AS `oquv_yili`,`a`.`mavzu_id` AS `mavzu_id`,`a`.`mavzu_bahosi` AS `mavzu_bahosi`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at` from (`mmt_mavzu_bahosis` `a` join (select `b`.`jangchi_id` AS `jangchi_id`,`b`.`mavzu_id` AS `mavzu_id`,`b`.`oquv_yili` AS `oquv_yili`,max(`b`.`id`) AS `id` from `mmt_mavzu_bahosis` `b` group by `b`.`jangchi_id`,`b`.`mavzu_id`,`b`.`oquv_yili`) `d` on((`d`.`id` = `a`.`id`))) ;

-- --------------------------------------------------------

--
-- Структура для представления `mmt_ozlashtirishes`
--
DROP TABLE IF EXISTS `mmt_ozlashtirishes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mmt_ozlashtirishes`  AS  select `history`.`id` AS `id`,`history`.`jangchi_id` AS `jangchi_id`,`history`.`oquv_yili` AS `oquv_yili`,`history`.`mmt_mavzu_id` AS `mmt_mavzu_id`,`history`.`mmt_mash_id` AS `mmt_mash_id`,`history`.`bahosi` AS `bahosi`,`history`.`ozlashtirish` AS `ozlashtirish`,`history`.`mmt_vaqti` AS `mmt_vaqti`,`history`.`created_at` AS `created_at`,`history`.`updated_at` AS `updated_at` from (`mmt_mash_baholashes` `history` join (select `b`.`jangchi_id` AS `jangchi_id`,`b`.`mmt_mash_id` AS `mmt_mash_id`,`b`.`mmt_mavzu_id` AS `mmt_mavzu_id`,`b`.`oquv_yili` AS `oquv_yili`,max(`b`.`id`) AS `id` from `mmt_mash_baholashes` `b` group by `b`.`jangchi_id`,`b`.`mmt_mash_id`,`b`.`mmt_mavzu_id`,`b`.`oquv_yili`) `d` on((`d`.`id` = `history`.`id`))) ;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `battalions`
--
ALTER TABLE `battalions`
  ADD CONSTRAINT `battalions_ibfk_1` FOREIGN KEY (`harbiy_qism_id`) REFERENCES `harbiy_qisms` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `guruhs`
--
ALTER TABLE `guruhs`
  ADD CONSTRAINT `guruhs_ibfk_1` FOREIGN KEY (`vzvod_id`) REFERENCES `vzvods` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `jangchilars`
--
ALTER TABLE `jangchilars`
  ADD CONSTRAINT `jangchilars_ibfk_1` FOREIGN KEY (`harbiy_qism_id`) REFERENCES `harbiy_qisms` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jangchilars_ibfk_2` FOREIGN KEY (`battalion_id`) REFERENCES `battalions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jangchilars_ibfk_3` FOREIGN KEY (`vzvod_id`) REFERENCES `vzvods` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jangchilars_ibfk_4` FOREIGN KEY (`guruh_id`) REFERENCES `guruhs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jangchilars_ibfk_5` FOREIGN KEY (`seksiya_id`) REFERENCES `seksiyas` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `seksiyas`
--
ALTER TABLE `seksiyas`
  ADD CONSTRAINT `seksiyas_ibfk_1` FOREIGN KEY (`guruh_id`) REFERENCES `guruhs` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `vzvods`
--
ALTER TABLE `vzvods`
  ADD CONSTRAINT `vzvods_ibfk_1` FOREIGN KEY (`battalion_id`) REFERENCES `battalions` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

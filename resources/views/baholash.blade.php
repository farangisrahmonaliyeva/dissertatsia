@extends('layouts.header')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <h1 class="m-0 text-dark">Jangovor qobiliyati-@if(!empty($jangchi)){{$jangchi->ismi}} @endif</h1>
    </nav>
@extends('layouts.navbar')
    @section('baholash','active')
    <div class="content-wrapper">
        <div class="content" >
            <section class="content" style="margin-bottom: -40px;">
                <form action="/baholash" method="POST" style="padding-top: 10px;">
                    @csrf
                    <div class="container-fluid" >
                        <div class="row">
                        <div class="form-group" style="margin-right: 10px;">
                            <select class="form-control dynamic" data-dependent="battalion" name="harbiy_qism" id="harbiy_qism">
                                <option value="">Harbiy qismni tanlang</option>
                                @foreach($harbiy_qisms as $harbiy_qism)
                                    <option value="{{$harbiy_qism->id}}" {{ !empty($jangchi) && $jangchi->harbiy_qism_id == $harbiy_qism->id ? 'selected' : '' }}>{{$harbiy_qism->nomi}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group " style="margin-right: 10px;">
                            <select class="form-control dynamic" data-dependent="battalion" name="battalion" id="battalion">
                                <option value="">Batalyonni tanlang</option>
                                @foreach($battalions as $battalion)
                                    <option value="{{$battalion->id}}" {{ !empty($jangchi) && $jangchi->battalion_id == $battalion->id ? 'selected' : '' }}>{{$battalion->nomi}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="margin-right: 10px;">
                            <select class="form-control dynamic"  name="vzvod" id="vzvod">
                                <option value="">Vzvodni tanlang</option>
                                @foreach($vzvods as $vzvod)
                                    <option value="{{$vzvod->id}}" {{ !empty($jangchi) && $jangchi->vzvod_id == $vzvod->id ? 'selected' : '' }}>{{$vzvod->nomi}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="margin-right: 10px;">
                            {{--<label for="guruh">guruhni tanlang</label>--}}
                            <select class="form-control dynamic" data-dependent="guruh" name="guruh" id="guruh">
                                <option value="">Guruhni tanlang</option>
                                @foreach($guruhs as $guruh)
                                    <option value="{{$guruh->id}}" {{ !empty($jangchi) && $jangchi->guruh_id == $guruh->id ? 'selected' : '' }}>{{$guruh->nomi}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="margin-right: 10px;">
                            {{--<label for="seksiya">seksiya tanlang</label>--}}
                            <select class="form-control dynamic" data-dependent="seksiya" name="seksiya" id="seksiya">
                                <option value="">Seksiyani tanlang</option>
                                @foreach($seksiyas as $seksiya)
                                    <option value="{{$seksiya->id}}" {{ !empty($jangchi) && $jangchi->seksiya_id == $seksiya->id ? 'selected' : '' }}>{{$seksiya->nomi}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="margin-right: 10px;">
                            {{--<label for="familiya">Familiyani tanlang</label>--}}
                            <select class="form-control dynamic" data-dependent="familiya" name="familiya" id="familiya">
                                <option value="">Familiyani tanlang</option>
                                @foreach($jangchilars as $jangchilar)
                                    <option value="{{$jangchilar->familyasi}}" {{ !empty($jangchi) && $jangchilar->id == $jangchi->id ? 'selected' : '' }}>{{$jangchilar->familyasi}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="margin-right: 10px;">
                            {{--<label for="ismi">Ismni tanlang</label>--}}
                            <select class="form-control dynamic" data-dependent="ismi" name="ismi" id="ismi">
                                <option value="">Ismni tanlang</option>
                                @foreach($jangchilars as $jangchilar)
                                    @if (!empty($jangchi) && $jangchilar->id == $jangchi->id)
                                        <option value="{{$jangchilar->id}}" {{ !empty($jangchi) && $jangchilar->id == $jangchi->id ? 'selected' : '' }}>{{$jangchilar->ismi}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-info" type="submit">OK</button>
                        </div>
                    </div>
                    </div>
                </form>
            </section>
            <div class="container">
                <br>
                <div class="col-sm-12">

                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                </div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#jx">Jismoniy xolati</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#mrx">Ma'naviy ruxiy xolati</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#jt">Jangovor tayyorgarlik</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="jx" class="container tab-pane active"><br>
                        <div class="row">
                            <div class="col-6" style="width:100%">
                                <div class="card border-primary mb-3" >
                                    {{--<div class="card-header bg-primary"></div>--}}
                                    <div class="card-body text-primary">
                                        <h5>Sog'lomlik darajasi</h5>
                                        <form action="/baholash/jh/soglomligi" method="POST">
                                            @csrf
                                            <input type="hidden" name="ismi" value="{{$id}}">
                                            <div class="row">
                                                <div class="col-8 form-group">
                                                    <select name="soglomligi" class="form-control select2" style="width: 100%;">
                                                        <option value="5">Yuqori</option>
                                                        <option value="4">yaxshi</option>
                                                        <option value="3">o'rta</option>
                                                        <option value="2">qoniqarli</option>
                                                        <option value="1">qoniqarsiz</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <button class="btn btn-info" style="float:right;" type="submit">OK</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6" >
                                <div class="card border-primary mb-3">
                                    {{--<div class="card-header bg-primary"></div>--}}
                                    <div class="card-body text-primary">
                                        <h5>Charchaganlik darajasi</h5>
                                        <form action="/baholash/jh/charchaganlik_darajasi" method="post">
                                        @csrf
                                            <input type="hidden" name="ismi" value="{{$id}}">
                                             <div class="row">
                                                <div class="col-8 form-group">
                                                    <select name="charchaganlik_darajasi" class="form-control select2" style="width: 100%;">
                                                        {{--<option value="">Baholang</option>--}}
                                                        <option value="5">juda past darajada</option>
                                                        <option value="4">past darajada</option>
                                                        <option value="3">o'rta darajada</option>
                                                        <option value="2">yuqori darajada</option>
                                                        <option value="1">o'ta yuqori darajada</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <button class="btn btn-info" style="float:right;" type="submit">OK</button>
                                                </div>
                                             </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="card border-primary mb-3" style="width:100%">
                                <div class="card-header bg-primary"><h5>Jismoniy tayyorgarlik</h5></div>
                                <div class="card-body text-primary">
                                    <h5></h5>
                                    <form action="baholash/jh/jismoniy_tayyorgarlik" method="POST">
                                        @csrf
                                        <input type="hidden" name="ismi" value="{{$id}}">
                                        <div class="row">
                                            <div class="col-4 form-group" style="padding-right: 10px; ">
                                                <select name="turnik" class="form-control select2" >
                                                    <option value="">Turnik</option>
                                                    <option value="5">5</option>
                                                    <option value="4">4</option>
                                                    <option value="3">3</option>
                                                    <option value="2">2</option>
                                                    <option value="1">1</option>
                                                </select>
                                            </div>
                                            <div class="col-4 form-group" style="padding-right: 10px; ">
                                                <select name="100_metrga_yugurish" class="form-control select2" >
                                                    <option value="">100m ga yugurish</option>
                                                    <option value="5">5</option>
                                                    <option value="4">4</option>
                                                    <option value="3">3</option>
                                                    <option value="2">2</option>
                                                    <option value="1">1</option>
                                                </select>
                                            </div>
                                            <div class="col-4 form-group" style="padding-right: 10px; ">
                                                <select name="l1_kmga_yugurish" class="form-control select2" >
                                                    <option value="">1km ga yugurish</option>
                                                    <option value="5">5</option>
                                                    <option value="4">4</option>
                                                    <option value="3">3</option>
                                                    <option value="2">2</option>
                                                    <option value="1">1</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4 form-group" style="padding-right: 10px; ">
                                                <select name="l3_kmga_yugurish" class="form-control select2">
                                                        <option value="">3km ga yugurish</option>
                                                        <option value="5">5</option>
                                                        <option value="4">4</option>
                                                        <option value="3">3</option>
                                                        <option value="2">2</option>
                                                        <option value="1">1</option>
                                                    </select>
                                            </div>
                                            <div class="col-4 form-group" style="padding-right: 10px; ">
                                                <select name="lapkost" class="form-control select2">
                                                    <option value="">Lopkost</option>
                                                    <option value="5">5</option>
                                                    <option value="4">4</option>
                                                    <option value="3">3</option>
                                                    <option value="2">2</option>
                                                    <option value="1">1</option>
                                                </select>
                                            </div>
                                            <div class="col-4 form-group" style="padding-right: 10px; ">
                                                <select name="press_qolni_bukish" class="form-control select2">
                                                    <option value="">Press & qo'lni bukish</option>
                                                    <option value="5">5</option>
                                                    <option value="4">4</option>
                                                    <option value="3">3</option>
                                                    <option value="2">2</option>
                                                    <option value="1">1</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4 form-group" style="padding-right: 10px; ">
                                                <select name="l1erkin_mashqlar_toplami" class="form-control select2" >
                                                    <option value="">1 erkin mashqlar</option>
                                                    <option value="5">5</option>
                                                    <option value="4">4</option>
                                                    <option value="3">3</option>
                                                    <option value="2">2</option>
                                                    <option value="1">1</option>
                                                </select>
                                            </div>
                                            <div class="col-4 form-group" style="padding-right: 10px; ">
                                                <select name="l2erkin_mashqlar_toplami" class="form-control select2" >
                                                    <option value="">2 erkin mashqlar</option>
                                                    <option value="5">5</option>
                                                    <option value="4">4</option>
                                                    <option value="3">3</option>
                                                    <option value="2">2</option>
                                                    <option value="1">1</option>
                                                </select>
                                            </div>
                                            <div class="col-4 ">
                                                <div class="form-group">
                                                    <select name="umumiy_baho" class="form-control select2" style="background-color: rgb(212,216,248)" >
                                                        <option value="">Umumiy baxo</option>
                                                        <option value="5">5</option>
                                                        <option value="4">4</option>
                                                        <option value="3">3</option>
                                                        <option value="2">2</option>
                                                        <option value="1">1</option>
                                                    </select>

                                                </div>
                                                <div class="form-group float-right" >
                                                    <button class="btn btn-info" type="submit">OK</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <section class="content">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header bg-primary">
                                            <h3 class="card-title">@if(!empty($jangchi)){{$jangchi->unvoni}} - {{$jangchi->familyasi}} {{$jangchi->ismi}} {{$jangchi->sharifi}}@endif</h3>
                                        </div>
                                        <div class="card-body">
                                            <table id="example1" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Jismoniy <br> holati</th>
                                                        <th>Jismoniy <br> tayyorgarligi</th>
                                                        <th>Charchaganlik <br> darajasi</th>
                                                        <th>vaqti</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(!empty($jangchi))
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                    if(!count($jismoniy_holatis))$jh=0;else $jh = $jismoniy_holatis[0]->umumiy_baho;
                                                                    if(!count($charchaganlik_darajasi)) $chd = 0; else $chd = $charchaganlik_darajasi[0]->charchaganlik_darajasi;
                                                                    if (!$soglomligi)$soglomligi = 0;else $soglomligi = $soglomligi->soglomligi;
                                                                    echo number_format((float)($jh+$chd+$soglomligi)/3.0, 2, '.', '');
                                                                ?>
                                                            </td>
                                                            <td>
                                                                @if(count($jismoniy_holatis)){{$jismoniy_holatis[0]->umumiy_baho}} @endif
                                                            </td>
                                                            <td>
                                                                @if(count($charchaganlik_darajasi)){{$charchaganlik_darajasi[0]->charchaganlik_darajasi}}@endif
                                                            </td>
                                                            <td>@if(count($jismoniy_holatis)){{$jismoniy_holatis[0]->created_at}}@endif</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div id="mrx" class="container tab-pane fade"><br>
                        <div class="row">
                            <div class="col-3">
                                <div class="card border-primary mb-3" style="height: 245px;">
                                    <div class="card-header bg-primary"><h5>Ahloqiy ruhiy tayyorgarligi</h5></div>
                                    <div class="card-body text-primary">
                                        <form action="/baholash/mrh/arh" method="POST">
                                            @csrf
                                            <input type="hidden" name="ismi" value="{{$id}}">
                                            <div class="form-group">
                                                <select name="arh" class="form-control select2" style="width: 100%;">
                                                    <option value="5">o'ta yuqori darajada</option>
                                                    <option value="4">yuqori darajada</option>
                                                    <option value="3">o'rta darajada</option>
                                                    <option value="2">past darajada</option>
                                                    <option value="1">o'ta past darajada</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <button class="btn btn-info" style="float:right;" type="submit">OK</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="content">
                                    <div class="card border-primary mb-3">
                                        <div class="card-header bg-primary"><h5>Ma'naviy ma'rifiy <br>   tayyorgarligi</h5></div>
                                        <div class="card-body text-primary">
                                            <form action="/baholash/mrh/mmt" method="POST">
                                                @csrf
                                                <input type="hidden" name="ismi" value="{{$id}}">
                                                <div class="container-fluid row">
                                                    <div class="form-group" style="padding-right: 10px;">
                                                            <select name="oquv_yili" class="form-control">
                                                                <option value="">O'quv yili</option>
                                                                <option value="2016-2017">2016-2017</option>
                                                                <option value="2017-2018">2017-2018</option>
                                                                <option value="2018-2019">2018-2019</option>
                                                                <option value="2019-2020">2019-2020</option>
                                                                <option value="2020-2021">2020-2021</option>
                                                            </select>
                                                    </div>
                                                    <div class="form-group" style="padding-right: 10px;">
                                                        <select class="form-control dynamic" data-dependent="mashgulot" name="mavzu" id="mavzu">
                                                            <option value="">Mavzuni tanlang</option>
                                                            @foreach($mmt_mavzus as $mmt_mavzu)
                                                                <option value="{{$mmt_mavzu->id}}">{{$mmt_mavzu->nomi}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group"  style="padding-right: 10px;">
                                                        <select class="form-control dynamic" data-dependent="mashgulot" name="mashgulot" id="mashgulot">
                                                            <option value="">Mashg'ulotni tanlang</option>
                                                            @foreach($mmt_mashgulots as $mmt_mashgulot)
                                                                <option value="{{$mmt_mashgulot->id}}">{{$mmt_mashgulot->nomi}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="container-fluid row">
                                                    <div class="form-group"  style="padding-right: 10px;">
                                                        <select name="bahosi" class="form-control select2" style="width: 100%;">
                                                            {{--<option value="0">Baholang</option>--}}
                                                            <option value="5">5</option>
                                                            <option value="4">4</option>
                                                            <option value="3">3</option>
                                                            <option value="2">2</option>
                                                            <option value="1">1</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group" style="padding-right: 10px;">
                                                        <div class="input-group">
                                                            <input name="vaqti" class="form-control" type="date" value="{{ date('Y-m-d') }}">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                    <div class="form-group"m >
                                                        <button class="btn btn-info form-control" type="submit">OK</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <section class="content">
                                <div class="row">
                                    <div class="col-12" style="width: 1000px;">
                                        <div class="card" >
                                            <diav class="card-header bg-primary ">
                                                <h3 class="card-title ">@if(!empty($jangchi)){{$jangchi->unvoni}} - {{$jangchi->familyasi}} {{$jangchi->ismi}} {{$jangchi->sharifi}}@endif</h3>
                                            </diav>
                                            <div class="card-body">
                                                <table id="example2" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            {{--<th rowspan="2">Ahloqiy<br>ruxiy holati</th>--}}
                                                            <th colspan="6" style="align-content: center">Ma'naviy ma'rifiy tayyorgarligi</th>
                                                            <tr>
                                                                <th>T/R</th>
                                                                <th>Mavzu №</th>
                                                                <th>Mashg'ulot №</th>
                                                                <th>Baxosi</th>
                                                                <th>O'zlashtirish darajasi</th>
                                                                <th>Vaqt</th>
                                                            </tr>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(!empty($jangchi))
                                                    <?php $i=1;$baho =0; ?>
                                                    @foreach($mmt_mavzus as $mmt_mavzu)
                                                        @foreach($mmt_mashgulots as $mmt_mashgulot)
                                                            <tr>

                                                                <td>{{$i++}}</td>
                                                                <td>{{$mmt_mavzu->mavzu_raqami}}</td>
                                                                <td>{{$mmt_mashgulot->nomi}}</td>
                                                                <td>
                                                                    <?php
                                                                    if(count($mmt_ozlashtirish))
                                                                    {
                                                                        $baho =\App\mmt_ozlashtirish::where('mmt_mavzu_id',$mmt_mavzu->id)
                                                                            ->where('mmt_mash_id',$mmt_mashgulot->id)->where('jangchi_id',$jangchi->id)->first();

                                                                    }?>
                                                                    @if(($baho)){{$baho['bahosi']}}@else {{0}} @endif
                                                                </td>
                                                                <td>
                                                                    @if(($baho)){{$baho['ozlashtirish']}}@else {{0}} @endif
                                                                </td>
                                                                <td>
                                                                    @if(($baho)){{$baho['mmt_vaqti']}}@else {{0}}@endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                    @endif
                                                    </tbody>
                                                    <tfoot>
                                                    </tfoot>
                                                </table>
                                                <table id="example2" class="table table-bordered table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>T/R</th>
                                                        <th>Baxosi</th>
                                                        <th>Vaqti</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(!empty($jangchi))
                                                        <?php $i=1; ?>
                                                        @foreach($arh as $data)
                                                        <tr>
                                                            <td>{{$i++}}</td>
                                                            <td>{{$data->arh}}</td>
                                                            <td>{{$data->created_at}}</td>
                                                        </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </section>
                        </div>

                    </div>
                    <div id="jt" class="container tab-pane fade"><br>
                        <div class="card border-primary mb-3">
                            <div class="card-header bg-primary"><h5>Jangovor tayyorgarlik</h5></div>
                            <div class="card-body text-primary">
                                <section class="content">
                                    <form action="/baholash/jt" method="POST">
                                        @csrf
                                        <input type="hidden" name="ismi" value="{{$id}}">
                                        <div class="container-fluid row">
                                            <div class="form-group" style="padding-right: 10px;">
                                                <select name="oquv_yili" class="form-control">
                                                    <option value="">O'quv yili</option>
                                                    <option value="2016-2017">2016-2017</option>
                                                    <option value="2017-2018">2017-2018</option>
                                                    <option value="2018-2019">2018-2019</option>
                                                    <option value="2019-2020">2019-2020</option>
                                                    <option value="2020-2021">2020-2021</option>
                                                </select>
                                            </div>
                                            <div class="form-group" style="padding-right: 10px;">
                                                <select name="mutaxassislik"  id="mutaxassislik" class="form-control" >
                                                    <option value="">Mutaxassislik</option>
                                                    @foreach($mutaxassisliks as $mutaxassislik)
                                                        <option value="{{$mutaxassislik->nomi}}">{{$mutaxassislik->nomi}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group" style="padding-right: 10px">
                                                <select class="form-control dynamic" data-dependent="jt_mavzu" name="jt_fan" id="jt_fan">
                                                    <option value="">Fanni tanlang</option>
                                                    @foreach($jt_fans as $jt_fan)
                                                    <option value="{{$jt_fan->id}}">{{$jt_fan->nomi}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="container-fluid row">
                                            <div class="form-group" style="padding-right: 10px">
                                                <select class="form-control dynamic" data-dependent="jt_mashgulot" name="jt_mavzu" id="jt_mavzu">
                                                    <option value="">Mavzuni tanlang</option>
                                                    @foreach($jt_mavzus as $jt_mavzu)
                                                        <option value="{{$jt_mavzu->id}}">{{$jt_mavzu->nomi}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group" style="padding-right: 10px">
                                                <select class="form-control dynamic" data-dependent="jt_mashgulot" name="jt_mashgulot" id="jt_mashgulot">
                                                    <option value="">mashg'ulotni tanlang</option>
                                                    @foreach($jt_mashgulots as $jt_mashgulot)
                                                        <option value="{{$jt_mashgulot->id}}">{{$jt_mashgulot->nomi}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group" style="padding-right: 10px">
                                                <select name="bahosi" class="form-control select2" style="width: 100%;">

                                                    <option value="5">5</option>
                                                    <option value="4">4</option>
                                                    <option value="3">3</option>
                                                    <option value="2">2</option>
                                                    <option value="1">1</option>
                                                </select>
                                            </div>
                                            <div class="form-group" style="padding-right: 10px;">
                                                <div class="input-group">
                                                    <input name="vaqti" class="form-control" type="date" value="{{ date('Y-m-d') }}">
                                                </div>
                                            </div>
                                            <div class="form-group" style="padding-right: 10px">
                                                <button class="btn btn-primary form-control" type="submit">OK</button>
                                            </div>
                                        </div>
                                    </form>
                                </section>
                            </div>
                        </div>
                        <section class="content">
                            <div class="row">
                                <div class="col-12" style="width: 1000px;">
                                    <!-- /.card -->
                                    <div class="card">
                                        <div class="card-header bg-primary">
                                            <h3 class="card-title">@if(!empty($jangchi)){{$jangchi->unvoni}} - {{$jangchi->familyasi}} {{$jangchi->ismi}} {{$jangchi->sharifi}}@endif</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table id="example2" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>T/R</th>
                                                        <th>Fan nomi</th>
                                                        <th>Mavzu №</th>
                                                        <th>Mavzu nomi</th>
                                                        <th>Mashg'ulot nomi</th>
                                                        <th>Bahosi</th>
                                                        <th>O'zlashtirish</th>
                                                        <th>vaqti</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if(!empty($jangchi))
                                                <?php $i=1;$baho =0; ?>
                                                @foreach($jt_fans as $fan)
                                                    @foreach($fan->jt_mavzus as $mavzu)
                                                        @foreach($mavzu->jt_mashgulots as $mashgulot)
                                                            <tr>
                                                                <td>{{$i++}}</td>
                                                                <td>{{$fan->nomi}}</td>
                                                                <td>{{$mavzu->raqami}}</td>
                                                                <td>{{$mavzu->nomi}}</td>
                                                                <td>{{$mashgulot->nomi}}</td>
                                                                <td>
                                                                <?php
                                                                    $jt_mash_bahosi = \App\jt__baholash::where('jangchi_id',$jangchi->id)->where('jt_fan_id',$fan->id)->where('jt_mavzu_id',$mavzu->id)->where('jt_mash_id',$mashgulot->id)->get();
                                                                if(count($jt_mash_bahosi))
                                                                {

                                                                    echo $jt_mash_bahosi[0]->bahosi;
                                                                }
                                                                ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $jt_mash_ozlashtirish = \App\jt_mash_ozlashtirish::where('jangchi_id',$jangchi->id)->where('jt_fan_id',$fan->id)->where('jt_mavzu_id',$mavzu->id)->where('jt_mash_id',$mashgulot->id)->get();
                                                                    if(count($jt_mash_ozlashtirish))
                                                                    {

                                                                        echo $jt_mash_bahosi[0]->bahosi;
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $jt_mash_ozlashtirish = \App\jt_mash_ozlashtirish::where('jangchi_id',$jangchi->id)->where('jt_fan_id',$fan->id)->where('jt_mavzu_id',$mavzu->id)->where('jt_mash_id',$mashgulot->id)->get();
                                                                    if(count($jt_mash_ozlashtirish))
                                                                    {

                                                                        echo $jt_mash_bahosi[0]->jt_vaqti;
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </div>
    </div></div>
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>--}}
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- page script -->
<script src="{{asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
<script>

    $(document).ready(function() {
        /**
         * for showing edit item popup
         */
        $('#harbiy_qism').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'harbiy_qism': $(this).val()
                },
                success: function(result){
                    var d = JSON.parse(result);
                    $('#battalion').html(d.text);
                    $('#familiya').html(d.text2);
                }
            });
        });
        $('#battalion').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'battalion': $(this).val()
                },
                success: function(result){
                    var d = JSON.parse(result);
                    $('#familiya').html(d.text2);
                    $('#vzvod').html(d.text);
                }
            });
        });
        $('#vzvod').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'vzvod': $(this).val()
                },
                success: function(result){
                    var d = JSON.parse(result);
                    $('#familiya').html(d.text2);
                    $('#guruh').html(d.text);
                }
            });
        });
        $('#guruh').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'guruh': $(this).val()
                },
                success: function(result){
                    var d = JSON.parse(result);
                    $('#familiya').html(d.text2);
                    $('#seksiya').html(d.text);
                }
            });
        });
        $('#seksiya').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'seksiya': $(this).val()
                },
                success: function(result){
                    var d = JSON.parse(result);
                    $('#familiya').html(d.text);
                }
            });
        });
        $('#familiya').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'familiya': $(this).val(),
                    'seksiya': $('#seksiya').val(),
                    'guruh2': $('#guruh').val(),
                    'battalion2': $('#battalion').val(),
                    'vzvod2': $('#vzvod').val(),
                    'harbiy_qism2': $('#harbiy_qism').val(),
                },
                success: function(result){
                    var d = JSON.parse(result);
                    $('#ismi').html(d.text2);
                }
            });
        });
        $('#mavzu').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'mavzu': $(this).val(),
                },
                success: function(result){
                    var d = JSON.parse(result);
                    $('#mashgulot').html(d.text);
                }
            });
        });
        $('#jt_fan').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/mashgulotlar/jh/search",
                method: "GET",
                data: {
                    'jt_fan': $(this).val()
                },
                success: function(result){
                    $('#jt_mavzu').html(result);
                }
            });
        });
        $('#jt_mavzu').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/mashgulotlar/jh/search",
                method: "GET",
                data: {
                    'jt_mavzu': $(this).val()
                },
                success: function(result){
                    $('#jt_mashgulot').html(result);
                }
            });
        });
        $(document).on('click', "#edit-item", function () {
            $(this).addClass('edit-item-trigger-clicked'); //useful for identifying which trigger was clicked and consequently grab data from the correct row and not the wrong one.

            var options = {
                'backdrop': 'static'
            };
            $('#edit-modal').modal(options)
        })

        // on modal show
        $('#edit-modal').on('show.bs.modal', function () {
            var el = $(".edit-item-trigger-clicked"); // See how its usefull right here?
            var row = el.closest(".data-row");

            // get the data
            var id = el.data('item-id');
            var name = row.children(".name").text();
            var description = row.children(".description").text();

            // fill the data in the input fields
            $("#modal-input-id").val(id);
            $("#modal-input-name").val(name);
            $("#modal-input-description").val(description);

        })
        $('#edit-modal').on('hide.bs.modal', function () {
            $('.edit-item-trigger-clicked').removeClass('edit-item-trigger-clicked')
            $("#edit-form").trigger("reset");
        })
    })
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable();
        $('#example3').DataTable();
    });
</script>
</body>
</html>

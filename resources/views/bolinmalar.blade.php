@extends('layouts.header')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <h1 class="m-0 text-dark">Harbiy qismlar</h1>
        <a href="harbiy_qism/create" style="margin-left: 600px;"><button class="colsm-6 btn btn-outline-success">Qo'shish <i class="ion ion-android-add-circle p-1"></i></button></a>
    </nav>
    <!-- /.navbar -->

@extends('layouts.navbar')
    @section('bolinma','active')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" >
        <section class="content" >
            <div class="container-fluid" >
                <div class="col-sm-12">

                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                </div>
                <!-- Small boxes (Stat box) -->
                <div class="row" style="margin-top: 20px;">
                    @foreach($harbiy_qisms as $harbiy_qism)
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <div class="row">
                                        <div class="col-9">
                                            <p>{{$harbiy_qism->nomi}}</p>
                                        </div>
                                        <div class="col float-right">
                                           <a href="{{route('edit_harbiy_qism',$harbiy_qism->id)}}" style="color: white;"> <i class="ion ion-edit" data-toggle="tooltip" data-placement="top" title="O'zgartirish"  style="font-size: 20px"></i></a>
                                            <a href="{{route('delete_harbiy_qism',$harbiy_qism->id)}}" style="color: white;">  <i class="ion ion-trash-a" data-toggle="tooltip" data-placement="top" title="O'chirish" style="font-size: 20px"></i></a>
                                        </div>
                                    </div>
                                    <p>shaxsiy tarkib soni - {{$harbiy_qism->shaxsiy_tarkib_soni}} ta</p>
                                </div>
                                <input type="hidden" value="{{$harbiy_qism->id}}" name="id">

                                <a href="/harbiy_qism/{{$harbiy_qism->id}}" class="small-box-footer">Ko'rish <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>

</div>
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

</script>
</body>
</html>

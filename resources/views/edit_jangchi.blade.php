@extends('layouts.header')
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    @extends('layouts.navbar')
    <div class="content-wrapper">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title"><h3><b>Jangchini o'zgartirish</b></h3></h3>
            </div>
            <form action="{{ route('update_jangchi', $jangchi->id) }}" class="form-horizontal" method="post">
                @csrf
                <div class="card-body" style="margin-left:50px; ">
                    <div class="row" >
                        <div class="col-3" style="margin-right: 20px;">
                            <div class="form-group row">
                                <select name="unvoni" class="form-control" id="unvoni" required>

                                    <option value="" class="w-100">Unvoni</option>
                                    @if($jangchi->unvoni == "oddiy askar")
                                        <option value="oddiy askar" selected>oddiy askar</option>
                                    @else
                                    <option value="oddiy askar">oddiy askar</option>
                                    @endif
                                    @if($jangchi->unvoni == "ShAHX oddiy askar")
                                        <option value="ShAHX oddiy askar">ShAHX oddiy askar</option>
                                    @else
                                     <option value="ShAHX oddiy askar">ShAHX oddiy askar</option>
                                    @endif
                                    @if($jangchi->unvoni == "kichik serjant")
                                        <option value="kichik serjant" selected>kichik serjant</option>
                                    @else
                                    <option value="kichik serjant">kichik serjant</option>
                                    @endif
                                    @if($jangchi->unvoni == "serjant")
                                        <option value="serjant" selected>serjant</option>
                                    @else
                                        <option value="serjant">serjant</option>
                                    @endif

                                    @if($jangchi->unvoni == "katta serjant")
                                        <option value="katta serjant" selected>katta serjant</option>
                                    @else
                                        <option value="katta serjant">katta serjant</option>
                                    @endif

                                    @if($jangchi->unvoni == "leytenant")
                                        <option value="leytenant" selected>leytenant</option>
                                    @else
                                        <option value="leytenant">leytenant</option>
                                    @endif

                                    @if($jangchi->unvoni == "katta leytenant")
                                        <option value="katta leytenant" selected>katta leytenant</option>
                                    @else
                                        <option value="katta leytenant">katta leytenant</option>
                                    @endif
                                    @if($jangchi->unvoni == "kapitan")
                                        <option value="kapitan" selected>kapitan</option>
                                    @else
                                        <option value="kapitan">kapitan</option>
                                    @endif
                                    @if($jangchi->unvoni == "mayor")
                                        <option value="mayor" selected>mayor</option>
                                    @else
                                        <option value="mayor">mayor</option>
                                    @endif
                                    @if($jangchi->unvoni == "podpolkovnik")
                                        <option value="podpolkovnik" selected>podpolkovnik</option>
                                    @else
                                        <option value="podpolkovnik">podpolkovnik</option>
                                    @endif
                                    @if($jangchi->unvoni == "polkovnik")
                                        <option value="polkovnik" selected>polkovnik</option>
                                    @else
                                        <option value="polkovnik">polkovnik</option>
                                    @endif
                                    @if($jangchi->unvoni == "general mayor")
                                        <option value="general mayor" selected>general mayor</option>
                                    @else
                                        <option value="general mayor">general mayor</option>
                                    @endif
                                    @if($jangchi->unvoni == "general leytenant")
                                        <option value="general leytenant" selected>general leytenant</option>
                                    @else
                                        <option value="general leytenant">general leytenant</option>
                                    @endif
                                    @if($jangchi->unvoni == "general polkovnik")
                                        <option value="general polkovnik" selected>general polkovnik</option>
                                    @else
                                        <option value="general polkovnik">general polkovnik</option>
                                    @endif

                                </select>
                            </div>
                            <div class="form-group row ">
                                <input type="text" class="form-control" name="familiya" id="familiya" placeholder="Familiyani kiriting" value="{{$jangchi->familyasi}}" required>
                            </div>
                            <div class="form-group row">
                                <input type="text" class="form-control" name="ismi" id="ismi" placeholder="Ismini kiriting" value="{{$jangchi->ismi}}" required>
                            </div>
                            <div class="form-group row">
                                <input type="text" class="form-control"  name="sharifi" id="sharifi" placeholder="Sharifini kiriting" value="{{$jangchi->sharifi}}" required>
                            </div>
                            <div class="input-group row" style="padding-left: -30px; " >
                                <div class="input-group-prepend form-check">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="date" class="form-control" name="date" placeholder="Tug'ilgan vaqti" value="{{$jangchi->tugilgan_vaqti}}" required>
                            </div>
                            <div class="form-group row " style="padding: 20px;">
                                <label for="jinsi" class="col-scol-form-label " style="padding-right:10px;"  >Jinsi</label>
                                <div class="form-check ">
                                    <input class="form-check-input" value="1"  @if($jangchi->jinsi==1)checked @endif type="radio" name="jinsi">
                                    <label class="form-check-label">Erkak</label>
                                </div>
                                <div class="form-check" style="margin-left: 20px;">
                                    <input class="form-check-input" value="2"  @if($jangchi->jinsi==2)checked @endif type="radio" name="jinsi" >
                                    <label class="form-check-label">Ayol</label>
                                </div>
                            </div>
                        </div>
                        {{--////--}}
                        <div class="col-3" style="margin-right: 20px;">
                            <div class="form-group row">
                                <input type="text" class="form-control" name="ish_joyi" id="ish_joyi" value="{{$jangchi->ish_joyi}}" placeholder="Lavozimini kiriting" required>
                            </div>

                            <div class="form-group row ">
                                <select class="form-control select2" name="harbiy_qism" id="harbiy_qism" style="width: 100%;" required>
                                    @foreach($harbiy_qism as $item)
                                        @if($item->id==$jangchi->harbiy_qism_id)<option value="{{ $item->id }}" selected>{{ $item->nomi }}</option>@php $battalion = $item; @endphp @else
                                        <option value="{{ $item->id }}">{{ $item->nomi }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row ">
                                <select class="form-control select2" name="battalion" id="battalion" style="width: 100%;" >
                                    @foreach($battalion->battalions as $item)
                                        @if($item->id == $jangchi->battalion_id)<option value="{{ $item->id }}" selected>{{ $item->nomi }}</option>@php $vzod = $item; @endphp @else
                                            <option value="{{ $item->id }}">{{ $item->nomi }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row " >
                                <select class="form-control select2" style="width: 100%;" name="vzvod" id="vzvod" >
                                    @foreach($vzod->vzvods as $item)
                                        @if($item->id == $jangchi->vzvod_id)<option value="{{ $item->id }}" selected>{{ $item->nomi }}</option>@php $guruh = $item; @endphp @else
                                            <option value="{{ $item->id }}">{{ $item->nomi }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row ">
                                <select class="form-control select2" style="width: 100%;" name="guruh" id="guruh" >
                                    @foreach($guruh->guruhs as $item)
                                        @if($item->id == $jangchi->guruh_id)<option value="{{ $item->id }}" selected>{{ $item->nomi }}</option>@php $sek = $item; @endphp @else
                                            <option value="{{ $item->id }}">{{ $item->nomi }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row">
                                <select class="form-control select2" style="width: 100%;" name="seksiya" id="seksiya" >
                                    @foreach($sek->seksiyas as $item)
                                        @if($item->id == $jangchi->seksiya_id)<option value="{{ $item->id }}" selected>{{ $item->nomi }}</option> @else
                                            <option value="{{ $item->id }}">{{ $item->nomi }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-3" style="margin-right: 20px;">

                            <div class="form-group row">
                                <input type="text" class="form-control" name="boy" id="boy" value="{{$jangchi->boyi}}" placeholder="Bo'yi(sm)">
                            </div>
                            <div class="form-group row">
                                <input type="text" class="form-control" name="vazn" id="vazn" value="{{$jangchi->vazni}}" placeholder="Vazni(kg)">
                            </div>
                        </div>
                </div>
                    <div class="row" style="margin-right: 200px; "> <button type="submit" class="btn btn-info w-100">O'zgartirish</button></div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}}"></script>
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{asset('plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js')}}"></script>
<!-- InputMask -->
<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
<!-- date-range-picker -->
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Bootstrap Switch -->
<script src="{{asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- Page script -->
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY hh:mm A'
            }
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate  : moment()
            },
            function (start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
            }
        )

        //Timepicker
        $('#timepicker').datetimepicker({
            format: 'LT'
        })

        //Bootstrap Duallistbox
        $('.duallistbox').bootstrapDualListbox()

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        $('.my-colorpicker2').on('colorpickerChange', function(event) {
            $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
        });

        $("input[data-bootstrap-switch]").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });
        $('#harbiy_qism').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'harbiy_qism': $(this).val()
                },
                success: function(result){
                    var d = JSON.parse(result);
                    $('#battalion').html(d.text);
                    $('#familiya').html(d.text2);
                }
            });
        });
        $('#battalion').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'battalion': $(this).val()
                },
                success: function(result){
                    var d = JSON.parse(result);
                    $('#familiya').html(d.text2);
                    $('#vzvod').html(d.text);
                }
            });
        });
        $('#vzvod').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'vzvod': $(this).val()
                },
                success: function(result){
                    var d = JSON.parse(result);
                    $('#familiya').html(d.text2);
                    $('#guruh').html(d.text);
                }
            });
        });
        $('#guruh').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'guruh': $(this).val()
                },
                success: function(result){
                    var d = JSON.parse(result);
                    $('#familiya').html(d.text2);
                    $('#seksiya').html(d.text);
                }
            });
        });
    })
</script>
</body>
</html>

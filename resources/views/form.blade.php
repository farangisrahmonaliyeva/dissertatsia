@extends('layouts.header')
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <h1 class="m-0 text-dark">Shaxsiy tarkib ro'yhati</h1>
        <a href="/jangchilar/create"><button style="margin-left: 600px; " class="btn btn-primary">Qo'shish</button></a>
    </nav>
    @extends('layouts.navbar')
    @section('jangchilar','active')
    <div class="content-wrapper">
        <section class="content">
            <div class="content-header">
                {{--<div class="content-wrapper">--}}
                {{--<div class="card card-info">--}}
                {{--<div class="card-header">--}}
                {{--<h3 class="card-title"><h3><b>Harbiy xizmatchini ro'yhatdan o'tkazish</b></h3></h3>--}}
                {{--</div>--}}
                {{--<form action="/store_jangchilar" class="form-horizontal" method="post">--}}
                {{--@csrf--}}
                {{--<div class="card-body" >--}}
                {{--<div class="row" >--}}
                {{--<div class="col-3" style="margin-right: 20px;">--}}
                {{--<div class="form-group row">--}}
                {{--<select name="unvoni" class="form-control" id="unvoni" required>--}}
                {{--<option value="" class="w-100">Unvoni</option>--}}
                {{--<option value="oddiy askar">oddiy askar</option>--}}
                {{--<option value="ShAHX oddiy askar">ShAHX oddiy askar</option>--}}
                {{--<option value="kichik serjant">kichik serjant</option>--}}
                {{--<option value="serjant">serjant</option>--}}
                {{--<option value="katta serjant">katta serjant</option>--}}
                {{--<option value="leytenant">leytenant</option>--}}
                {{--<option value="katta leytenant">katta leytenant</option>--}}
                {{--<option value="kapitan">kapitan</option>--}}
                {{--<option value="mayor">mayor</option>--}}
                {{--<option value="podpolkovnik">podpolkovnik</option>--}}
                {{--<option value="polkovnik">polkovnik</option>--}}
                {{--<option value="general mayor">general mayor</option>--}}
                {{--<option value="general leytenant">general leytenant</option>--}}
                {{--<option value="general polkovnik">general polkovnik</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--<div class="form-group row ">--}}
                {{--<input type="text" class="form-control" name="familiya" id="familiya" placeholder="Familiyani kiriting" required>--}}
                {{--</div>--}}
                {{--<div class="form-group row">--}}
                {{--<input type="text" class="form-control" name="ismi" id="ismi" placeholder="Ismini kiriting" required>--}}
                {{--</div>--}}
                {{--<div class="form-group row">--}}
                {{--<input type="text" class="form-control"  name="sharifi" id="sharifi" placeholder="Sharifini kiriting" required>--}}
                {{--</div>--}}
                {{--<div class="input-group row" style="padding-left: -30px; " >--}}
                {{--<div class="input-group-prepend form-check">--}}
                {{--<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>--}}
                {{--</div>--}}
                {{--<input type="date" class="form-control" name="date" placeholder="Tug'ilgan vaqti" required>--}}
                {{--</div>--}}
                {{--<div class="form-group row " style="padding: 20px;">--}}
                {{--<label for="jinsi" class="col-scol-form-label " style="padding-right:10px;"  >Jinsi</label>--}}
                {{--<div class="form-check ">--}}
                {{--<input class="form-check-input" value="1" type="radio" name="jinsi" checked>--}}
                {{--<label class="form-check-label">Erkak</label>--}}
                {{--</div>--}}
                {{--<div class="form-check" style="margin-left: 20px;">--}}
                {{--<input class="form-check-input" value="2" type="radio" name="jinsi" >--}}
                {{--<label class="form-check-label">Ayol</label>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-3" style="margin-right: 20px;">--}}
                {{--<div class="form-group row">--}}
                {{--<input type="text" class="form-control" name="ish_joyi" id="ish_joyi" placeholder="Lavozimini kiriting" required>--}}
                {{--</div>--}}

                {{--<div class="form-group row ">--}}
                {{--<select class="form-control select2" name="harbiy_qism" id="harbiy_qism" style="width: 100%;" required>--}}
                {{--<option selected="selected" value="">Harbiy qismni tanlang</option>--}}
                {{--@foreach($harbiy_qism as $item)--}}
                {{--<option value="{{ $item->id }}">{{ $item->nomi }}</option>--}}
                {{--@endforeach--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--<div class="form-group row ">--}}
                {{--<select class="form-control select2" name="battalion" id="battalion" style="width: 100%;">--}}
                {{--<option selected="selected" value="">Batalyonni tanlang</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--<div class="form-group row " >--}}
                {{--<select class="form-control select2" style="width: 100%;" name="vzvod" id="vzvod" >--}}
                {{--<option selected="selected" value="">Vzvodni tanlang</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--<div class="form-group row ">--}}
                {{--<select class="form-control select2" style="width: 100%;" name="guruh" id="guruh" >--}}
                {{--<option selected="selected" value="">Guruhni tanlang</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--<div class="form-group row">--}}
                {{--<select class="form-control select2" style="width: 100%;" name="seksiya" id="seksiya" >--}}
                {{--<option selected="selected" value="">Seksiyani tanlang</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-3" style="margin-right: 20px;">--}}

                {{--<div class="form-group row">--}}
                {{--<input type="text" class="form-control" name="boy" id="boy" placeholder="Bo'yi(sm)">--}}
                {{--</div>--}}
                {{--<div class="form-group row">--}}
                {{--<input type="text" class="form-control" name="vazn" id="vazn" placeholder="Vazni(kg)">--}}
                {{--</div>--}}

                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row" style="margin-right: 200px; "> <button type="submit" class="btn btn-info w-100">Ro'yxatga qo'shish</button></div>--}}
                {{--</div>--}}
                {{--</form>--}}
                {{--</div>--}}
                {{--</div>--}}



                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
            </div>
            <div class="card">
                <div class="card-header">
                    <section class="content" style="margin-bottom: -40px;">
                        <form action="/baholash" method="POST" style="padding-top: 10px;">
                            @csrf
                            <div class="container-fluid" >
                                <div class="row">
                                    <div class="form-group" style="margin-right: 10px;">
                                        {{--<label for="harbiy_qism">Harbiy qismni tanlang</label>--}}
                                        <select class="form-control dynamic" data-dependent="battalion" name="harbiy_qism" id="harbiy_qism">
                                            <option value="">Harbiy qismni tanlang</option>
                                            @foreach($harbiy_qisms as $harbiy_qism)
                                                <option value="{{$harbiy_qism->id}}">{{$harbiy_qism->nomi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group " style="margin-right: 10px;">
                                        {{--<label for="battalion">battalion tanlang</label>--}}
                                        <select class="form-control dynamic" data-dependent="battalion" name="battalion" id="battalion">
                                            <option value="">Batalyonni tanlang</option>
                                            @foreach($battalions as $battalion)
                                                <option value="{{$battalion->id}}">{{$battalion->nomi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" style="margin-right: 10px;">
                                        {{--<label for="vzvod">vzvod tanlang</label>--}}
                                        <select class="form-control dynamic"  name="vzvod" id="vzvod">
                                            <option value="">Vzvodni tanlang</option>
                                            @foreach($vzvods as $vzvod)
                                                <option value="{{$vzvod->id}}">{{$vzvod->nomi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" style="margin-right: 10px;">
                                        {{--<label for="guruh">guruhni tanlang</label>--}}
                                        <select class="form-control dynamic" data-dependent="guruh" name="guruh" id="guruh">
                                            <option value="">Guruhni tanlang</option>
                                            @foreach($guruhs as $guruh)
                                                <option value="{{$guruh->id}}">{{$guruh->nomi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" style="margin-right: 10px;">
                                        {{--<label for="seksiya">seksiya tanlang</label>--}}
                                        <select class="form-control dynamic" data-dependent="seksiya" name="seksiya" id="seksiya">
                                            <option value="">Seksiyani tanlang</option>
                                            @foreach($seksiyas as $seksiya)
                                                <option value="{{$seksiya->id}}">{{$seksiya->nomi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" style="margin-right: 10px;">
                                        {{--<label for="familiya">Familiyani tanlang</label>--}}
                                        <select class="form-control dynamic" data-dependent="familiya" name="familiya" id="familiya">
                                            <option value="">Familiyani tanlang</option>
                                            @foreach($jangchilars as $jangchilar)
                                                <option value="{{$jangchilar->id}}">{{$jangchilar->familyasi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" style="margin-right: 10px;">
                                        {{--<label for="ismi">Ismni tanlang</label>--}}
                                        <select class="form-control dynamic" data-dependent="ismi" name="ismi" id="ismi">
                                            <option value="">Ismni tanlang</option>
                                            @foreach($jangchilars as $jangchilar)
                                                <option value="{{$jangchilar->id}}">{{$jangchilar->ismi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-info" type="submit">Izlash</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0" style="height: 300px;">
                    <table class="table table-head-fixed">
                        <thead>
                        <tr>
                            <th>Unvoni</th>
                            <th>Lavozimi</th>
                            <th>Familyasi</th>
                            <th>Ismi</th>
                            <th>Sharifi</th>
                            <th>Tugilgan  vaqti</th>
                            <th>O'zgartirish</th>
                            <th>O'chirish</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($jangchilars as $jangchilar)
                            <tr  style="height: 5px">
                                <td>{{$jangchilar->unvoni}}</td>
                                <td>{{$jangchilar->ish_joyi}}</td>
                                <td>{{$jangchilar->familyasi}}</td>
                                <td>{{$jangchilar->ismi}}</td>
                                <td>{{$jangchilar->sharifi}}</td>
                                <td>{{$jangchilar->tugilgan_vaqti}}</td>
                                <td><a href="{{route('edit_jangchi',$jangchilar->id)}}">
                                        <button style="background-color: blue;color:white;">O'zgartirish</button></a>
                                </td>
                                <td>
                                    <form action="{{ route('delete_jangchi',$jangchilar->id)}}" method="post">
                                        @csrf
                                        <input type="hidden" name="harbiy_qism" value="{{$jangchilar->harbiy_qism_id}}">
                                        <input type="hidden" name="battalion" value="{{$jangchilar->battalion_id}}">
                                        <input type="hidden" name="vzvod" value="{{$jangchilar->vzvod_id}}">
                                        <input type="hidden" name="guruh" value="{{$jangchilar->guruh_id}}">
                                        <input type="hidden" name="seksiya" value="{{$jangchilar->seksiya_id}}">
                                        <button style="background-color: red; color: white;" type="submit">O'chirish</button>
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <td>{{$jangchilar->unvoni}}</td>
                                <td>{{$jangchilar->ish_joyi}}</td>
                                <td>{{$jangchilar->familyasi}}</td>
                                <td>{{$jangchilar->ismi}}</td>
                                <td>{{$jangchilar->sharifi}}</td>
                                <td>{{$jangchilar->tugilgan_vaqti}}</td>
                                <td><a href="{{route('edit_jangchi',$jangchilar->id)}}">
                                        <button style="background-color: blue;color:white;">O'zgartirish</button></a>
                                </td>
                                <td>
                                    <form action="{{ route('delete_jangchi',$jangchilar->id)}}" method="post">
                                        @csrf
                                        <input type="hidden" name="harbiy_qism" value="{{$jangchilar->harbiy_qism_id}}">
                                        <input type="hidden" name="battalion" value="{{$jangchilar->battalion_id}}">
                                        <input type="hidden" name="vzvod" value="{{$jangchilar->vzvod_id}}">
                                        <input type="hidden" name="guruh" value="{{$jangchilar->guruh_id}}">
                                        <input type="hidden" name="seksiya" value="{{$jangchilar->seksiya_id}}">
                                        <button style="background-color: red; color: white;" type="submit">O'chirish</button>
                                    </form>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}}"></script>
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{asset('plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js')}}"></script>
<!-- InputMask -->
<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
<!-- date-range-picker -->
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Bootstrap Switch -->
<script src="{{asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- Page script -->
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY hh:mm A'
            }
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate  : moment()
            },
            function (start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
            }
        )

        //Timepicker
        $('#timepicker').datetimepicker({
            format: 'LT'
        })

        //Bootstrap Duallistbox
        $('.duallistbox').bootstrapDualListbox()

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        $('.my-colorpicker2').on('colorpickerChange', function(event) {
            $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
        });

        $("input[data-bootstrap-switch]").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });
        $('#harbiy_qism').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'harbiy_qism': $(this).val()
                },
                success: function(result){
                    $('#battalion').html(result);
                }
            });
        });
        $('#battalion').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'battalion': $(this).val()
                },
                success: function(result){
                    $('#vzvod').html(result);
                }
            });
        });
        $('#vzvod').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'vzvod': $(this).val()
                },
                success: function(result){
                    $('#guruh').html(result);
                }
            });
        });
        $('#guruh').change(function () {
            // console.log($(this).val());
            $.ajax({
                url: "/baholash/jh/search",
                method: "GET",
                data: {
                    'guruh': $(this).val()
                },
                success: function(result){
                    $('#seksiya').html(result);
                }
            });
        });
    })
</script>
</body>
</html>

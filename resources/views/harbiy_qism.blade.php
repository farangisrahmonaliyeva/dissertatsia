@extends('layouts.header')
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    @extends('layouts.navbar')
    <div class="content-wrapper">
        {{--lolo--}}
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Horizontal Form</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form class="form-horizontal">
                @csrf
                <div class="card-body">
                    <div class="form-group row">
                        <label for="unvoni" class="col-sm-2 col-form-label">Unvoni</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="unvoni" id="unvoni" placeholder="Unvonni kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="familiya" class="col-sm-2 col-form-label">Familiya</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="familiya" id="familiya" placeholder="Familiyani kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ismi" class="col-sm-2 col-form-label">Ismi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="ismi" id="ismi" placeholder="Ismini kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="sharifi" class="col-sm-2 col-form-label">Sharifi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="sharifi" id="sharifi" placeholder="sharifini kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jinsi" class="col-sm-2 col-form-label">Jinsi</label>
                        <div class="form-check ">
                            <input class="form-check-input" value="1" type="radio" name="jinsi" checked>
                            <label class="form-check-label">Erkak</label>
                        </div>

                        <div class="form-check col-sm-8" style="margin-left: 20px;">
                            <input class="form-check-input" value="2" type="radio" name="jinsi" >
                            <label class="form-check-label">Ayol</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="date" class="col-sm-2 col-form-label">Date masks:</label>

                        <div class="input-group col-sm-8" >
                            <div class="input-group-prepend form-check">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" class="form-control" data-inputmask-alias="datetime" name="date" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                        </div>
                        <!-- /.input group -->
                    </div>
                    <div class="form-group row">
                        <label for="ish_joyi" class="col-sm-2 col-form-label">Ish joyi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="ish_joyi" id="ish_joyi" placeholder="Ish joyini kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Seksiya</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">1</option>
                                <option>2</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label class="col-sm-2 col-form-label">Guruh</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">1</option>
                                <option>2</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label class="col-sm-2 col-form-label">Vzvod</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">1</option>
                                <option>2</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label class="col-sm-2 col-form-label">Battalion</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">1</option>
                                <option>2</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label class="col-sm-2 col-form-label">Harbiy qism</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">1</option>
                                <option>2</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="boy" class="col-sm-2 col-form-label">Bo'yi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="boy" id="boy" placeholder="Bo'yni kiriting(sm)">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="vazn" class="col-sm-2 col-form-label">Og'irligi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="vazn" id="vazn" placeholder="Og'irligini kiriting(kg)">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jangovor_qobilyati" class="col-sm-2 col-form-label">Jangovor qobilyati</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="jangovor_qobilyati" id="jangovor_qobilyati" placeholder="Jangovor qobilyatini kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="soglomligi" class="col-sm-2 col-form-label">Sog'lomligi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="soglomligi" name="soglomligi" placeholder="Sog'lomligini kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jismoniy_tayyorgarligi" class="col-sm-2 col-form-label">Jismoniy tayyorgarligi</label>
                        <div class="col-sm-8">
                            <input type="text" name="jismoniy_tayyorgarligi" class="form-control" id="jismoniy_tayyorgarligi" placeholder="Jismoniy tayyorgarligini kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jismoniy_xolati" class="col-sm-2 col-form-label">Jismoniy xolati</label>
                        <div class="col-sm-8">
                            <input type="text" name="jismoniy_xolati" class="form-control" id="jismoniy_xolati" placeholder="Jismoniy xolati kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="charchaganlik_darajasi" class="col-sm-2 col-form-label">Charchaganlik darajasi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="charchaganlik_darajasi" id="charchaganlik_darajasi" placeholder="Charchaganlik darajasini kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="manaviy_ruhiy_holati" class="col-sm-2 col-form-label">Ma'naviy ruhiy holati</label>
                        <div class="col-sm-8">
                            <input type="text" name="manaviy_ruhiy_holati" class="form-control" id="manaviy_ruhiy_holati" placeholder="Ma'naviy ruhiy holatini kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ahloqiy_ruhiy_xolati" class="col-sm-2 col-form-label">ahloqiy_ruhiy_xolati</label>
                        <div class="col-sm-8">
                            <input type="text" name="ahloqiy_ruhiy_xolati" class="form-control" id="ahloqiy_ruhiy_xolati" placeholder="ahloqiy_ruhiy_xolatini kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="mmt" class="col-sm-2 col-form-label">mmt</label>
                        <div class="col-sm-8">
                            <input type="text" name="mmt" class="form-control" id="mmt" placeholder="mmt kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jangovor_tayyorgarligi" class="col-sm-2 col-form-label">jangovor_tayyorgarligi</label>
                        <div class="col-sm-8">
                            <input type="text" name="jangovor_tayyorgarligi" class="form-control" id="jangovor_tayyorgarligi" placeholder="jangovor_tayyorgarligi kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="mmt_vaqti" class="col-sm-2 col-form-label">mmt_vaqti</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="mmt_vaqti" id="mmt_vaqti" placeholder="jangovor_tayyorgarligi kiriting">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="exampleCheck2" id="exampleCheck2">
                                <label class="form-check-label" for="exampleCheck2">Remember me</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-info">Sign in</button>
                    <button type="submit" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
            </form>
        </div>
        {{--lolo--}}
    </div>
</div>
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}}"></script>
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{asset('plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js')}}"></script>
<!-- InputMask -->
<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
<!-- date-range-picker -->
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Bootstrap Switch -->
<script src="{{asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- Page script -->
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY hh:mm A'
            }
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate  : moment()
            },
            function (start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
            }
        )

        //Timepicker
        $('#timepicker').datetimepicker({
            format: 'LT'
        })

        //Bootstrap Duallistbox
        $('.duallistbox').bootstrapDualListbox()

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        $('.my-colorpicker2').on('colorpickerChange', function(event) {
            $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
        });

        $("input[data-bootstrap-switch]").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });

    })
</script>
</body>
</html>

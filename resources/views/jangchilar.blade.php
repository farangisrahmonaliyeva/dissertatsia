<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>jQuery tablesorter 2.0 - Filter Widget External Search</title>

    <!-- jQuery -->
        <script src="js/jquery-latest.min.js"></script>

        <!-- Demo stuff -->
        <link class="ui-theme" rel="stylesheet" href="{{'css/jquery-ui.min.css'}}">
        <script src="{{asset('js/jquery-ui.min.js')}}"></script>
        <link rel="stylesheet" href="{{'css/jq.css'}}">
        <link href="{{'css/prettify.css'}}" rel="stylesheet">
        <script src="{{'js/prettify.js'}}"></script>
        <script src="{{'js/docs.js'}}"></script>

        <!-- Tablesorter: required -->
        <link rel="stylesheet" href="{{asset('css/theme.blue.css')}}">
        <script src="{{asset('js/jquery.tablesorter.js')}}"></script>
        <script src="../js/widgets/widget-storage.js"></script>
        <script src="../js/widgets/widget-filter.js"></script>

        <script id="js">$(function() {

            var $table = $('table').tablesorter({
                theme: 'blue',
                widgets: ["zebra", "filter"],
                widgetOptions : {
                    // filter_anyMatch replaced! Instead use the filter_external option
                    // Set to use a jQuery selector (or jQuery object) pointing to the
                    // external filter (column specific or any match)
                    filter_external : '.search',
                    // add a default type search to the first name column
                    filter_defaultFilter: { 1 : '~{query}' },
                    // include column filters
                    filter_columnFilters: true,
                    filter_placeholder: { search : 'Search...' },
                    filter_saveFilters : true,
                    filter_reset: '.reset'
                }
            });

            // make demo search buttons work
            $('button[data-column]').on('click', function() {
                var $this = $(this),
                    totalColumns = $table[0].config.columns,
                    col = $this.data('column'), // zero-based index or "all"
                    filter = [];

                // text to add to filter
                filter[ col === 'all' ? totalColumns : col ] = $this.text();
                $table.trigger('search', [ filter ]);
                return false;
            });

        });</script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dissertatsia</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}p">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <script src="{{asset('js/popper.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('js/bootstrap.min.js')}}"></link>


    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- jsGrid -->
    <link rel="stylesheet" href="{{asset('plugins/jsgrid/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/jsgrid/jsgrid-theme.min.css')}}">
    <style>
        .tablesorter thead .disabled {display: none}
    </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <h1 class="m-0 text-dark">Shaxsiy tarkib ro'yhati</h1>
        <a href="/jangchilar/create"><button style="margin-left: 600px; " class="btn btn-primary">Qo'shish</button></a>
    </nav>
    @extends('layouts.navbar')
    @section('jangchilar','active')
    <div class="content-wrapper">
        <section class="content">
            <div class="content-header">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
            </div>
            <div class="card">
                <div class="card-header">                </div>
                    <div id="main">
                        {{--<div id="demo"><input class="search" type="search" data-column="all"> (Barcha ma'lumotlari asosida qidiruv)<br>--}}
                            <input class="search" type="search" data-column="3" placeholder="HQ bo'yicha qidiruv">
                            <input class="search" type="search" data-column="4" placeholder="Bat bo'yicha qidiruv">
                            <input class="search" type="search" data-column="5" placeholder="Vzvod bo'yicha qidiruv">
                            <input class="search" type="search" data-column="6" placeholder="Guruh bo'yicha qidiruv">
                            <input class="search" type="search" data-column="7" placeholder="Seksiya bo'yicha qidiruv">
                            <!-- targeted by the "filter_reset" option -->

                        <input class="search" type="search" data-column="2" placeholder="FISH bo'yicha qidiruv">
                        <button type="button" class="reset">Tozalash</button>
                            <div class="card-body table-responsive p-0" style="height: 500px;">

                                <table class="tablesorter ">
                                <thead>
                                <tr>
                                    <th>Unvoni</th>
                                    <th>Lavozimi</th>
                                    <th>F.I.Sh</th>
                                    <th>HQ</th>
                                    <th>Bat</th>
                                    <th>Vzvod</th>
                                    <th>Guruh</th>
                                    <th>Seksiya</th>
                                    <td class="sorter-false filter-false" style="width: 36px;"></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($jangchilars as $jangchilar)
                                    <tr  style="height: 5px">
                                        <td>{{$jangchilar->unvoni}}</td>
                                        <td>{{$jangchilar->ish_joyi}}</td>
                                        <td>{{$jangchilar->familyasi}} {{$jangchilar->ismi}} {{$jangchilar->sharifi}}</td>
                                        <td>{{ $jangchilar->harbiyqism() }}</td>
                                        <td>{{$jangchilar->battalion['nomi']}}</td>
                                        <td>{{$jangchilar->vzvod['nomi']}}</td>
                                        <td>{{$jangchilar->guruh['nomi']}}</td>
                                        <td>{{$jangchilar->seksiya['nomi']}}</td>
                                        <td style="width: 30px">
                                            <div class="row" style="padding-left:5px; margin-bottom: -10px">
                                                <a href="{{route('edit_jangchi',$jangchilar->id)}}">
                                                    <i class="ion ion-edit" data-toggle="tooltip" data-placement="top" title="O'zgartirish" style="font-size: 15px"></i>
                                                </a>
                                                <form action="{{ route('delete_jangchi',$jangchilar->id)}}" method="post" style="margin: 0px !important;">
                                                    @csrf
                                                    <input type="hidden" name="harbiy_qism" value="{{$jangchilar->harbiy_qism_id}}">
                                                    <input type="hidden" name="battalion" value="{{$jangchilar->battalion_id}}">
                                                    <input type="hidden" name="vzvod" value="{{$jangchilar->vzvod_id}}">
                                                    <input type="hidden" name="guruh" value="{{$jangchilar->guruh_id}}">
                                                    <input type="hidden" name="seksiya" value="{{$jangchilar->seksiya_id}}">
                                                    <label for="submit">
                                                        <i class="ion ion-trash-a" data-toggle="tooltip" data-placement="top" title="O'chirish" style="font-size: 15px; margin-left: 5px;margin-right: 5px; ">
                                                        </i>
                                                    </label>
                                                    <input type="submit" hidden id="submit">
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                    </div>

            </div>
        </section>
                {{--</div>--}}
    </div>
        </section>
    </div>
</div>

{{--<script src="{{asset('js/jquery.min.js')}}"></script>--}}
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- page script -->
<script src="{{asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
</body>

</html>

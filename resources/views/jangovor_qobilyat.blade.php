<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Dissertatsia</title>

    <!-- jQuery -->
    <script src="{{asset('js/jquery-latest.min.js')}}"></script>

    <!-- Demo stuff -->
    <link class="ui-theme" rel="stylesheet" href="{{asset('css/jquery-ui.min.css')}}">
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/jq.css')}}">
    <link href="{{asset('css/prettify.css')}}" rel="stylesheet">
    <script src="{{asset('js/prettify.js')}}"></script>
    <script src="{{asset('js/docs.js')}}"></script>

    <!-- Tablesorter: required -->
    <link rel="stylesheet" href="{{asset('css/theme.blue.css')}}">
    <script src="{{asset('js/jquery.tablesorter.js')}}"></script>
    <script src="{{asset('js/widgets/widget-storage.js')}}"></script>
    <script src="{{asset('js/widgets/widget-filter.js')}}"></script>

    <script id="js">
        $(function() {
            var $table = $('table').tablesorter({
                theme: 'blue',
                widgets: ["zebra", "filter"],
                widgetOptions : {
                    // filter_anyMatch replaced! Instead use the filter_external option
                    // Set to use a jQuery selector (or jQuery object) pointing to the
                    // external filter (column specific or any match)
                    filter_external : '.search',
                    // add a default type search to the first name column
                    filter_defaultFilter: { 1 : '~{query}' },
                    // include column filters
                    filter_columnFilters: true,
                    filter_placeholder: { search : 'Search...' },
                    filter_saveFilters : true,
                    filter_reset: '.reset'
                }
            });

            // make demo search buttons work
            $('button[data-column]').on('click', function() {
                var $this = $(this),
                    totalColumns = $table[0].config.columns,
                    col = $this.data('column'), // zero-based index or "all"
                    filter = [];

                // text to add to filter
                filter[ col === 'all' ? totalColumns : col ] = $this.text();
                $table.trigger('search', [ filter ]);
                return false;
            });

        });
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dissertatsia</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}p">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>--}}
    <script src="{{asset('js/popper.min.js')}}"></script>
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />--}}
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" />
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>--}}
    <script src="{{asset('js/bootstrap.min.js')}}"></script>


    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- jsGrid -->
    <link rel="stylesheet" href="{{asset('plugins/jsgrid/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/jsgrid/jsgrid-theme.min.css')}}">
    <style>
        .tablesorter thead .disabled {display: none}
    </style>
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <h1 class="m-0 text-dark">Harbiy qismlar jangvor qobiliyati</h1>
    </nav>
@extends('layouts.navbar')
@section('jangovor_qobiliyat','active')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" >
        <section class="content" >
            <div class="container-fluid" >
                <!-- Small boxes (Stat box) -->
                <div class="row" style="margin-top: 20px;">
                    @foreach($harbiy_qisms as $harbiy_qism)
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <?php $u_jis_hol=0; $u_mrh=0; $u_jq=0; $u_jt=0; $jangchi_soni=0;?>
                                    @foreach($harbiy_qism->jangchilars as $jangchilar)
                                        <?php
                                            $jis_hol=0;$mrh=0; $jt=0; $jq=0;$jis_tay=0;
                                        //jismoniy holati
                                        $jismoniy_tayyorgarlik = \App\jismoniy_holat::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($jismoniy_tayyorgarlik))
                                        {
                                            $jis_tay+= $jismoniy_tayyorgarlik[0]->umumiy_baho;
                                        }
                                        $jis_hol = $jis_tay;
                                        $charchaganlik = \App\charchaganlik::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($charchaganlik))
                                        {
                                            $jis_hol +=$charchaganlik[0]->charchaganlik_darajasi;
                                        }
                                        $soglomligi = \App\soglomligi::where('jangchi_id',$jangchilar->id)->orderBy('id', 'desc')->first();
                                        if($soglomligi)
                                        {
                                            $jis_hol +=$soglomligi->soglomligi;
                                        }
                                        $jis_hol=$jis_hol/3;
                                        //jismoniy holati
                                        //mrh
                                        $arh = \App\arh_end::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($arh)){
                                            $mrh += $arh[0]->arh;
                                        }
                                        $mmt_ozlashtirish = \App\mmt_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2018-2019')->get();
                                        if(count($mmt_ozlashtirish))
                                        {
                                            $mrh+= $mmt_ozlashtirish->sum('bahosi');
                                        }
                                        $mrh = $mrh/2.0;
                                        //mrh
                                        //jangovot t
                                            $jan_t = \App\jangovor_tayyorgarlik_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2018-2019')->get();
                                            if(count($jan_t)) $jt = $jan_t[0]->ball;
                                            $jt = $jt;
                                        //jangovot t
                                            $jq = ($jis_hol+$mrh+$jt)/3;

                                            $u_jis_hol+=$jis_hol;
                                            $u_mrh += $mrh;
                                            $u_jt += $jt;
                                            $u_jq +=$jq;
                                            $jangchi_soni++;
                                        ?>
                                    @endforeach
                                    <h4>{{$harbiy_qism->nomi}}</h4>
                                    <h6>Jangovor qobiliyati <span class="badge bg-danger float-right"><?php echo number_format((float)($u_jq/$jangchi_soni*20), 2, '.', '')?> %</span></h6>
                                    <h6>Jismoniy holati <span class="badge bg-success float-right"><?php echo number_format((float)($u_jis_hol/$jangchi_soni*20), 2, '.', '')?>%</span></h6>
                                    <h6>Ma'naviy ruhiy holati <span  class="badge bg-warning float-right"><?php echo number_format((float)($u_mrh/$jangchi_soni*20), 2, '.', '')?>%</span></h6>
                                    <h6>Jangovor tayyorgarligi <span class="badge bg-black float-right"><?php echo number_format((float)($u_jt/$jangchi_soni*20), 2, '.', '')?>%</span></h6>
                                </div>
                                <input type="hidden" value="{{$harbiy_qism->id}}" name="id">
                                <a href="jangovor_qobiliyat/harbiy_qism/{{$harbiy_qism->id}}" class="small-box-footer">Ko'rish <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="content-header">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
            </div>
            <div class="card">
                <div class="card-header"> </div>
                <div id="main">
                    <div class="card-body table-responsive p-0" style="height: 500px;">
                        <table class="tablesorter">
                            <thead>
                                <tr>
                                    <th>T/R</th>
                                    <th>Harbiy qism nomi</th>
                                    <th>Jismoniy tayyorgarlik</th>
                                    <th>Ma'naviy ma'rifiy tayyorgarlik</th>
                                    <th>Jangovor tayyorgarligi</th>
                                    <th>Jangovor qobiliyat</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($harbiy_qisms as $harbiy_qism)

                                <tr  style="height: 5px">
                                    <td>{{$i++}}</td>
                                    <td>{{$harbiy_qism->nomi}}</td>
                                    <?php $u_jis_tay=0;$u_mmt=0; $u_jt=0; $u_jq=0; $jangchi_soni=0;?>
                                    @foreach($harbiy_qism->jangchilars as $jangchilar)
                                        <?php
                                        $jis_tay=0;$mmt=0; $jt=0; $jq=0;
                                        $jismoniy_tayyorgarlik = \App\jismoniy_holat::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($jismoniy_tayyorgarlik))
                                        {
                                            $jis_tay+= $jismoniy_tayyorgarlik[0]->umumiy_baho;
                                        }
                                        $mmt_ozlashtirish = \App\mmt_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2018-2019')->get();
                                        if(count($mmt_ozlashtirish))
                                        {
                                            $mmt+= $mmt_ozlashtirish->sum('bahosi');
                                        }
                                        $jangovor_tay = \App\jangovor_tayyorgarlik_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2018-2019')->get();
                                        if(count($jangovor_tay))
                                        {
                                            $jt+= $jangovor_tay->sum('ball');
                                        }
                                        $ch=0;$s=0;$a=0;
                                        $charchaganlik = \App\charchaganlik::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($charchaganlik))
                                        {
                                            $ch =$charchaganlik[0]->charchaganlik_darajasi;
                                        }
                                        $soglomligi = \App\soglomligi::where('jangchi_id',$jangchilar->id)->orderBy('id', 'desc')->first();
                                        if($soglomligi)
                                        {
                                            $s =$soglomligi->soglomligi;
                                        }
                                        $arh = \App\arh_end::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($arh)){
                                            $a = $arh[0]->arh;
                                        }
                                        $jh = ($jis_tay+$ch+$s)/3;
                                        $mrh = ($mmt+$a)/2;
                                        $jq = ($jh+$mrh+$jt)/3;

                                        $u_jis_tay +=$jis_tay;
                                        $u_mmt +=$mmt;
                                        $u_jt +=$jt;

                                        $u_jq +=$jq;
                                        $jangchi_soni++;
                                        ?>
                                    @endforeach
                                    <td><?php echo number_format((float)($u_jis_tay)/$jangchi_soni*20, 2, '.', '')?>%</td>

                                    <td><?php echo number_format((float)($u_mmt)/$jangchi_soni*20, 2, '.', '')?>%</td>
                                    <td><?php echo number_format((float)($u_jt)/$jangchi_soni*20, 2, '.', '')?>%</td>
                                    <td>
                                        <?php echo number_format((float)($u_jq)/$jangchi_soni*20, 2, '.', '')?>%
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <?php $i=1;?>
            <div class="card">
                <div class="card-header">                </div>
                <div id="main">
                    <div class="card-body table-responsive p-0" style="height: 500px;">
                        <table class="tablesorter">
                            <thead>
                                <tr>
                                    <th>T/R</th>
                                    <th>Harbiy qism nomi</th>
                                    <th>Unvoni</th>
                                    <th>F.I.SH</th>
                                    <th>Jismoniy tayyorgarlik</th>
                                    <th>Ma'naviy ma'rifiy tayyorgarlik</th>
                                    <th>Jangovor tayyorgarligi</th>
                                    <th>Jangovor qobiliyat</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($jangchilars as $jangchilar)
                                <tr  style="height: 5px">
                                    <td>{{$i++}}</td>
                                    <td>{{$jangchilar->unvoni}}</td>
                                    <td>{{$jangchilar->ish_joyi}}</td>
                                    <td>{{$jangchilar->familyasi}} {{$jangchilar->ismi}} {{$jangchilar->sharifi}}</td>
                                    <td>
                                        <?php
                                        $jismoniy_tayyorgarlik = \App\jismoniy_holat::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($jismoniy_tayyorgarlik))
                                        {
                                            echo number_format((float)($jismoniy_tayyorgarlik[0]->umumiy_baho*20), 2, '.', '');
                                            echo "%";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $mmt_ozlashtirish = \App\mmt_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2018-2019')->get();
                                        if(count($mmt_ozlashtirish))
                                        {
                                            echo number_format((float)($mmt_ozlashtirish->sum('bahosi')*20), 2, '.', '');
                                            echo "%";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $jangovor_t = \App\jangovor_tayyorgarlik_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2018-2019')->get();
                                        if(count($jangovor_t))
                                        {
                                            echo number_format((float)($jangovor_t->sum('ball')*20), 2, '.', '');
                                            echo "%";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $jis_hol=0;     $jis_tay=0;
                                        $jismoniy_tayyorgarlik = \App\jismoniy_holat::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($jismoniy_tayyorgarlik))
                                        {
                                            $jis_tay+= $jismoniy_tayyorgarlik[0]->umumiy_baho;
                                        }
                                        $jis_hol = $jis_tay;
                                        $charchaganlik = \App\charchaganlik::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($charchaganlik))
                                        {
                                            $jis_hol +=$charchaganlik[0]->charchaganlik_darajasi;
                                        }
                                        $soglomligi = \App\soglomligi::where('jangchi_id',$jangchilar->id)->orderBy('id', 'desc')->first();
                                        if($soglomligi)
                                        {
                                            $jis_hol +=$soglomligi->soglomligi;
                                        }
                                        $jis_hol=$jis_hol/3;
                                        $mrh=0;
                                        $arh = \App\arh_end::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($arh)){
                                            $mrh += $arh[0]->arh;
                                        }
                                        $mmt_ozlashtirish = \App\mmt_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2018-2019')->get();
                                        $mrh+=$mmt_ozlashtirish->sum('bashosi');

                                        $jangovor_t = \App\jangovor_tayyorgarlik_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2018-2019')->get();
//

                                        $jt=0;
                                        if(count($jangovor_t))
                                        {
                                            $jt = $jangovor_t->sum('ball');
                                        }
                                        $jq = ($jis_hol+$mrh+$jt)/3*20;
                                        echo number_format((float)($jq), 2, '.', '');
                                        ?>%
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <?php $i=1;?>
            <div class="card">
                <div class="card-header"> </div>
                <div id="main">

                    <div class="card-body table-responsive p-0" style="height: 500px;">
                        <table class="tablesorter ">
                            <thead>
                            <tr>
                                <th rowspan="2">T/R</th>
                                <th rowspan="2">Harbiy qism nomi</th>
                                <th rowspan="2">Unvoni</th>
                                <th rowspan="2">F.I.SH</th>
                                <th colspan="3">Jismoniy tayyorgarlik</th>
                                <th colspan="3">Ma'naviy ma'rifiy tayyorgarlik</th>
                                <th colspan="3">Jangovor tayyorgarligi</th>
                                <th colspan="3">Jangovor qobiliyat</th>
                            </tr>
                            <tr>
                                <th>2017-18</th>
                                <th>2018-19</th>
                                <th>farqi</th>
                                <th>2017-18</th>
                                <th>2018-19</th>
                                <th>farqi</th>
                                <th>2017-18</th>
                                <th>2018-19</th>
                                <th>farqi</th>
                                <th>2017-18</th>
                                <th>2018-19</th>
                                <th>farqi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($jangchilars as $jangchilar)
                                <tr  style="height: 5px">
                                    <td>{{$i++}}</td>
                                    <td>{{$jangchilar->unvoni}}</td>
                                    <td>{{$jangchilar->ish_joyi}}</td>
                                    <td>{{$jangchilar->familyasi}} {{$jangchilar->ismi}} {{$jangchilar->sharifi}}</td>
                                    <td>
                                        <?php
                                            $jismoniy_tayyorgarlik = \App\jismoniy_holat::where('jangchi_id',$jangchilar->id)->get();
                                            if(count($jismoniy_tayyorgarlik))
                                            {
                                                echo number_format((float)($jismoniy_tayyorgarlik[0]->umumiy_baho*20), 2, '.', '');
                                                echo "%";
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $jismoniy_tayyorgarlik = \App\jismoniy_holat::where('jangchi_id',$jangchilar->id)->get();
                                            if(count($jismoniy_tayyorgarlik))
                                            {
                                                echo number_format((float)($jismoniy_tayyorgarlik[0]->umumiy_baho*20), 2, '.', '');
                                                echo "%";
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $jismoniy_tayyorgarlik1 = \App\jismoniy_holat::where('jangchi_id',$jangchilar->id)->get();
                                            $jismoniy_tayyorgarlik2 = \App\jismoniy_holat::where('jangchi_id',$jangchilar->id)->get();
                                            if(count($jismoniy_tayyorgarlik1)&&count($jismoniy_tayyorgarlik2))
                                            {
                                                if($jismoniy_tayyorgarlik1[0]->umumiy_baho)
                                                {
                                                    echo number_format((float)($jismoniy_tayyorgarlik2[0]->umumiy_baho*20-$jismoniy_tayyorgarlik1[0]->umumiy_baho*20), 2, '.', '');
                                                    echo "%";
                                                }
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $mmt_ozlashtirish = \App\mmt_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2016-2017')->get();
                                            if(count($mmt_ozlashtirish))
                                            {
                                                echo number_format((float)($mmt_ozlashtirish->sum('bahosi')*20), 2, '.', '');
                                                echo "%";
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $mmt_ozlashtirish = \App\mmt_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2017-2018')->get();
                                            if(count($mmt_ozlashtirish))
                                            {
                                                echo number_format((float)($mmt_ozlashtirish->sum('bahosi')*20), 2, '.', '');
                                                echo "%";
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $mmt_ozlashtirish1 = \App\mmt_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2016-2017')->get();
                                            $mmt_ozlashtirish2 = \App\mmt_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2017-2018')->get();
                                            if(count($mmt_ozlashtirish1)&&count($mmt_ozlashtirish2))
                                            {
                                                if($mmt_ozlashtirish1->sum('bahosi'))
                                                {
                                                    echo number_format((float)($mmt_ozlashtirish2->sum('bahosi')*20-$mmt_ozlashtirish1->sum('bahosi')*20), 2, '.', '');
                                                    echo "%";
                                                }
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $jangovor_t = \App\jangovor_tayyorgarlik_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2016-2017')->get();
                                            if(count($jangovor_t))
                                            {
                                                echo number_format((float)($jangovor_t->sum('ball')*20), 2, '.', '');
                                                echo "%";
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $jangovor_t = \App\jangovor_tayyorgarlik_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2017-2018')->get();
                                            if(count($jangovor_t))
                                            {
                                                echo number_format((float)($jangovor_t->sum('ball')*20), 2, '.', '');
                                                echo "%";
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $jangovor_t1 = \App\jangovor_tayyorgarlik_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2016-2017')
                                                                                                  ->where('mutaxassislik',$jangchilar->mutaxassislik)->get();
                                            $jangovor_t2 = \App\jangovor_tayyorgarlik_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2017-2018')
                                                                                                  ->where('mutaxassislik',$jangchilar->mutaxassislik)->get();
                                            if(count($jangovor_t2)&&count($jangovor_t1))
                                            {
                                                $jj=0;
                                                if ($jangovor_t1->sum('ball'))
                                                    {
                                                        $jj =$jangovor_t2->sum('ball')*20-$jangovor_t1->sum('ball')*20;
                                                        echo number_format((float)($jj), 2, '.', '');
                                                        echo "%";
                                                    }
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $jis_hol=0;     $jis_tay=0;
                                        $jismoniy_tayyorgarlik = \App\jismoniy_holat::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($jismoniy_tayyorgarlik))
                                        {
                                            $jis_tay+= $jismoniy_tayyorgarlik[0]->umumiy_baho;
                                        }
                                        $jis_hol = $jis_tay;
                                        $charchaganlik = \App\charchaganlik::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($charchaganlik))
                                        {
                                            $jis_hol +=$charchaganlik[0]->charchaganlik_darajasi;
                                        }
                                        $soglomligi = \App\soglomligi::where('jangchi_id',$jangchilar->id)->orderBy('id', 'desc')->first();
                                        if($soglomligi)
                                        {
                                            $jis_hol +=$soglomligi->soglomligi;
                                        }
                                        $jis_hol=$jis_hol/3;

                                        $mrh=0;
                                        $arh = \App\arh_end::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($arh)){
                                            $mrh += $arh[0]->arh;
                                        }
                                        $mmt_ozlashtirish = \App\mmt_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2016-2017')->get();
                                        $mrh+=$mmt_ozlashtirish->sum('bashosi');

                                        $jangovor_t = \App\jangovor_tayyorgarlik_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2016-2017')->get();
                                        //
                                        $jt=0;
                                        if(count($jangovor_t))
                                        {
                                            $jt = $jangovor_t->sum('ball');
                                        }
                                        $jq1=0;
                                        $jq1 = ($jis_hol+$mrh+$jt)/3*20;
                                        echo number_format((float)($jq1), 2, '.', '');
                                        ?>%
                                    </td>
                                    <td>
                                        <?php
                                        $jis_hol=0;     $jis_tay=0;
                                        $jismoniy_tayyorgarlik = \App\jismoniy_holat::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($jismoniy_tayyorgarlik))
                                        {
                                            $jis_tay+= $jismoniy_tayyorgarlik[0]->umumiy_baho;
                                        }
                                        $jis_hol = $jis_tay;
                                        $charchaganlik = \App\charchaganlik::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($charchaganlik))
                                        {
                                            $jis_hol +=$charchaganlik[0]->charchaganlik_darajasi;
                                        }
                                        $soglomligi = \App\soglomligi::where('jangchi_id',$jangchilar->id)->orderBy('id', 'desc')->first();
                                        if($soglomligi)
                                        {
                                            $jis_hol +=$soglomligi->soglomligi;
                                        }
                                        $jis_hol=$jis_hol/3;

                                        $mrh=0;
                                        $arh = \App\arh_end::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($arh)){
                                            $mrh += $arh[0]->arh;
                                        }
                                        $mmt_ozlashtirish = \App\mmt_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2017-2018')->get();
                                        $mrh+=$mmt_ozlashtirish->sum('bashosi');

                                        $jangovor_t = \App\jangovor_tayyorgarlik_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2017-2018')->get();
                                        //
                                        $jt=0;
                                        if(count($jangovor_t))
                                        {
                                            $jt = $jangovor_t->sum('ball');
                                        }
                                        $jq2=0;
                                        $jq2 = ($jis_hol+$mrh+$jt)/3*20;
                                        echo number_format((float)($jq2), 2, '.', '');
                                        ?>%
                                    </td>
                                    <td>
                                        <?php
                                            echo number_format((float)($jq2-$jq1), 2, '.', '');
                                        ?>%
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <?php $i=1;?>
            <div class="card">
                <div class="card-header"> </div>
                <div id="main">

                    <div class="card-body table-responsive p-0" style="height: 500px;">

                        <table class="tablesorter ">
                            <thead>
                            <tr>
                                <th rowspan="2">T/R</th>
                                <th rowspan="2">Harbiy qism nomi</th>
                                <th colspan="3">Jismoniy tayyorgarlik</th>
                                <th colspan="3">Ma'naviy ma'rifiy tayyorgarlik</th>
                                <th colspan="3">Jangovor tayyorgarligi</th>
                                <th colspan="3">Jangovor qobiliyat</th>
                            </tr>
                            <tr>
                                <th>2017/2018</th>
                                <th>2018/2019</th>
                                <th>farqi</th>
                                <th>2017/2018</th>
                                <th>2018/2019</th>
                                <th>farqi</th>
                                <th>2017/2018</th>
                                <th>2018/2019</th>
                                <th>farqi</th>
                                <th>2017/2018</th>
                                <th>2018/2019</th>
                                <th>farqi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($harbiy_qisms as $harbiy_qism)

                                <tr  style="height: 5px">
                                    <td>{{$i++}}</td>
                                    <td>{{$harbiy_qism->nomi}}</td>
                                    <?php
                                            $u_jis_tay1=0;$u_mmt1=0; $u_jt1=0; $u_jq1=0; $jangchi_soni1=0;
                                            $u_jis_tay2=0;$u_mmt2=0; $u_jt2=0; $u_jq2=0; $jangchi_soni2=0;
                                    ?>
                                    @foreach($harbiy_qism->jangchilars as $jangchilar)
                                        <?php
                                        $jis_tay1=0;$mmt1=0; $jt1=0; $jq1=0;
                                        $jis_tay2=0;$mmt2=0; $jt2=0; $jq2=0;
                                        $jismoniy_tayyorgarlik1 = \App\jismoniy_holat::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($jismoniy_tayyorgarlik1))
                                        {
                                            $jis_tay1+= $jismoniy_tayyorgarlik1[0]->umumiy_baho;
                                        }
                                        $jismoniy_tayyorgarlik2 = \App\jismoniy_holat::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($jismoniy_tayyorgarlik2))
                                        {
                                            $jis_tay2+= $jismoniy_tayyorgarlik2[0]->umumiy_baho;
                                        }
                                        ////////////////////
                                        $mmt_ozlashtirish1 = \App\mmt_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2016-2017')->get();
                                        if(count($mmt_ozlashtirish1))
                                        {
                                            $mmt1+= $mmt_ozlashtirish1->sum('bahosi');
                                        }
                                        $mmt_ozlashtirish = \App\mmt_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2017-2018')->get();
                                        if(count($mmt_ozlashtirish2))
                                        {
                                            $mmt2+= $mmt_ozlashtirish2->sum('bahosi');
                                        }
                                        //////
                                        $jangovor_tay1 = \App\jangovor_tayyorgarlik_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2016-2017')->get();
                                        if(count($jangovor_tay1))
                                        {
                                            $jt1+= $jangovor_tay1->sum('ball');
                                        }
                                        $jangovor_tay2 = \App\jangovor_tayyorgarlik_ozlashtirish::where('jangchi_id',$jangchilar->id)->where('oquv_yili','2017-2018')->get();
                                        if(count($jangovor_tay2))
                                        {
                                            $jt2+= $jangovor_tay2->sum('ball');
                                        }
                                        //////////////////////////////
                                        $ch1=0;$s1=0;$a1=0; $ch2=0;$s2=0;$a2=0;
                                        $charchaganlik1 = \App\charchaganlik::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($charchaganlik1))
                                        {
                                            $ch1 =$charchaganlik1[0]->charchaganlik_darajasi;
                                        }
                                        $soglomligi1 = \App\soglomligi::where('jangchi_id',$jangchilar->id)->orderBy('id', 'desc')->first();
                                        if($soglomligi1)
                                        {
                                            $s1 =$soglomligi1->soglomligi;
                                        }
                                        $arh1 = \App\arh_end::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($arh1)){
                                            $a1 = $arh1[0]->arh;
                                        }
                                        $charchaganlik2 = \App\charchaganlik::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($charchaganlik2))
                                        {
                                            $ch2 =$charchaganlik2[0]->charchaganlik_darajasi;
                                        }
                                        $soglomligi2 = \App\soglomligi::where('jangchi_id',$jangchilar->id)->orderBy('id', 'desc')->first();
                                        if($soglomligi2)
                                        {
                                            $s2 =$soglomligi2->soglomligi;
                                        }
                                        $arh2 = \App\arh_end::where('jangchi_id',$jangchilar->id)->get();
                                        if(count($arh2)){
                                            $a2 = $arh2[0]->arh;
                                        }
                                        $jh1 = ($jis_tay1+$ch1+$s1)/3;  $jh2 = ($jis_tay2+$ch2+$s2)/3;
                                        $mrh1 = ($mmt1+$a1)/2;         $mrh2 = ($mmt2+$a2)/2;
                                        $jq1 = ($jh1+$mrh1+$jt1)/3;     $jq2 = ($jh2+$mrh2+$jt2)/3;

                                        $u_jis_tay1 +=$jis_tay1;          $u_jis_tay2 +=$jis_tay2;
                                        $u_mmt1 +=$mmt1;                  $u_mmt2 +=$mmt2;
                                        $u_jt1 +=$jt1;                    $u_jt2 +=$jt2;

                                        $u_jq1 +=$jq1;                    $u_jq2 +=$jq2;
                                        $jangchi_soni1++;                $jangchi_soni2++;
                                        ?>
                                    @endforeach
                                    <td><?php echo number_format((float)($u_jis_tay1)/$jangchi_soni1*20, 2, '.', '')?>%</td>
                                    <td><?php echo number_format((float)($u_jis_tay2)/$jangchi_soni2*20, 2, '.', '')?>%</td>
                                    <td><?php echo number_format((float)(($u_jis_tay2)/$jangchi_soni2*20-($u_jis_tay1)/$jangchi_soni1*20), 2, '.', '')?>%</td>

                                    <td><?php echo number_format((float)($u_mmt1)/$jangchi_soni1*20, 2, '.', '')?>%</td>
                                    <td><?php echo number_format((float)($u_mmt2)/$jangchi_soni2*20, 2, '.', '')?>%</td>
                                    <td><?php echo number_format((float)(($u_mmt2)/$jangchi_soni2*20-($u_mmt1)/$jangchi_soni1*20), 2, '.', '')?>%</td>

                                    <td><?php echo number_format((float)($u_jt1)/$jangchi_soni1*20, 2, '.', '')?>%</td>
                                    <td><?php echo number_format((float)($u_jt2)/$jangchi_soni2*20, 2, '.', '')?>%</td>
                                    <td><?php echo number_format((float)(($u_jt2)/$jangchi_soni2*20-($u_jt1)/$jangchi_soni1*20), 2, '.', '')?>%</td>

                                    <td>
                                        <?php echo number_format((float)($u_jq1)/$jangchi_soni1*20, 2, '.', '')?>%
                                    </td>
                                    <td>
                                        <?php echo number_format((float)($u_jq2)/$jangchi_soni2*20, 2, '.', '')?>%
                                    </td>
                                    <td>
                                        <?php echo number_format((float)(($u_jq2)/$jangchi_soni2*20-($u_jq1)/$jangchi_soni1*20), 2, '.', '')?>%
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </section>
    </div>
</div>
{{--<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>--}}
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

</script>
</body>
</html>

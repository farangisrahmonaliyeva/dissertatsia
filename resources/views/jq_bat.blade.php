<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Dissertatsia</title>

    <!-- jQuery -->
    <script src="{{asset('js/jquery-latest.min.js')}}"></script>

    <!-- Demo stuff -->
    <link class="ui-theme" rel="stylesheet" href="{{asset('css/jquery-ui.min.css')}}">
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/jq.css')}}">
    <link href="{{asset('css/prettify.css')}}" rel="stylesheet">
    <script src="{{asset('js/prettify.js')}}"></script>
    <script src="{{asset('js/docs.js')}}"></script>

    <!-- Tablesorter: required -->
    <link rel="stylesheet" href="{{asset('css/theme.blue.css')}}">
    <script src="{{asset('js/jquery.tablesorter.js')}}"></script>
    <script src="{{asset('js/widgets/widget-storage.js')}}"></script>
    <script src="{{asset('js/widgets/widget-filter.js')}}"></script>

    <script id="js">
        $(function() {
            var $table = $('table').tablesorter({
                theme: 'blue',
                widgets: ["zebra", "filter"],
                widgetOptions : {
                    // filter_anyMatch replaced! Instead use the filter_external option
                    // Set to use a jQuery selector (or jQuery object) pointing to the
                    // external filter (column specific or any match)
                    filter_external : '.search',
                    // add a default type search to the first name column
                    filter_defaultFilter: { 1 : '~{query}' },
                    // include column filters
                    filter_columnFilters: true,
                    filter_placeholder: { search : 'Search...' },
                    filter_saveFilters : true,
                    filter_reset: '.reset'
                }
            });

            // make demo search buttons work
            $('button[data-column]').on('click', function() {
                var $this = $(this),
                    totalColumns = $table[0].config.columns,
                    col = $this.data('column'), // zero-based index or "all"
                    filter = [];

                // text to add to filter
                filter[ col === 'all' ? totalColumns : col ] = $this.text();
                $table.trigger('search', [ filter ]);
                return false;
            });

        });
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dissertatsia</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}p">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- jsGrid -->
    <link rel="stylesheet" href="{{asset('plugins/jsgrid/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/jsgrid/jsgrid-theme.min.css')}}">
    <style>
        .tablesorter thead .disabled {display: none}
    </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <h1 class="m-0 text-dark">{{$hq->nomi}} jangvor qobiliyati</h1>
        {{--<a href="/jangchilar/create"><button style="margin-left: 600px; " class="btn btn-primary">Qo'shish</button></a>--}}
    </nav>
@extends('layouts.navbar')
@section('jangovor_qobiliyat','active')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" >
        <section class="content" >
            <div class="container-fluid" >
                <!-- Small boxes (Stat box) -->
                <div class="row" style="margin-top: 20px;">
                    @foreach($battalions as $harbiy_qism)
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h4>{{$harbiy_qism->nomi}}</h4>
                                    <h6>Jangovor qobiliyati <span class="badge bg-danger">55%</span></h6>
                                    <h6>Jismoniy xolati <span class="badge bg-success">55%</span></h6>
                                    <h6>MRH <span class="badge bg-warning">55%</span></h6>
                                    <h6>Jangovor tayyorgarligi <span class="badge bg-black">55%</span></h6>
                                </div>

                                {{--<div class="icon">--}}
                                    {{--<i class="ion ion-clipboard"></i>--}}
                                {{--</div>--}}
                                <input type="hidden" value="{{$harbiy_qism->id}}" name="id">

                                <a href="{{route('jq_vzvod',$harbiy_qism->id)}}" class="small-box-footer">Ko'rish <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="content-header">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
            </div>
            <div class="card">
                <div class="card-header"> </div>
                <div id="main">

                    <div class="card-body table-responsive p-0" style="height: 500px;">

                        <table class="tablesorter ">
                            <thead>
                            <tr>
                                <th>Batalyon nomi</th>
                                <th>Jangovor qobilyati</th>
                                <th>Jismoniy xolati</th>
                                <th>M.R.H</th>
                                <th>Jangovor tayyorgarligi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($battalions as $harbiy_qism)
                                <tr  style="height: 5px">
                                    <td>{{$harbiy_qism->nomi}}</td>
                                    <td>{{$harbiy_qism->jangovor_qobilyati}}</td>
                                    <td>{{$harbiy_qism->jismoniy_holati}}</td>
                                    <td>{{$harbiy_qism->manaviy_ruxiy_holati}}</td>
                                    <td>{{$harbiy_qism->jangovor_tayyorgarligi}}</td>
                                    {{--<td>{{$harbiy_qism->nomi}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="card">
                <div class="card-header">                </div>
                <div id="main">
                    <div class="card-body table-responsive p-0" style="height: 500px;">

                        <table class="tablesorter">
                            <thead>
                            <tr>
                                <th>Unvoni</th>
                                <th>Lavozimi</th>
                                <th>F.I.Sh</th>
                                <th>Jangovor qobilyati</th>
                                <th>Jismoniy xolati</th>
                                <th>M.R.H</th>
                                <th>Jangovor t</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($jangchilars as $jangchilar)
                                <tr  style="height: 5px">
                                    <td>{{$jangchilar->unvoni}}</td>
                                    <td>{{$jangchilar->ish_joyi}}</td>
                                    <td>{{$jangchilar->familyasi}} {{$jangchilar->ismi}} {{$jangchilar->sharifi}}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </section>

    </div>

</div>
{{--<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>--}}
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

</script>
</body>
</html>

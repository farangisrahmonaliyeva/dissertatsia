<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <img src="../../dist/img/AdminLTELogo.png"
             alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">DISSERTATSIA</span>
    </a>
<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="#" class="d-block">Meliqo'ziyev Rustam</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->

            <li class="nav-item">
                <a href="/" class="nav-link @yield('home')">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Home
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/" class="nav-link @yield('qurollanish')">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Qurollanish imkoniyati
                    </p>
                </a>
            </li>
            <li class="nav-item ">
                <a href="/jangovor_qobiliyat" class="nav-link @yield('jangovor_qobiliyat')">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Jangovor qobilyati
                    </p>
                </a>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link @yield('qurol')">
                    <i class="nav-icon fas fa-copy"></i>
                    <p>
                        Qurollar
                        <i class="fas fa-angle-left right "></i>
                        <span class="badge badge-info right">6</span>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/tank" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Tank</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/btr" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>BTR</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/pjm" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>PJM</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/oqotar_qurol" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>O'q otar qurol</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/tankka_qarshi_qurol" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Tankka qarshi qurol</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/minamyot" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Minamyot va artileriya</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/artileriya_oziyurar" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>O'zi harakatlanadigan artileriya</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="/jangchilar" class="nav-link @yield('jangchilar')">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Shaxsiy tarkib
                        {{--<span class="right badge badge-danger">New</span>--}}
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/baholash" class="nav-link @yield('baholash')">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Baholash
                        {{--<span class="right badge badge-danger">New</span>--}}
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/mashgulotlar" class="nav-link @yield('mashgulotlar')">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Mashg'ulotlar
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/bolinma" class="nav-link @yield('bolinma')">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Bo'linmalar
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/ogirlik_koeffitsenti" class="nav-link @yield('ogirlik_koeffitsenti')">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Og'irlik K.ni kiritish
                    </p>
                </a>
            </li>
        </ul>
        <ul>

        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>
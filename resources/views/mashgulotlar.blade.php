<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Dissertatsia</title>

    <!-- jQuery -->
    <script src="{{asset('js/jquery-latest.min.js')}}"></script>

    <!-- Demo stuff -->
    <link class="ui-theme" rel="stylesheet" href="{{asset('css/jquery-ui.min.css')}}">
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/jq.css')}}">
    <link href="{{asset('css/prettify.css')}}" rel="stylesheet">
    <script src="{{asset('js/prettify.js')}}"></script>
    <script src="{{asset('js/docs.js')}}"></script>

    <!-- Tablesorter: required -->
    <link rel="stylesheet" href="{{asset('css/theme.blue.css')}}">
    <script src="{{asset('js/jquery.tablesorter.js')}}"></script>
    <script src="{{asset('js/widgets/widget-storage.js')}}"></script>
    <script src="{{asset('js/widgets/widget-filter.js')}}"></script>

    <script id="js">
        $(function() {
            var $table = $('table').tablesorter({
                theme: 'blue',
                widgets: ["zebra", "filter"],
                widgetOptions : {
                    // filter_anyMatch replaced! Instead use the filter_external option
                    // Set to use a jQuery selector (or jQuery object) pointing to the
                    // external filter (column specific or any match)
                    filter_external : '.search',
                    // add a default type search to the first name column
                    filter_defaultFilter: { 1 : '~{query}' },
                    // include column filters
                    filter_columnFilters: true,
                    filter_placeholder: { search : 'Search...' },
                    filter_saveFilters : true,
                    filter_reset: '.reset'
                }
            });

            // make demo search buttons work
            $('button[data-column]').on('click', function() {
                var $this = $(this),
                    totalColumns = $table[0].config.columns,
                    col = $this.data('column'), // zero-based index or "all"
                    filter = [];

                // text to add to filter
                filter[ col === 'all' ? totalColumns : col ] = $this.text();
                $table.trigger('search', [ filter ]);
                return false;
            });

        });
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dissertatsia</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}p">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- jsGrid -->
    <link rel="stylesheet" href="{{asset('plugins/jsgrid/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/jsgrid/jsgrid-theme.min.css')}}">
    <style>
        .tablesorter thead .disabled {display: none}
    </style>
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <h1 class="m-0 text-dark">Mashg'ulotlar</h1>
    </nav>
@extends('layouts.navbar')
    @section('mashgulotlar','active')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="content-header">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
            </div>
            <div class="container">
                {{--<h2>Toggleable Tabs</h2>--}}
                <br>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#mmt">Ma'naviy ma'rifiy tayyorgarligi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#jt">Jangovor tayyorgarlik</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="mmt" class="container tab-pane active"><br>
                        <div class="row">
                            <div class="col-5">
                                <div class="card border-primary mb-3"  style="height: 230px;">
                                    <div class="card-body text-primary">
                                        <form action="add/mmt_mavzu" method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="form-group" style="padding-right: 10px;">
                                                    <select name="oquv_yili" class="form-control">
                                                        <option value="">O'quv yili</option>
                                                        <option value="2016-2017">2016-2017</option>
                                                        <option value="2017-2018">2017-2018</option>
                                                        <option value="2018-2019">2018-2019</option>
                                                        <option value="2019-2020">2019-2020</option>
                                                        <option value="2020-2021">2020-2021</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <input required type="text" name="mavzu_raqami" class="form-control" style="width: 245px;" placeholder="Raqami">
                                                </div>
                                                <div class="form-group mb-3">
                                                    <input required name="mavzu" id="mavzu" type="text" class="form-control" style="width: 393px" placeholder="Mavzu nomi">
                                                </div>
                                                <div class="form-group" >
                                                        <button class="btn btn-primary" type="submit">Qo'shish</button>
                                                    <button type="button" class="btn btn-primary ml-2" data-toggle="modal" data-target="#editmmtmavzuModal">
                                                        O'zgartirish
                                                    </button>
                                                    <button type="button" class="btn btn-primary ml-2" data-toggle="modal" data-target="#deletemmtmavzuModal">
                                                        O'chirish
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- Modal -->
                                        <div class="modal fade" id="editmmtmavzuModal" tabindex="-1" id="ajax-crud-modal" role="dialog" aria-labelledby="editmmtmavzuModal" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="editmmtmavzuModal">M.M.T. mavzusini o'zgartirish</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="edit/mmt_mavzu" id="editmmtmavzuModal" method="POST" class="m-5" >
                                                        @csrf
                                                        <div class="row">
                                                            <div class="form-group" style="padding-right: 10px;">
                                                                <select name="oquv_yili" class="form-control">
                                                                    <option value="">O'quv yili</option>
                                                                    <option value="2016-2017">2016-2017</option>
                                                                    <option value="2017-2018">2017-2018</option>
                                                                    <option value="2018-2019">2018-2019</option>
                                                                    <option value="2019-2020">2019-2020</option>
                                                                    <option value="2020-2021">2020-2021</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <select class="form-control" name="mavzu_id" required>
                                                                    <option>Mavzu raqami</option>
                                                                    @foreach($mmt_mavzus as $mavzu)
                                                                        <option value="{{$mavzu->id}}">{{$mavzu->mavzu_raqami}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <input required type="text" name="mavzu_raqami" class="form-control ml-2" style="width: 245px;" placeholder="Raqami">
                                                            </div>
                                                            <div class="form-group mb-3">
                                                                <input required name="mavzu" id="mavzu" type="text" class="form-control" style="width: 393px" placeholder="Mavzu nomi">
                                                            </div>
                                                            <div class="form-group" >
                                                                <button class="btn btn-primary" type="submit">Qo'shish</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="deletemmtmavzuModal" tabindex="-1" role="dialog" aria-labelledby="deletemmtmavzuModal" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="deletemmtmavzuModal">M.M.T. mavzusini o'chirish</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="delete/mmt_mavzu" method="POST" class="m-5" >
                                                        @csrf
                                                        <div class="row">
                                                            <div class="form-group">
                                                                <select class="form-control" name="mavzu_id" required>
                                                                    <option>Mavzu raqami</option>
                                                                    @foreach($mmt_mavzus as $mavzu)
                                                                        <option value="{{$mavzu->id}}">{{$mavzu->mavzu_raqami}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <button type="submit" class="btn btn-primary ml-2" data-toggle="modal" data-target="#exampleModal">
                                                                    O'chirish
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col">
                                <div class="card border-primary mb-3"  style="height: 230px;">
                                    <div class="card-body text-primary">
                                        <form action="/add/mmt_mashgulot" method="POST">
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            @csrf
                                            <div class="row">
                                                <div class="form-group" style="padding-right: 10px;">
                                                    <select name="oquv_yili" class="form-control">
                                                        <option value="">O'quv yili</option>
                                                        <option value="2016-2017">2016-2017</option>
                                                        <option value="2017-2018">2017-2018</option>
                                                        <option value="2018-2019">2018-2019</option>
                                                        <option value="2019-2020">2019-2020</option>
                                                        <option value="2020-2021">2020-2021</option>
                                                    </select>
                                                </div>
                                                <div class="form-group" style="padding-right: 10px; ">
                                                    <select class="form-control" name="mavzu_id" required>
                                                        <option>Mavzu raqami</option>
                                                        @foreach($mmt_mavzus as $mavzu)
                                                            <option value="{{$mavzu->id}}">{{$mavzu->mavzu_raqami}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                {{--<div class="form-group" style="width:60%">--}}
                                                    {{--<input required type="text" disabled placeholder="Mavzu nomi" class="form-control">--}}
                                                {{--</div>--}}
                                                <div class="form-group" style="padding-right: 10px;">
                                                    <input required placeholder="Mashg'ulot nomi" style="width: 510px;" name="nomi" class="form-control" type="text">
                                                 </div>
                                                <div class="form-group" style="padding-right: 10px; ">
                                                    <input required name="soati" placeholder="Mashg'ulot soati" class="form-control" type="number">
                                                </div>
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">Qo'shish</button>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary ml-2" data-toggle="modal" data-target="#editmmtModal">
                                                        O'zgartirish
                                                    </button>
                                                    <button type="button" class="btn btn-primary ml-2" data-toggle="modal" data-target="#deletemmtModal">
                                                        O'chirish
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- Modal -->
                                        <div class="modal fade" id="deletemmtModal" tabindex="-1" role="dialog" aria-labelledby="deletemmtModal" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="deletemmtmavzuModal">M.M.T. mashg'ulotni o'chirish</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="delete/mmt_mash" method="POST" class="m-5" >
                                                        @csrf
                                                        <div class="row">
                                                            <div class="form-group">
                                                                <select class="form-control" name="mavzu_id" required>
                                                                    <option>Mavzu raqami</option>
                                                                    @foreach($mmt_mavzus as $mavzu)
                                                                        <option value="{{$mavzu->id}}">{{$mavzu->mavzu_raqami}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <button type="submit" class="btn btn-primary ml-2" data-toggle="modal" data-target="#exampleModal">
                                                                    O'chirish
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <section class="content">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <table class="tablesorter">
                                                <thead>
                                                    <tr>
                                                        <th>Mavzu raqami</th>
                                                        <th>Mavzu</th>
                                                        <th>Mashg'ulot</th>
                                                        <th>soati</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($mmt_mavzus as $mavzu)
                                                    @foreach($mavzu->mmt_mashgulots as $mashgulot)
                                                        <tr>
                                                            <td>{{$mavzu->mavzu_raqami}}</td>
                                                            <td>{{$mavzu->nomi}}</td>
                                                            <td>{{$mashgulot->nomi}}</td>
                                                            <td>{{$mashgulot->soati}}</td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>

                    </div>

                    <div id="jt" class="container tab-pane fade"><br>
                        <div class="row">
                            <div class="col-4">
                                <div class="card border-primary">
                                    <div class="card-body text-primary" style="height: 280px;">
                                        <form action="/add/mutaxassislik" method="POST">
                                            @csrf
                                            <div class="form-group" style="padding-right: 10px;">
                                                <select name="oquv_yili" class="form-control">
                                                    <option value="">O'quv yili</option>
                                                    <option value="2016-2017">2016-2017</option>
                                                    <option value="2017-2018">2017-2018</option>
                                                    <option value="2018-2019">2018-2019</option>
                                                    <option value="2019-2020">2019-2020</option>
                                                    <option value="2020-2021">2020-2021</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <input id="mutaxassislik" name="nomi" type="text" class="form-control mutaxassislik" placeholder="mutaxassislik nomi">
                                            </div>
                                            <div class="row" >
                                                <div class="form-group" style="margin-left: 10px" >
                                                    <button type="submit" class="btn btn-primary" >Qo'shish</button>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary" style="margin-left: 10px" data-toggle="modal" data-target="#exampleModal2">
                                                        O'zgartirish
                                                    </button>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary" style="margin-left: 10px" data-toggle="modal" data-target="#exampleModal2">
                                                        O'chirish
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="card border-primary">
                                    <div class="card-body text-primary" style="height: 280px;">
                                        <form action="/add/jt_fan" method="POST">
                                            @csrf
                                            <div class="form-group" style="padding-right: 10px;">
                                                <select name="oquv_yili" class="form-control jt_year_click">
                                                    <option value="">O'quv yili</option>
                                                    <option value="2016-2017">2016-2017</option>
                                                    <option value="2017-2018">2017-2018</option>
                                                    <option value="2018-2019">2018-2019</option>
                                                    <option value="2019-2020">2019-2020</option>
                                                    <option value="2020-2021">2020-2021</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select name="mutaxassislik"  id="mutaxassislik" class="form-control mutaxassislik mutaxassislik_click" >
                                                    <option value="">Mutaxassislik</option>
                                                    @foreach($mutaxassisliks as $mutaxassislik)
                                                        <option value="{{$mutaxassislik->id}}">{{$mutaxassislik->nomi}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <input id="fan" name="nomi" type="text" class="form-control" placeholder="Fan nomi">
                                            </div>
                                            <div class="row" >
                                                <div class="form-group" style="margin-left: 10px" >
                                                    <button type="submit" class="btn btn-primary" >Qo'shish</button>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary" style="margin-left: 10px" data-toggle="modal" data-target="#editJtFan">
                                                        O'zgartirish
                                                    </button>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary" style="margin-left: 10px" data-toggle="modal" data-target="#exampleModal2">
                                                        O'chirish
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="editJtFan" tabindex="-1" id="ajax-crud-modal" role="dialog" aria-labelledby="editJtFan" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="editJtFan">J.T fanni o'zgartirish</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="edit/jt_fan" id="editJtFan" method="POST" class="m-5" >
                                            @csrf
                                                <div class="form-group" style="padding-right: 10px;">
                                                    <select name="oquv_yili" class="form-control jt_year_click">
                                                        <option value="">O'quv yili</option>
                                                        <option value="2016-2017">2016-2017</option>
                                                        <option value="2017-2018">2017-2018</option>
                                                        <option value="2018-2019">2018-2019</option>
                                                        <option value="2019-2020">2019-2020</option>
                                                        <option value="2020-2021">2020-2021</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select name="mutaxassislik"  id="mutaxassislik" class="form-control mutaxassislik_click" >
                                                        <option value="">Mutaxassislik</option>
                                                        @foreach($mutaxassisliks as $mutaxassislik)
                                                            <option value="{{$mutaxassislik->id}}">{{$mutaxassislik->nomi}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control">
                                                </div>
                                                <div class="form-group" >
                                                    <button class="btn btn-primary" type="submit">O'zgartirish</button>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="card border-primary mb-3">
                                    <div class="card-body text-primary" style="height: 280px;">
                                        <form action="/add/jt_mavzu" method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="form-group" style="padding-right: 10px;">
                                                    <select name="oquv_yili" class="form-control jt_year_click">
                                                        <option value="">O'quv yili</option>
                                                        <option value="2016-2017">2016-2017</option>
                                                        <option value="2017-2018">2017-2018</option>
                                                        <option value="2018-2019">2018-2019</option>
                                                        <option value="2019-2020">2019-2020</option>
                                                        <option value="2020-2021">2020-2021</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select name="mutaxassislik"  id="mutaxassislik" class="form-control mutaxassislik_click" >
                                                        <option value="">Mutaxassislik</option>
                                                        @foreach($mutaxassisliks as $mutaxassislik)
                                                            <option value="{{$mutaxassislik->id}}">{{$mutaxassislik->nomi}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group" >
                                                    <select id="fan" class="form-control" name="fan_id">
                                                        <option>Fan</option>
                                                        @foreach($fans as $fan)
                                                            <option value="{{$fan->id}}">{{$fan->nomi}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group" style="margin-left: 0px;">
                                                    <input required name="jt_mavzu_raqami" id="jt_mavzu_raqami" style="width: 80px;" class="form-control" type="number"placeholder="№">
                                                </div>
                                                <div class="form-group">
                                                    <input required name="jt_mavzu" id="jt_mavzu" class="form-control" style="width: 310px;" type="text" placeholder="Mavzuni kiriting">
                                                </div>
                                                <div class="row">
                                                    <div class="form-group" style="margin-left: 10px" >
                                                        <button type="submit" class="btn btn-primary" >Qo'shish</button>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-primary" style="margin-left: 10px" data-toggle="modal" data-target="#editJtMavzu">
                                                            O'zgartirish
                                                        </button>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-primary" style="margin-left: 10px" data-toggle="modal" data-target="#deleteJtMavzu">
                                                            O'chirish
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal fade" id="editJtMavzu" tabindex="-1" id="ajax-crud-modal" role="dialog" aria-labelledby="editJtMavzu" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editJtMavzu">J.T fanni o'zgartirish</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form action="edit/jt_mavzu" id="editJtMavzu" method="POST" class="m-5" >
                                                    @csrf
                                                    <div class="form-group" style="padding-right: 10px;">
                                                        <select name="oquv_yili" class="form-control jt_year_click">
                                                            <option value="">O'quv yili</option>
                                                            <option value="2016-2017">2016-2017</option>
                                                            <option value="2017-2018">2017-2018</option>
                                                            <option value="2018-2019">2018-2019</option>
                                                            <option value="2019-2020">2019-2020</option>
                                                            <option value="2020-2021">2020-2021</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <select name="mutaxassislik"  id="mutaxassislik" class="form-control mutaxassislik_click" >
                                                            <option value="">Mutaxassislik</option>
                                                            @foreach($mutaxassisliks as $mutaxassislik)
                                                                <option value="{{$mutaxassislik->id}}">{{$mutaxassislik->nomi}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group" >
                                                        <select id="fan" class="form-control" name="fan_id">
                                                            <option>Fan</option>
                                                            @foreach($fans as $fan)
                                                                <option value="{{$fan->id}}">{{$fan->nomi}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                    <div class="form-group" >
                                                        <button class="btn btn-primary" type="submit">O'zgartirish</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card border-primary mb-3" >
                                    <div class="card-body text-primary">
                                        <form action="/add/jt_mashgulot" method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="form-group" style="padding-right: 10px;">
                                                    <select name="oquv_yili" class="form-control jt_year_click">
                                                        <option value="">O'quv yili</option>
                                                        <option value="2016-2017">2016-2017</option>
                                                        <option value="2017-2018">2017-2018</option>
                                                        <option value="2018-2019">2018-2019</option>
                                                        <option value="2019-2020">2019-2020</option>
                                                        <option value="2020-2021">2020-2021</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select name="mutaxassislik"  id="mutaxassislik" class="form-control mutaxassislik_click" >
                                                        <option value="">Mutaxassislik</option>
                                                        @foreach($mutaxassisliks as $mutaxassislik)
                                                            <option value="{{$mutaxassislik->id}}">{{$mutaxassislik->nomi}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group" style="padding-left: 10px">
                                                    <select class="form-control dynamic jt_fan_click" data-dependent="mavzu" name="jt_fan" id="jt_fan">
                                                        <option>Fan</option>
                                                        @foreach($fans as $fan)
                                                            <option value="{{$fan->id}}">{{$fan->nomi}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group" style="padding-left: 10px">
                                                    <select class="form-control" name="jt_mavzu_id" id="jt_mavzu_id">
                                                        <option>Mavzu</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <input placeholder="Mashg'ulot nomi" style="width: 400px" class="form-control" name="nomi" type="text">
                                                </div>
                                                <div class="form-group" style="margin-left: 10px">
                                                    <input placeholder="Mashg'ulot soati" type="number" class="form-control" id="soati" name="soati">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group" style="margin-left: 10px" >
                                                    <button type="submit" class="btn btn-primary" >Qo'shish</button>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary" style="margin-left: 10px" data-toggle="modal" data-target="#editJtMashgulot">
                                                        O'zgartirish
                                                    </button>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary" style="margin-left: 10px" data-toggle="modal" data-target="#deleteJtMashgulot">
                                                        O'chirish
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="modal fade" id="editJtMashgulot" tabindex="-1" id="ajax-crud-modal" role="dialog" aria-labelledby="editJtMashgulot" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="editJtMashgulot">J.T Mashg'ulotni o'zgartirish</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="edit/jt_mashgulot" id="editJtMashgulot" method="POST" class="m-5" >
                                                @csrf
                                                <div class="form-group" style="padding-right: 10px;">
                                                    <select name="oquv_yili" class="form-control jt_year_click">
                                                        <option value="">O'quv yili</option>
                                                        <option value="2016-2017">2016-2017</option>
                                                        <option value="2017-2018">2017-2018</option>
                                                        <option value="2018-2019">2018-2019</option>
                                                        <option value="2019-2020">2019-2020</option>
                                                        <option value="2020-2021">2020-2021</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select name="mutaxassislik"  id="mutaxassislik" class="form-control mutaxassislik_click" >
                                                        <option value="">Mutaxassislik</option>
                                                        @foreach($mutaxassisliks as $mutaxassislik)
                                                            <option value="{{$mutaxassislik->id}}">{{$mutaxassislik->nomi}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group" >
                                                    <select id="fan" class="form-control" name="fan_id">
                                                        <option>Fan</option>
                                                        @foreach($fans as $fan)
                                                            <option value="{{$fan->id}}">{{$fan->nomi}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control" name="jt_mavzu_id" id="jt_mavzu_id">
                                                        <option>Mavzu</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control">
                                                </div>
                                                <div class="form-group" >
                                                    <button class="btn btn-primary" type="submit">O'zgartirish</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <section class="content">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <table class="tablesorter">
                                                <thead>
                                                    <tr>
                                                        <th>Mutaxassislik</th>
                                                        <th>Fan</th>
                                                        <th>Mavzu</th>
                                                        <th>Mashg'ulot</th>
                                                        <th>soati</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($fans as $fan)
                                                    @foreach($fan->jt_mavzus as $mavzu)
                                                    {{--  {{$mavzu->mmt_mashgulots}}--}}
                                                    @foreach($mavzu->jt_mashgulots as $mashgulot)
                                                        <tr>
                                                            <td>{{$mashgulot->mutaxassislik_id}}</td>
                                                            <td>{{$fan->nomi}}</td>
                                                            <td>{{$mavzu->nomi}}</td>
                                                            <td>{{$mashgulot->nomi}}</td>
                                                            <td>{{$mashgulot->soati}}</td>

                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
{{--<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>--}}
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable();
    });
    $(document).ready(function() {
        /**
         * for showing edit item popup
         */

        $(document).on('click', "#edit-item", function () {
            $(this).addClass('edit-item-trigger-clicked'); //useful for identifying which trigger was clicked and consequently grab data from the correct row and not the wrong one.

            var options = {
                'backdrop': 'static'
            };
            $('#edit-modal').modal(options)
        })

        // on modal show
        $('#edit-modal').on('show.bs.modal', function () {
            var el = $(".edit-item-trigger-clicked"); // See how its usefull right here?
            var row = el.closest(".data-row");

            // get the data
            var id = el.data('item-id');
            var name = row.children(".name").text();
            var description = row.children(".description").text();

            // fill the data in the input fields
            $("#modal-input-id").val(id);
            $("#modal-input-name").val(name);
            $("#modal-input-description").val(description);

        })
        $('#edit-modal').on('hide.bs.modal', function () {
            $('.edit-item-trigger-clicked').removeClass('edit-item-trigger-clicked')
            $("#edit-form").trigger("reset");
        })
    })
    $(document).ready(function() {
        $('.jt_fan_click').change(function () {
            d = $(this).parent().parent();
            $.ajax({
                url: "/mashgulotlar/jh/search",
                method: "GET",
                data: {
                    'jt_fan': $(this).val()
                },
                success: function(result){
                    d.find('select[name="jt_mavzu_id"]').html(result);
                    // console.log(d.find('select[name="jt_mavzu_id"]').attr('class'));
                }
            });
        });
        $('.mutaxassislik_click').change(function () {
            d = $(this).parent().parent();
            $.ajax({
                url: "/mashgulotlar/jh/search",
                method: "GET",
                data: {
                    'jt_mash': $(this).val()
                },
                success: function(result){
                    d.find('select[name="fan_id"]').html(result);
                    d.find('select[name="jt_fan"]').html(result);
                    // console.log(d.find('select[name="jt_mavzu_id"]').attr('class'));
                }
            });
        });
        $('.jt_mavzu_click').change(function () {
            d = $(this).parent().parent();
            $.ajax({
                url: "/mashgulotlar/jh/search",
                method: "GET",
                data: {
                    'jt_mavzu': $(this).val()
                },
                success: function(result){
                    d.find('select[name="jt_mashgulot"]').html(result);
                    // console.log(d.find('select[name="jt_mavzu_id"]').attr('class'));
                }
            });
        });
        $('.jt_year_click').change(function () {
            d = $(this).parent().parent();
            $.ajax({
                url: "/mashgulotlar/jh/search",
                method: "GET",
                data: {
                    'js_year': $(this).val()
                },
                success: function(result){
                    d.find('select[name="mutaxassislik"]').html(result);
                    // console.log(d.find('select[name="jt_mavzu_id"]').attr('class'));
                }
            });
        });
        $(document).on('click', "#edit-item", function () {
            $(this).addClass('edit-item-trigger-clicked'); //useful for identifying which trigger was clicked and consequently grab data from the correct row and not the wrong one.

            var options = {
                'backdrop': 'static'
            };
            $('#edit-modal').modal(options)
        })

        // on modal show
        $('#edit-modal').on('show.bs.modal', function () {
            var el = $(".edit-item-trigger-clicked"); // See how its usefull right here?
            var row = el.closest(".data-row");

            // get the data
            var id = el.data('item-id');
            var name = row.children(".name").text();
            var description = row.children(".description").text();

            // fill the data in the input fields
            $("#modal-input-id").val(id);
            $("#modal-input-name").val(name);
            $("#modal-input-description").val(description);

        })
        $('#edit-modal').on('hide.bs.modal', function () {
            $('.edit-item-trigger-clicked').removeClass('edit-item-trigger-clicked')
            $("#edit-form").trigger("reset");
        })
    })

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        /*  When user click add user button */
        $('#create-new-user').click(function () {
            $('#btn-save').val("create-user");
            $('#editmmtmavzuModal').trigger("reset");
            $('#userCrudModal').html("Add New User");
            $('#ajax-crud-modal').modal('show');
        });

        /* When click edit user */
        $('body').on('click', '#edit-user', function () {
            var user_id = $(this).data('id');
            $.get('ajax-crud/' + user_id +'/edit', function (data) {
                $('#userCrudModal').html("Edit User");
                $('#btn-save').val("edit-user");
                $('#ajax-crud-modal').modal('show');
                $('#user_id').val(data.id);
                $('#name').val(data.name);
                $('#email').val(data.email);
            })
        });
        //delete user login
        $('body').on('click', '.delete-user', function () {
            var user_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "DELETE",
                url: "{{ url('ajax-crud')}}"+'/'+user_id,
                success: function (data) {
                    $("#user_id_" + user_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    });

    if ($("#editmmtmavzuModal").length > 0) {
        $("#editmmtmavzuModal").validate({

            submitHandler: function(form) {

                var actionType = $('#btn-save').val();
                $('#btn-save').html('Sending..');

                $.ajax({
                    data: $('#editmmtmavzuModal').serialize(),
                    url: "https://www.tutsmake.com/laravel-example/ajax-crud/store",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        var user = '<tr id="user_id_' + data.id + '"><td>' + data.id + '</td><td>' + data.name + '</td><td>' + data.email + '</td>';
                        user += '<td><a href="javascript:void(0)" id="edit-user" data-id="' + data.id + '" class="btn btn-info">Edit</a></td>';
                        user += '<td><a href="javascript:void(0)" id="delete-user" data-id="' + data.id + '" class="btn btn-danger delete-user">Delete</a></td></tr>';


                        if (actionType == "create-user") {
                            $('#users-crud').prepend(user);
                        } else {
                            $("#user_id_" + data.id).replaceWith(user);
                        }

                        $('#editmmtmavzuModal').trigger("reset");
                        $('#ajax-crud-modal').modal('hide');
                        $('#btn-save').html('Save Changes');

                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $('#btn-save').html('Save Changes');
                    }
                });
            }
        })
    }
</script>
</body>
</html>

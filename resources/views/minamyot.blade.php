@extends('layouts.header')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <h1 class="text-dark text-center">Minamyot va artileriya</h1>
        {{--<a href="" style="margin-left: 400px;"><button class=" btn btn-outline-success">Qo'shish <i class="ion ion-android-add-circle p-1"></i></button></a>--}}
    </nav>
    <!-- /.navbar -->

@extends('layouts.navbar')
@section('qurol','active')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" >
        <section class="content" >
            <div class="container-fluid" >
                <div class="content-header">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                </div>
                <div class="card card-info">
                    <div class="card-header">
                        <h1 class="card-title">Minamyot va artileriya qo'shish</h1>
                    </div>
                    <form class="form-horizontal" action="/store/minamyot_va_artileriya" method="POST">
                        <div class="card-body">
                            @csrf
                            @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                            <div class="row">
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="shatakka_oluvchi_dvegateli_quvvati" class="form-control" type="text" placeholder="shatakka_oluvchi_dvegateli_quvvati" name="shatakka_oluvchi_dvegateli_quvvati">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="ogirligi" class="form-control" type="text" placeholder="ogirligi" name="ogirligi">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="shatakka_oluvchining_ogirligi" class="form-control" type="text" placeholder="shatakka_oluvchining_ogirligi" name="shatakka_oluvchining_ogirligi">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="max_harakat_tezligi" class="form-control" type="text" placeholder="max_harakat_tezligi" name="max_harakat_tezligi">
                                            </div>
                                        </div>
                            <div class="row">
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="orqaga_harakatlanish_max_tezligi" class="form-control" type="text" placeholder="orqaga_harakatlanish_max_tezligi" name="orqaga_harakatlanish_max_tezligi">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="umumiy_yurish_zaxirasi" class="form-control" type="text" placeholder="umumiy_yurish_zaxirasi" name="umumiy_yurish_zaxirasi">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="kotarilish_burchagi" class="form-control" type="text" placeholder="kotarilish_burchagi" name="kotarilish_burchagi">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="yon_tomonlarga_burilish_burchagi" class="form-control" type="text" placeholder="yon_tomonlarga_burilish_burchagi" name="yon_tomonlarga_burilish_burchagi">
                                            </div>
                                        </div>
                            <div class="row">
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="devorlardan_otish_balandligi" class="form-control" type="text" placeholder="devorlardan_otish_balandligi" name="devorlardan_otish_balandligi">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="chuqurlikdan_otish_kengligi" class="form-control" type="text" placeholder="chuqurlikdan_otish_kengligi" name="chuqurlikdan_otish_kengligi">
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <input id="shatakka_oluvchi_dvegatelning_km_benzin_istemoli" class="form-control" type="text" placeholder="shatakka_oluvchi_dvegatelning_km_benzin_istemoli" name="shatakka_oluvchi_dvegatelning_km_benzin_istemoli">
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <input id="shatakka_oluvchi_dvegatelning_soatiga_benzin_istemoli" class="form-control" type="text" placeholder="shatakka_oluvchi_dvegatelning_soatiga_benzin_istemoli" name="shatakka_oluvchi_dvegatelning_soatiga_benzin_istemoli">
                                                </div>
                                            </div>
                                        </div>
                            <div class="row">
                                            <div class="col-3 ">
                                                    <div class="form-group">
                                                        <input id="korpus_uzunligi" class="form-control" type="text" placeholder="korpus_uzunligi" name="korpus_uzunligi">
                                                    </div>
                                                </div>
                                            <div class="col-3 ">
                                                    <div class="form-group">
                                                        <input id="korpus_kengligi" class="form-control" type="text" placeholder="korpus_kengligi" name="korpus_kengligi">
                                                    </div>
                                                </div>
                                            <div class="col-3 ">
                                                    <div class="form-group">
                                                        <input id="bor_boyicha_balandligi" class="form-control" type="text" placeholder="bor_boyicha_balandligi" name="bor_boyicha_balandligi">
                                                    </div>
                                                </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <input id="ekvivalent_zirx_qalinligi" class="form-control" type="text" placeholder="ekvivalent_zirx_qalinligi" name="ekvivalent_zirx_qalinligi">
                                                </div>
                                            </div>
                                        </div>
                            <div class="row">
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <input id="pushka_kalibri" class="form-control" type="text" placeholder="pushka_kalibri" name="pushka_kalibri">
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <input id="jangovor_toplam" class="form-control" type="text" placeholder="jangovor_toplam" name="jangovor_toplam">
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <input id="otish_surati" class="form-control" type="text" placeholder="otish_surati" name="otish_surati">
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <input id="nishonlarni_yoq_qilish_uzoqligi" class="form-control" type="text" placeholder="nishonlarni_yoq_qilish_uzoqligi" name="nishonlarni_yoq_qilish_uzoqligi">
                                                </div>
                                            </div>

                                        </div>
                            <div class="row">
                               
                                <div class="col-3">
                                    <div class="form-group">
                                        <input id=gorizontal_boyicha_pushkaning_nishonga_olish_tezligi" class="form-control" type="text" placeholder="gorizontal_boyicha_pushkaning_nishonga_olish_tezligi" name="gorizontal_boyicha_pushkaning_nishonga_olish_tezligi">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input id="snaryadlarning_talofat_yetkazish_boyicha_kengligi" class="form-control" type="text" placeholder="snaryadlarning_talofat_yetkazish_boyicha_kengligi" name="snaryadlarning_talofat_yetkazish_boyicha_kengligi">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-info" type="submit">Qo'shish</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

</script>
</body>
</html>

@extends('layouts.header')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <h1 class="text-dark text-center">O'qotar qurol</h1>
        {{--<a href="" style="margin-left: 400px;"><button class=" btn btn-outline-success">Qo'shish <i class="ion ion-android-add-circle p-1"></i></button></a>--}}
    </nav>
    <!-- /.navbar -->

@extends('layouts.navbar')
@section('qurol','active')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" >
        <section class="content" >
            <div class="container-fluid" >
                <div class="content-header">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                </div>
                <div class="card card-info">
                    <div class="card-header">
                        <h1 class="card-title">O'q otar qurol qo'shish</h1>
                    </div>
                    <form class="form-horizontal" action="/store/oqotar_qurol" method="POST">
                        <div class="card-body">
                            @csrf
                            @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                            <div class="row">
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="qurol_ogirligi" class="form-control" type="text" placeholder="qurol_ogirligi" name="qurol_ogirligi">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="inson_max_harakat_tezligi" class="form-control" type="text" placeholder="inson_max_harakat_tezligi" name="inson_max_harakat_tezligi">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="inson_orqaga_harakatlanish_max_tezligi" class="form-control" type="text" placeholder="inson_orqaga_harakatlanish_max_tezligi" name="inson_orqaga_harakatlanish_max_tezligi">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="umumiy_yurish_zaxirasi" class="form-control" type="text" placeholder="umumiy_yurish_zaxirasi" name="umumiy_yurish_zaxirasi">
                                            </div>
                                        </div>
                            <div class="row">
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="kotarilish_burchagi" class="form-control" type="text" placeholder="kotarilish_burchagi" name="kotarilish_burchagi">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="yon_tomonlarga_burilish_burchagi" class="form-control" type="text" placeholder="yon_tomonlarga_burilish_burchagi" name="yon_tomonlarga_burilish_burchagi">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="devorlardan_otish_balandligi" class="form-control" type="text" placeholder="devorlardan_otish_balandligi" name="devorlardan_otish_balandligi">
                                            </div>
                                            <div class="col-3 form-group" style="padding-right: 10px; ">
                                                <input id="chuqurlikdan_otish_kengligi" class="form-control" type="text" placeholder="chuqurlikdan_otish_kengligi" name="chuqurlikdan_otish_kengligi">
                                            </div>
                            </div>
                            <div class="row">
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <input id="qurol_uzunligi" class="form-control" type="text" placeholder="qurol_uzunligi" name="qurol_uzunligi">
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <input id="qurol_kalibri" class="form-control" type="text" placeholder="qurol_kalibri" name="qurol_kalibri">
                                                </div>
                                            </div>

                                            <div class="col-3">
                                                <div class="form-group">
                                                    <input id="magazindagi_oqlar_soni" class="form-control" type="text" placeholder="magazindagi_oqlar_soni" name="magazindagi_oqlar_soni">
                                                </div>
                                            </div>
                                            <div class="col-3 ">
                                                    <div class="form-group">
                                                        <input id="otish_surati" class="form-control" type="text" placeholder="otish_surati" name="otish_surati">
                                                    </div>
                                                </div>
                            </div>
                            <div class="row">
                                            <div class="col-3 ">
                                                    <div class="form-group">
                                                        <input id="nishonlarni_yoq_qilish_uzoqligi" class="form-control" type="text" placeholder="nishonlarni_yoq_qilish_uzoqligi" name="nishonlarni_yoq_qilish_uzoqligi">
                                                    </div>
                                                </div>
                                            <div class="col-3 ">
                                                    <div class="form-group">
                                                        <input id="jangovor_toplam_nomenklaturasi" class="form-control" type="text" placeholder="jangovor_toplam_nomenklaturasi" name="jangovor_toplam_nomenklaturasi">
                                                    </div>
                                                </div>
                            </div>
                            
                            <div class="card-footer">
                                <button class="btn btn-info" type="submit">Qo'shish</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

</script>
</body>
</html>

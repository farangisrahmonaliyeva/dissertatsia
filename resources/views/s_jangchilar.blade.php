@extends('layouts.header')
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    @extends('layouts.navbar')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">DataTables</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <!-- /.card -->

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable with default features</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Uunvoni</th>
                                    <th>Familyasi</th>
                                    <th>Ismi</th>
                                    <th>Sharifi</th>
                                    <th>Jinsi</th>
                                    <th>Tugilgan_vaqti</th>
                                    <th>Ish_joyi</th>
                                    <th>Seksiya</th>
                                    {{--<th>Guruh</th>--}}
                                    {{--<th>Vzvod</th>--}}
                                    {{--<th>Battalion</th>--}}
                                    {{--<th>Xarbiy_qism</th>--}}
                                    <th>Boyi</th>
                                    <th>Vazni</th>
                                    <th>Jangovor_qobilyat</th>
                                    <th>Soglomligi</th>
                                    <th>Jismoniy tayyorgarligi</th>
                                    <th>Jismoniy xolati</th>
                                    <th>Charchaganlik darajasi</th>
                                    <th>Manaviy_ruhiy_holati</th>
                                    <th>Ahloqiy_ruhiy_xolati</th>
                                    <th>MMT</th>
                                    <th>Jangovor_tayyorgarligi</th>
                                    <th>MMT vaqti</th>
                                    <th>Created_at</th>
                                    <th>Updated_at</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($jangchilars as $jangchilar)
                                    <tr>
                                        <td>{{$jangchilar->unvoni}}</td>
                                        <td>{{$jangchilar->familyasi}}</td>
                                        <td>{{$jangchilar->ismi}}</td>
                                        <td>{{$jangchilar->sharifi}}</td>
                                        <td>{{$jangchilar->jinsi}}</td>
                                        <td>{{$jangchilar->tugilgan_vaqti}}</td>
                                        <td>{{$jangchilar->ish_joyi}}</td>
                                        <td>{{$jangchilar->seksiya_id}}</td>
                                        {{--<td>{{$jangchilar->guruh}}</td>--}}
                                        {{--<td>{{$jangchilar->vzvod}}</td>--}}
                                        {{--<td>{{$jangchilar->battalion}}</td>--}}
                                        {{--<td>{{$jangchilar->xarbiy_qism}}</td>--}}
                                        <td>{{$jangchilar->boyi}}</td>
                                        <td>{{$jangchilar->vazni}}</td>
                                        <td>{{$jangchilar->jangovor_qobilyati}}</td>
                                        <td>{{$jangchilar->soglomligi}}</td>
                                        <td>{{$jangchilar->jismoniy_tayyorgarligi}}</td>
                                        <td>{{$jangchilar->jismoniy_xolati}}</td>
                                        <td>{{$jangchilar->charchaganlik_darajasi}}</td>
                                        <td>{{$jangchilar->manaviy_ruhiy_holati}}</td>
                                        <td>{{$jangchilar->ahloqiy_ruhiy_xolati}}</td>
                                        <td>{{$jangchilar->mmt}}</td>
                                        <td>{{$jangchilar->jangovor_tayyorgarligi}}</td>
                                        <td>{{$jangchilar->mmt_vaqti}}</td>
                                        <td>{{$jangchilar->created_at}}</td>
                                        <td>{{$jangchilar->updated_at}}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>
                                {{--<tr>--}}
                                {{--<th>Rendering engine</th>--}}
                                {{--<th>Browser</th>--}}
                                {{--<th>Platform(s)</th>--}}
                                {{--<th>Engine version</th>--}}
                                {{--<th>CSS grade</th>--}}
                                {{--</tr>--}}
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });
</script>
</body>
</html>

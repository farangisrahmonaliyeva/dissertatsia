@extends('layouts.header')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <nav class="main-header navbar navbar-expand  navbar-light">
        <h1 class="m-0 text-dark">{{$hq->nomi}} </h1>
        <form action="/seksiya/create" method="post">
            @csrf
            <div style="margin-left: 400px;">
                <button class="col-sm-6 btn btn-outline-success" type="submit" style="width: 200px" >Qo'shish
                    <i class="ion ion-android-add-circle p-1"></i>
                </button>
            </div>
            <input type="hidden" name="id" value="{{$id}}">
        </form>

    </nav>
@extends('layouts.navbar')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="col-sm-12">

                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    @foreach($seksiyas as $seksiya)
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <div class="row">
                                        <div class="col-9">
                                            <p>{{$seksiya->nomi}}</p>
                                        </div>
                                        <div class="col float-right">
                                            <a href="{{route('edit_seksiya',$seksiya->id)}}" style="color: white;"> <i class="ion ion-edit" data-toggle="tooltip" data-placement="top" title="O'zgartirish" style="font-size: 20px"></i></a>
                                            <a href="{{route('delete_seksiya',$seksiya->id)}}" style="color: white;">  <i class="ion ion-trash-a" data-toggle="tooltip" data-placement="top" title="O'chirish" style="font-size: 20px"></i></a>
                                        </div>
                                    </div>
                                    <p>shaxsiy tarkib soni - {{$seksiya->shaxsiy_tarkib_soni}} ta</p>
                                </div>
                                {{--<a href="/shaxsiy_tarkib/{{$seksiya->id}}" class="small-box-footer">Ko'rish <i class="fas fa-arrow-circle-right"></i></a>--}}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
</div>
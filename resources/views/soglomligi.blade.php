@extends('layouts.header')
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    @extends('layouts.navbar')



    <div class="content-wrapper">
        {{--Test--}}
        <div class="container box">
            <div class="form-group">
                <select name="harbiy_qism" id="harbiy_qism" class="form-control input-lg dynamic" data-dependent="battalion">
                    @foreach($harbiy_qisms as $harbiy_qism)
                        <option value="{{$harbiy_qism->id}}">{{$harbiy_qism->nomi}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select name="battalion" id="battalion" class="form-control input-lg dynamic" data-dependent="vzvod">
                    <option value="">Select battalion</option>
                </select>
            </div>
            {{@csrf_field()}}
        </div>
        {{--Test--}}
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">DataTables</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <form action="" method="POST">
                <div class="container-fluid row">

                    <div class="form-group">
                            <label for="harbiy_qism">Harbiy qismni tanlang</label>
                            <select class="form-control dynamic" data-dependent="battalion" name="harbiy_qism" id="harbiy_qism">
                                <option value="">Harbiy qismni tanlang</option>
                                @foreach($harbiy_qisms as $harbiy_qism)
                                    <option value="{{$harbiy_qism->id}}">{{$harbiy_qism->nomi}}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group ">
                            <label for="battalion">battalion tanlang</label>
                            <select class="form-control dynamic" data-dependent="battalion" name="battalion" id="battalion">
                                <option value="">battalion tanlang</option>
                                @foreach($battalions as $battalion)
                                    <option value="{{$battalion->id}}">{{$battalion->nomi}}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group">
                            <label for="vzvod">vzvod tanlang</label>
                            <select class="form-control dynamic"  name="vzvod" id="vzvod">
                                <option value="">vzvodni tanlang</option>
                                @foreach($vzvods as $vzvod)
                                    <option value="{{$vzvod->id}}">{{$vzvod->nomi}}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group">
                            <label for="guruh">guruhni tanlang</label>
                            <select class="form-control dynamic" data-dependent="guruh" name="guruh" id="guruh">
                                <option value="">guruhni tanlang</option>
                                @foreach($guruhs as $guruh)
                                    <option value="{{$guruh->id}}">{{$guruh->nomi}}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group">
                            <label for="seksiya">seksiya tanlang</label>
                            <select class="form-control dynamic" data-dependent="seksiya" name="seksiya" id="seksiya">
                                <option value="">seksiya tanlang</option>
                                @foreach($seksiyas as $seksiya)
                                    <option value="{{$seksiya->id}}">{{$seksiya->nomi}}</option>
                                @endforeach
                            </select>
                    </div>

                    <div class="form-group" style="margin-top: 30px; margin-left: 30px;">
                        <button class="btn btn-success" type="submit">OK</button>
                    </div>
                </div>
            </form>
        </section>
        <section class="content">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <!-- /.card -->

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable with default features</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Uunvoni</th>
                                    <th>Familyasi</th>
                                    <th>Ismi</th>
                                    <th>Sharifi</th>
                                    <th>Baholash</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($jangchilars as $jangchilar)
                                    <tr>
                                        <td>{{$jangchilar->unvoni}}</td>
                                        <td>{{$jangchilar->familyasi}}</td>
                                        <td>{{$jangchilar->ismi}}</td>
                                        <td>{{$jangchilar->sharifi}}</td>

                                        <td class="align-middle">
                                            <button type="button" class="btn btn-success" id="edit-item" data-item-id="{{$jangchilar->id}}">Baxolash</button>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    <!-- Attachment Modal -->
                    <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="edit-modal-label">Edit Data</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" id="attachment-body-content">
                                    <form id="edit-form" class="form-horizontal" method="POST" action="">
                                        <div class="card text-white bg-dark mb-0">
                                            <div class="card-header">
                                                <h2 class="m-0">Edit</h2>
                                            </div>
                                            <div class="card-body">
                                                <!-- id -->
                                                <div class="form-group">
                                                    <label class="col-form-label" for="modal-input-id">Id (just for reference not meant to be shown to the general public) </label>
                                                    <input type="text" name="modal-input-id" class="form-control" id="modal-input-id" required>
                                                </div>
                                                <!-- /id -->
                                                <!-- name -->
                                                <div class="form-group">
                                                    <label class="col-form-label" for="modal-input-name">Name</label>
                                                    <input type="text" name="modal-input-name" class="form-control" id="modal-input-name" required autofocus>
                                                </div>
                                                <!-- /name -->
                                                <!-- description -->
                                                <div class="form-group">
                                                    <label class="col-form-label" for="modal-input-description">Email</label>
                                                    <input type="text" name="modal-input-description" class="form-control" id="modal-input-description" required>
                                                </div>
                                                <!-- /description -->
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Done</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Attachment Modal -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });
    $(document).ready(function() {
        /**
         * for showing edit item popup
         */

        $(document).on('click', "#edit-item", function () {
            $(this).addClass('edit-item-trigger-clicked'); //useful for identifying which trigger was clicked and consequently grab data from the correct row and not the wrong one.

            var options = {
                'backdrop': 'static'
            };
            $('#edit-modal').modal(options)
        })

        // on modal show
        $('#edit-modal').on('show.bs.modal', function () {
            var el = $(".edit-item-trigger-clicked"); // See how its usefull right here?
            var row = el.closest(".data-row");

            // get the data
            var id = el.data('item-id');
            var name = row.children(".name").text();
            var description = row.children(".description").text();

            // fill the data in the input fields
            $("#modal-input-id").val(id);
            $("#modal-input-name").val(name);
            $("#modal-input-description").val(description);

        })
        $('#edit-modal').on('hide.bs.modal', function () {
            $('.edit-item-trigger-clicked').removeClass('edit-item-trigger-clicked')
            $("#edit-form").trigger("reset");
        })
    })

</script>
</body>
</html>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/home', 'HomeController@index')->name('home');

//Harbiy Qism
Route::get('/bolinma', 'HarbiyQismController@harbiy_qismlar')->name('bolinma');
Route::get('/harbiy_qism/create', 'HarbiyQismController@create');
Route::post('/store_harbiy_qism','HarbiyQismController@store');
Route::get('/harbiy_qism/{id}/edit', 'HarbiyQismController@edit')->name('edit_harbiy_qism');
Route::post('/harbiy_qism/{id}/update', 'HarbiyQismController@update')->name('update_harbiy_qism');
Route::get('/harbiy_qism/{id}/delete','HarbiyQismController@destroy')->name('delete_harbiy_qism');
//Harbiy Qism

//jangchilar
Route::get('/jangchilar', 'JangchilarController@index');
Route::get('/jangchilar/create', 'JangchilarController@create');
Route::post('/store_jangchilar','JangchilarController@store');
Route::get('/jangchilar/{jangchi}/edit', 'JangchilarController@edit')->name('edit_jangchi');
Route::post('/jangchilar/{jangchi}/update', 'JangchilarController@update')->name('update_jangchi');
Route::post('/jangchilar/{jangchi}/delete','JangchilarController@destroy')->name('delete_jangchi');
Route::get('/jangchilar/{seksiya_id}','JangchilarController@seksiya');
//jangchilar

//Bo'linmalar
Route::get('/harbiy_qism/{id}','BattalionsController@battalionlar')->name('battalionlar');
Route::get('/battalion', 'BattalionsController@index')->name('batalion');
Route::post('/battalion/create','BattalionsController@create');
Route::post('/store_battalion','BattalionsController@store');
Route::get('/battalion/{id}/edit', 'BattalionsController@edit')->name('edit_battalion');
Route::post('/battalion/{id}/update', 'BattalionsController@update')->name('update_battalion');
Route::get('/battalion/{id}/delete','BattalionsController@destroy')->name('delete_battalion');
//Bo'linmalar

//Vzvod
Route::get('/battalion/{id}','VzvodController@vzvodlar')->name('vzvodlar');
Route::post('/vzvod/create','VzvodController@create');
Route::post('/store_vzvod','VzvodController@store');
Route::get('/vzvod/{id}/edit', 'VzvodController@edit')->name('edit_vzvod');
Route::post('/vzvod/{id}/update', 'VzvodController@update')->name('update_vzvod');
Route::get('/vzvod/{id}/delete','VzvodController@destroy')->name('delete_vzvod');
//Vzvod

//Guruh
Route::get('/vzvod/{id}','GuruhController@guruhlar')->name('guruhlar');
Route::post('/guruh/create','GuruhController@create');
Route::post('/store_guruh','GuruhController@store');
Route::get('/guruh/{id}/edit', 'GuruhController@edit')->name('edit_guruh');
Route::post('/guruh/{id}/update', 'GuruhController@update')->name('update_guruh');
Route::get('/guruh/{id}/delete','GuruhController@destroy')->name('delete_guruh');
//Guruh

//Seksiya
Route::get('/guruh/{id}','SeksiyaController@seksiyalar')->name('seksiyalar');
Route::post('/seksiya/create','SeksiyaController@create');
Route::post('/store_seksiya','SeksiyaController@store');
Route::get('/seksiya/{id}/edit', 'SeksiyaController@edit')->name('edit_seksiya');
Route::post('/seksiya/{id}/update', 'SeksiyaController@update')->name('update_seksiya');
Route::get('/seksiya/{id}/delete','SeksiyaController@destroy')->name('delete_seksiya');
//Seksiya

//Baholash
Route::get('/baholash','BaxolashController@index');
Route::get('/baholash/{id}','BaxolashController@index');
Route::post('/baholash','BaxolashController@index')->name('baholashId');
Route::post('/baholash/jh/soglomligi','BaxolashController@soglomligi');
Route::post('/baholash/jh/jismoniy_tayyorgarlik','BaxolashController@jismoniy_tayyorgarlik');
Route::post('/baholash/jh/charchaganlik_darajasi','BaxolashController@charchaganlik_darajasi');
Route::post('/baholash/mrh/arh','BaxolashController@arh');
Route::post('/baholash/mrh/mmt','BaxolashController@mmt');
Route::post('/baholash/jt','BaxolashController@jt');
Route::get('/baholash/jh/search','BaxolashController@search');
Route::get('/mashgulotlar/jh/search','MashgulotController@search');
Route::get('/mashgulotlar/mmt/search','MashgulotController@mmt_search');
//Baholash

Route::get('/table', function () {
    return view('table ');
});

Route::get('/jangovor_qobiliyat', 'JangovorQobiliyatController@index');
Route::get('/jangovor_qobiliyat/harbiy_qism/{id}', 'JangovorQobiliyatController@battalionlar')->name('jq_bat');
Route::get('/jangovor_qobiliyat/batalyon/{id}', 'JangovorQobiliyatController@vzvodlar')->name('jq_vzvod');
Route::get('/jangovor_qobiliyat/vzvod/{id}', 'JangovorQobiliyatController@guruhlar')->name('jq_guruh');
Route::get('/jangovor_qobiliyat/guruh/{id}', 'JangovorQobiliyatController@seksiyalar')->name('jq_seksiya');

Route::get('/mashgulotlar', 'MashgulotController@index')->name('mashgulotlar');

Route::get('/jismoniy_xolati', 'JangovorQobiliyatController@jismoniy_xolati');
Route::get('/soglomligi','JismoniyHolatiController@soglomligi')->name('soglomligi');
Route::get('/timeline', function () {
    return view('pages/timeline ');
});
Route::get('/form', function () {
    return view('form ');
});
Route::post('/baxolash/jangovor_qobiliyati/soglomligi/{id}','BaxolashController@soglomligi');

Auth::routes();
Route::post('/select_hq','JangovorQobiliyatController@select_hq');
Route::get('/dynamic_dependent','DynamicDependentController@index');

//Mashgulotlar
Route::post('/add/mmt_mavzu','MashgulotController@add_mmt_mavzu');
Route::post('/delete/mmt_mavzu','MashgulotController@delete_mmt_mavzu');
Route::post('/add/mmt_mashgulot','MashgulotController@add_mmt_mashgulot');

Route::post('/add/jt_fan','MashgulotController@add_jt_fan');

Route::post('/add/mutaxassislik','MashgulotController@mutax');
Route::post('/add/jt_mavzu','MashgulotController@add_jt_mavzu');
Route::post('/edit/jt_mavzu','MashgulotController@edit_jt_mavzu');
Route::post('/add/jt_mashgulot','MashgulotController@add_jt_mashgulot');
//Mashgulotlar

Route::get('/ogirlik_koeffitsenti', 'o_kController@index');
Route::post('/ogirlik_koeffitsenti/store/harakatchanlik', 'o_kController@store_harakatchanlik');
Route::post('/ogirlik_koeffitsenti/store/himoyalanganlik', 'o_kController@store_himoyalanganlik');
Route::post('/ogirlik_koeffitsenti/store/qurollanish', 'o_kController@store_qurollanish');

////////////////////////////////
Route::get('/tank', 'TankController@index');
Route::get('/btr', 'TankController@btr');
Route::get('/pjm', 'TankController@pjm');
Route::get('/oqotar_qurol', 'TankController@oqotar_qurol');
Route::get('/minamyot', 'TankController@minamyot');
Route::get('/artileriya_oziyurar', 'TankController@artileriya_oziyurar');
Route::get('/tankka_qarshi_qurol', 'TankController@tankka_qarshi_qurol');
//////////////////////////////////////
Route::post('/store/tank', 'TankController@store');
Route::post('/store/btr', 'TankController@storeBTR');
Route::post('/store/pjm', 'TankController@storePJM');
Route::post('/store/oqotar_qurol', 'TankController@storeoqotar_qurol');
Route::post('/store/minamyot_va_artileriya', 'TankController@storeminamyot_va_artileriya');
Route::post('/store/artileriya_oziyurar', 'TankController@storeartileriya_oziyurar');